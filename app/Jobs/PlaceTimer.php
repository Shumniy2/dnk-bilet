<?php

namespace App\Jobs;

use App\Orders;
use App\Places;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Helper\Cart;

class PlaceTimer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;
    protected $concert_id;
    protected $expire;

    /**
     * PlaceTimer constructor.
     * @param $id
     * @param $expire
     */
    public function __construct($order_id, $expire, $concert_id)
    {
        $this->order_id = $order_id;
        $this->concert_id = $concert_id;
        $this->expire = $expire;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        info("Test queue start");
//        $places = Places::findOrFail($this->id)->where([
//            ['concert_id', $this->concert_id],
//            ['engaged', 1],
//            ['expires', $this->expire]
//        ])->get();
//
//        foreach($places as $place) {
//            $place->engaged = 0;
//            $place->expires = $place->created_at;
//            $place->save();
//        }

//        $order = Orders::findOrFail($this->order_id)->tickets()->get();
//        foreach($order->tickets()->get() as $ticket) {
//            $ticket->places() ->where([
//                ['concert_id', $this->concert_id],
//                ['engaged', 1],
//                ['expires', $this->expire]
//            ])->get();
//        }

        info("Test queue end");
    }

}
