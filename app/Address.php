<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'city_id','address_ru','address_uk','address_en','text_ru','text_ua','text_en',
    ];

    public function cities() {
        return $this->belongsTo(City::class,'city_id', 'id');
    }
}
