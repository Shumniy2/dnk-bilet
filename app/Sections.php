<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sections extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name', 'slug',
    ];
    public function places() {
        return $this->hasMany(Places::class, 'section_id', 'id');
    }

//    public function cultures()
//    {
//        return $this->belongsTo(Culture::class, 'culture_id', 'id');
//    }
}
