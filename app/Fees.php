<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fees extends Model
{
    protected $table = "fee";

    public function scopeWaiting($query)
    {
        return $query->where('slug', 'waiting')->first()->id;
    }
}
