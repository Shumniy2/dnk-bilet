<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [
        'city_ru','city_uk','city_en',
    ];

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }
}
