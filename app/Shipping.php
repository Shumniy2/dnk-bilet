<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    protected $table = "shipping";

    public function scopeInternet($query)
    {
        return $query->where('slug', 'internet')->first()->id;
    }

    public function scopePickup($query)
    {
        return $query->where('slug', 'pickup')->first()->id;
    }
}
