<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Concert extends Model
{
    protected $fillable = [
        'name', 'date','image','status','ticket_order','canceled','codes_without_d','no_in_shop','hide_concert','another_codes','pay_for_organizator',
        'places','archive','description','before','after','hours','minutes','dk_id', 'city_id', 'organizer_id'
    ];

    public $timestamps = false;

//    public function prices() {
//        return $this->hasMany(ConcertPrices::class);
//    }

    public function places() {
        return $this->hasMany(Places::class, 'concert_id', 'id');
    }

    public function colors() {
        return $this->hasMany(Colors::class)->orderByDesc('price');
    }

    public function cultures() {
        return $this->belongsTo(Culture::class, 'dk_id', 'id');
    }

    public function cities() {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function organizers() {
        return $this->belongsTo(Organizators::class, 'organizer_id', 'id');
    }


//    public function concert_prices()
//    {
//        return $this->hasManyThrough(
//            ConcertPrices::class, Places::class,
//            'concert_id', 'place_id', 'id', 'id');
//    }

}
