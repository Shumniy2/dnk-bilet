<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Culture extends Model
{
    protected $table = 'cultures';

    protected $fillable = ['name', 'slug'];

    public function sections()
    {
        return $this->hasMany(Sections::class, 'culture_id', 'id');
    }
//    public function details()
//    {
//        return $this->belongsToMany(Detail::class);
//    }

//    public function concerts()
//    {
//        return $this->belongsToMany(Concert::class);
//    }

}
