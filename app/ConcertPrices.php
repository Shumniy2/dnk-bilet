<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConcertPrices extends Model
{
    protected $table = 'concert_prices';

    public $timestamps = false;

    protected $fillable = ['color_id', 'price', 'concert_id'];

    public function concert() {
        return $this->belongsTo(Concert::class);
    }

    public function places() {
        return $this->hasMany(Places::class, 'price_id', 'concert_id');
    }

    public function color() {
        return $this->belongsTo(Color::class, 'color_id', 'id');
    }
}
