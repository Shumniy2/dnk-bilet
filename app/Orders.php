<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = "orders";

    protected $fillable = ['concert_id', 'fee_id', 'shipping_id'];


    public function tickets()
    {
        return $this->hasMany(Tickets::class, 'order_id', 'id');
    }

    public function concerts() {
        return $this->belongsTo(Concert::class, 'concert_id', 'id');
    }

    public function fee() {
        return $this->belongsTo(Fees::class, 'fee_id', 'id');
    }

    public function shipping() {
        return $this->belongsTo(Shipping::class, 'shipping_id', 'id');
    }
}
