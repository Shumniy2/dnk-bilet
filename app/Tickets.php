<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tickets extends Model
{
    protected $fillable = ['place_id', 'print', 'code'];

    public function orders()
    {
        return $this->belongsTo(Orders::class);
    }


    public function places()
    {
        return $this->belongsTo(Places::class, 'place_id', 'id');
    }

//    public function scopeTotalSum($query) {
//       return $query->sum(function ($ticket) {
//            return $ticket->places->price;
//        });
//    }

    public function getTotalPriceAttribute() {
        return $this->places->price;
    }
}
