<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organizators extends Model
{
    protected $table = "organizators";

    protected $fillable = [
        'name',
    ];
}
