<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Places extends Model
{

    protected $table = "places";

    protected $fillable = [
        'section_id', 'row', 'engaged', 'num_place', 'price', 'color_id', 'concert_id', 'expires'
    ];

    public function sections() {
        return $this->belongsTo(Sections::class, 'section_id', 'id');
    }

    public function concerts() {
        return $this->belongsTo(Concert::class, 'concert_id', 'id');
    }

    public function colors() {
        return $this->belongsTo(Colors::class, 'color_id', 'id');
    }

}
