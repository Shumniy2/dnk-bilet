<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colors extends Model
{
    protected $table = 'colors';

    public $timestamps = false;

    protected $fillable = [
        'price', 'color', 'count', 'total'
    ];

    public function setFreeAttribute($value)
    {
        $this->attributes['free'] = intval($value);
    }

    public function places() {
        return $this->hasMany(Places::class, 'color_id', 'id');
    }

}
