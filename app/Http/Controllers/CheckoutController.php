<?php

namespace App\Http\Controllers;

use App\Concert;
use App\Helper\Cart;
use App\Orders;
use App\Places;
use Illuminate\Http\Request;
use App\Payment\DnkLiqPay;
use Illuminate\Support\Facades\Redirect;

class CheckoutController extends Controller
{
    private $public_key = "sandbox_i77115057653"; // sandbox_i94819305404
    private $private_key = "sandbox_qVvPwQCo3lg4sH96HtnsTQmXdOfBiiWrpeX7Lm0g"; // sandbox_h4SobNe7ZPqhe6Zy4uj0KYEJdxCZnCPO5ZxDswFN

    public function index($id)
    {
        $concert = Concert::findOrFail($id)->get()->first();
        $time_status = Cart::getTime();

        $totals = [];
        $params = [];
        $places = null;
        if (Cart::isHasCart()) {
            $cart = Cart::getCart();
            $order_id = $cart['order'];
            $order = Orders::findOrFail($order_id);

            $tickets = $order->tickets()->get();

            $totalPrice = $tickets->sum(function ($ticket) {
                return $ticket->places->price;
            });

            $totals = [
                'total' => $totalPrice,
                'qty' => $tickets->count()
            ];


            $payment = new DnkLiqPay($this->public_key, $this->private_key);
            $params = [
                'action' => 'pay',
                'amount' => $totalPrice,
                'currency' => $payment::CURRENCY_UAH,
                'description' => $concert->name. ' x'. $tickets->count(),
                'order_id' => (string) $order->id,
                'version' => 3,
                'result_url' => route('checkout'),
            ];

            $pay_form = $payment->cnb_form($params);
            return view('layouts.order-ticket.checkout', [
                'concert' => $concert,
                'order_id' => $order->id,
                'timer' => $time_status['time'],
                'tickets' => $tickets,
                'totals' => $totals,
                'checkout' => true,
                'pay_form' => $pay_form,
            ]);

        } else {
            abort(404);
        }


    }

    public function payment(Request $request) {
        $status = 'error';
        if($request->ajax() && $request->get('data')) {
            $data = json_decode($request->get('data'));
            $status = 'success';

        }
        return response()->json(['status' => $status]);
    }

    public function checkout(Request $request)
    {
        if ($request->get('data') && $request->get('signature')) {
            $data_hash = $request->get('data');
            $signature = $request->get('signature');

            $payment = new DnkLiqPay($this->public_key, $this->private_key);
            // dump($_GET);
            // dump($_POST);
            $signature_check = $payment->str_to_sign($this->private_key . $data_hash . $this->private_key);
//            dump($signature);
            if ($signature === $signature_check) {
                $data = json_decode(base64_decode($data_hash));
                // dump(base64_decode($data_hash));
                // dump($data);
                $status = $this->getPaymentStatus($data);
                // dump($status);
                return $status == null ? view('layouts.checkout.errors', ['status' => $status]) : view('layouts.checkout.checkout');
            }
        }
        return abort(404);
    }

    private function getPaymentStatus($data) {
        $results = null;
        if ($data) {
            $status = $data->status;
            if($status == 'failure' || $status == 'error') {
                $results['err_code'] = $data->err_code;
                $results['err_decription'] = $data->err_description;
            }
            if($status == 'success'){
                Orders::find($data->order_id)->update(['fee_id' => 3]);
            }
        }

        return $results;
    }

}

/*
+  +"payment_id": 1461495270
+  +"action": "pay"
+  +"status": "success"
+  +"version": 3
+  +"type": "buy"
+  +"paytype": "privat24"
+  +"public_key": "sandbox_i77115057653"
+  +"acq_id": 414963
+  +"order_id": "1000079"
+  +"liqpay_order_id": "TVCQ7SZT1603828619148195"
+  +"description": "324325 x1"
+  +"sender_phone": "380506621254"
+  +"sender_first_name": "Андрей"
+  +"sender_last_name": "Новосад"
+  +"sender_card_mask2": "414962*91"
+  +"sender_card_bank": "pb"
+  +"sender_card_type": "visa"
+  +"sender_card_country": 804
+  +"amount": 11.0
+  +"currency": "UAH"
+  +"sender_commission": 0.0
+  +"receiver_commission": 0.3
+  +"agent_commission": 0.0
+  +"amount_debit": 11.0
+  +"amount_credit": 11.0
+  +"commission_debit": 0.0
+  +"commission_credit": 0.3
+  +"currency_debit": "UAH"
+  +"currency_credit": "UAH"
+  +"sender_bonus": 0.0
+  +"amount_bonus": 0.0
+  +"mpi_eci": "7"
+  +"is_3ds": false
+  +"language": "ru"
+  +"create_date": 1603828597535
+  +"end_date": 1603828619208
+  +"transaction_id": 1461495270
+  */
