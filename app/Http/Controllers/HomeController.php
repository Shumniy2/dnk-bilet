<?php

namespace App\Http\Controllers;

use App\City;
use App\Colors;
use App\Concert;
use App\Helper\Helper;
use Geocoder\Model\AdminLevel;
use Geocoder\Query\ReverseQuery;
use Illuminate\Http\Request;
use Geocoder\Model\AddressBuilder;
use Geocoder\Provider\MapQuest\MapQuest;
use Geocoder\Query\GeocodeQuery;
use PHPUnit\Util\Color;

class HomeController extends Controller
{
    private const MAX_PAGE = 12;
    private const API_KEY = "nzQmkWoaw9njB58AuLIRzAFQ33VwcYTq";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if ($request->ajax() && $request->input('page')) {
            $concerts = Concert::paginate(self::MAX_PAGE);
            return view('layouts.concerts_ajax', ['concerts' => $concerts]);
        }
        $concerts = Concert::take(self::MAX_PAGE)->get();
        return view('home.index', ['concerts' => $concerts]);
    }

    public function search(Request $request)
    {
        $html = "";
        $query = $request->input('query');

        if ($request->ajax()) {
            if ($query) {
                $search = Concert::where("name", 'like', $query . '%')->get();
                if ($search->isNotEmpty()) {
                    $html = view('layouts.concerts_ajax', ['concerts' => $search])->render();
                }
            } else {
                $concerts = Concert::take(self::MAX_PAGE)->get();
                $html = view('layouts.concerts_ajax', ['concerts' => $concerts])->render();
            }
        }
        return response()->json([$html]);
    }



    public function ticket()
    {
        return view('layouts.ticket');
    }

    public function concert_id($id)
    {
        $concert = Concert::find($id);
        $text_hours = '';
        if($concert->hours == 1){
            $text_hours = 'час';
        }else if($concert->hours >= 2 && $concert->hours <= 4){
            $text_hours = 'часа';
    }else if($concert->hours >= 5 && $concert->hours <= 10){
            $text_hours = 'часов';
        }
        return view('layouts.concert_id',['id' => $id,'concert' => $concert,'text_hours' => $text_hours]);
    }

    public function cashier(Request $request)
    {

        $cities = City::has('addresses')->get();
       $locale = Helper::getDefaultLocale();
        return view('home.cashier', ['cities' => $cities,'locale' => $locale]);
    }

    public function about()
    {
        return view('home.about');
    }

    public function contacts()
    {
        return view('home.contacts');
    }


    public function admin()
    {
        return view('layouts.voyager_admin.voyager_index');
    }
}
