<?php

namespace App\Http\Controllers;

use App\Concert;
use App\Helper\Cart;
use App\Helper\Helper;
use App\Orders;
use App\Places;
use App\Sections;
use Illuminate\Http\Response;

class ConcertScene extends Controller
{
    public function order_ticket($id)
    {
        $totals = [];
        $tickets = [];
        $order_id = null;

        $concert = Concert::findOrFail($id);
        $slug = $concert->cultures()->first()->slug;
        $colors = $concert->colors()->get();
        $colors = $this->countFreePlaces($colors);
        $template = Helper::getDKSlug($slug);
        $sections = $concert->cultures->sections;
        foreach($sections as $section) {
            $count = $section->places()->where('engaged', 0)->count();
            $sections_free[$section->slug] = $count;
        }

        if(Cart::isHasCart())
        {
            $cart = Cart::getCart();
            $order_id = $cart['order'];
            $order = Orders::findOrFail($order_id);

            $order_id = $order->id;

            $tickets = $order->tickets()->get();

            $totalPrice = $tickets->sum(function ($ticket) {
                return $ticket->places->price;
            });

            $totals = [
                'total' => $totalPrice,
                'qty' => $tickets->count()
            ];
        }


        return view('layouts.order-ticket.scene',[
            'concert' => $concert,
            'order_id' => $order_id,
            'colors' => $colors,
            'settings' => $template['params']['settings'],
            'data' => $template['params']['places'],
            'template' => $template['file'],
            'tickets' => $tickets,
            'totals' => $totals,
            'sections' => $sections,
            'sections_free' => $sections_free,
        ]);

    }



    private function countFreePlaces($data) {
        foreach($data as $key => $item) {
            $color_id = $item->id;
            $count = Places::where([
                ['color_id', $color_id],
                ['engaged', 0]
            ])->count();

            $item->free = $count;
        }
        return $data;
    }
}
