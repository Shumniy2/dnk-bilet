<?php

namespace App\Http\Controllers;

use App\Concert;
use App\Fees;
use App\Helper\Cart;
use App\Helper\ColorsHelper;
use App\Jobs\PlaceTimer;
use App\Orders;
use App\Places;
use App\Shipping;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;


class CartController extends Controller
{

//    public function reservePlace(Request $request)
//    {
//        $cart_totals = [];
//        $id = $request->get('id');
//        if ($id) {
//            $place = Places::findOrFail($id);
//            if (!Cart::isHasCart()) {
//                $concert_id = $place->concerts->id;
//                Cart::addCart($place, $id);
//                $cart = Cart::getCart();
//                $cart_totals = [
//                    'qty' => count($cart['tickets']),
//                    'total' => $cart['total']
//                ];
//
//                $html = view('layouts.order-ticket.cart', [
//                    'concert_id' => $concert_id,
//                    'place' => $place,
//                    'cart_totals' => $cart_totals
//                ])->render();
//
//                $cart_totals['cart'] = $html;
//                $cart_totals['new'] = true;
//
//                $this->addToQueue($id, $concert_id);
//                $this->updatePlaceExpire($place);
//
//                $order = Orders::create([
//                    'concert_id' => $concert_id,
//                    'fee_id' => Fees::waiting(),
//                    'shipping_id' => Shipping::internet()
//                ]);
//
//
//                $order->tickets()->create([
//                    'ticket' => 'test',
//                    'price' => 100,
//                    'print' => 'ewtw',
//                    'code' => 232
//                ]);
//
//                $order->save();
//
//            } else {
//                Cart::addCart($place, $id);
//                $this->updatePlaceExpire($place);
//                $html = view('layouts.cart.cart_block', ['place' => $place])->render();
//            }
//
//
//            if (empty($cart_totals)) {
//                $cart = Cart::getCart();
//                $cart_totals = [
//                    'cart' => $html,
//                    'qty' => count($cart['tickets']),
//                    'total' => $cart['total']
//                ];
//            }
//
//        }
//
//        $time_status = Cart::getTime();
//        if($time_status) {
//            return response()->json($cart_totals)->withCookie(cookie('timer', $time_status['time'], Cart::TIMER, null, null, false, false));
//        } else {
//            return response()->json($cart_totals);
//        }
//    }

    public function reservePlace(Request $request)
    {
        $cart_totals = [];
        $id = $request->get('id');

        if ($id) {
            $place = Places::findOrFail($id);
            $concert_id = $place->concerts->id;
            if (!Cart::isHasCart()) {

                $order = Orders::create([
                    'concert_id' => $concert_id,
                    'fee_id' => Fees::waiting(),
                    'shipping_id' => Shipping::internet()
                ]);

                $order->tickets()->create([
                    'place_id' => $place->id,
                    'print' => $place->row . 'р ' . $place->num_place,
                    'code' => 'D100'
                ]);


                $cart_totals['new'] = true;

                $this->addToQueue($order->id, $concert_id);
                $this->updatePlaceExpire($place);

                Cart::addCart($order->id);
            } else {
                $cart = Cart::getCart();
                $order_id = $cart['order'];

                $order = Orders::findOrFail($order_id);

                $order->tickets()->create([
                    'place_id' => $place->id,
                    'print' => $place->row . 'р ' . $place->num_place,
                    'code' => 'D100'
                ]);


                $this->updatePlaceExpire($place);
            }
        }

        $tickets = $order->tickets()->get();

        $totalPrice = $tickets->sum(function ($ticket) {
            return $ticket->places->price;
        });

        $concert = Concert::findOrFail($concert_id);

        $html = view('layouts.order-ticket.cart', [
            'concert' => $concert,
            'order_id' => $order->id,
            'tickets' => $tickets,
            'totals' => [
                'total' => $totalPrice,
                'qty' => $tickets->count()
            ]
        ])->render();

        $cart_totals['cart'] = $html;

        $time_status = Cart::getTime();
        if ($time_status) {
            return response()->json($cart_totals)->withCookie(cookie('timer', $time_status['time'], Cart::TIMER, null, null, false, false));
        } else {
            return response()->json($cart_totals);
        }
    }


    private function addToQueue($order_id, $concert_id) {
        $expires = now()->addMinutes(Cart::TIMER);
        $expires_str = $expires->toDateTimeString();

        $tickets = Orders::findOrFail($order_id)->tickets()->get();
        foreach($tickets as $ticket) {
            $ticket->places() ->where([
                ['concert_id', $concert_id],
                ['engaged', 1],
                ['expires', $expires_str]
            ])->get();
        }

        $place_timer = (new PlaceTimer($order_id, $expires_str, $concert_id))->delay($expires);
        dispatch($place_timer);
        Cart::saveTime($expires_str);
    }

    private function updatePlaceExpire($place) {
        $expires = Cart::getTimer();
        $place->engaged = 1;
        $place->expires = $expires;
        $place->save();
    }


    public function removeTicket(Request $request) {
        $params = [];
        $tickets = [];
        $totals = [];
        $is_empty = false;

        $concert_id = $request->get('concert');
        $place_id = $request->get('id');

        if ($request->ajax() && $concert_id && $place_id) {
            if (Cart::isHasCart()) {
                $concert = Concert::findOrFail($concert_id);
                $cart = Cart::getCart();
                $order_id = $cart['order'];

                $order = Orders::findOrFail($order_id);

                $ticket = $order->tickets()->where('place_id', $place_id)->first();

                $place = $ticket->places()->first();

                $place->engaged = 0;
                $place->expires = $place->created_at;
                $place->save();

                if ($order->tickets->count() > 1) {
                    $ticket->delete();

                    $tickets = $order->tickets()->get();

                    $totalPrice = $tickets->sum(function ($ticket) {
                        return $ticket->places->price;
                    });

                    $colors = ColorsHelper::getOldColor([$place_id]);
                    $params['colors'] = $colors;

                    $totals = [
                        'total' => $totalPrice,
                        'qty' => $tickets->count()
                    ];

                } else {
                    $params['empty'] = true;
                    $order->delete();
                    Cart::clearSession();
                }

                $html = view('layouts.order-ticket.cart', [
                    'concert' => $concert,
                    'order_id' => $order->id,
                    'tickets' => $tickets,
                    'totals' => $totals
                ])->render();

                $params['cart'] = $html;

            } else {
                $is_empty = true;
            }
        }

        $response = Response::json($params);

        if($request->hasCookie('timer') && $is_empty) {
            $response->withCookie(cookie('timer', 0, -1));
        }

        return $response;
    }


    public function clearCart(Request $request)
    {
//        $concert_id = $request->get('id');
        $colors = [];
        if(Cart::isHasCart()) {
            $cart = Cart::getCart();
            $order_id = $cart['order'];

            $order = Orders::findOrFail($order_id);

            foreach($order->tickets()->get() as $tickets) {
                foreach($tickets->places()->get() as $place) {
                    $place->engaged = 0;
                    $place->expires = $place->created_at;
                    $place->save();
                }
            }

            $order->delete();
            Cart::clearSession();

//            $colors = ColorsHelper::getOldColor($tickets);
        }

        $html = view('layouts.order-ticket.cart')->render();


        $response = Response::json([
            'html' => $html,
            'colors' => $colors
        ]);

        if($request->hasCookie('timer')) {
            $response->withCookie(cookie('timer', 0, -1));
        }

        return $response;

    }


    public function status()
    {
        $time_status = Cart::getTime();
        if($time_status) {
            return response()->json($time_status['status'])->withCookie(cookie('timer', $time_status['time'], 1, null, null, false, false));
        } else {
            return response()->json($time_status['status']);
        }

    }
}
