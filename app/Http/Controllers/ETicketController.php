<?php

namespace App\Http\Controllers;

use App\Address;
use App\ETicket;
use App\Helper\Helper;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;

class ETicketController extends Controller
{
    public function index(Request $request)
    {
        $etickets = ETicket::first();
        if(empty($etickets)){
            return view('layouts.etickets.ticket');
        }else{
            $eticket = Helper::getEticketLocalization($etickets);
        }
        return view('layouts.etickets.ticket',['eticket' => $eticket]);
    }

    public function create()
    {
        $tickets = ETicket::first();
        return view('layouts.etickets.ticket_create',['tickets' => $tickets]);
    }

    public function store(Request $request)
    {
//        ETicket::create(['title_ru' => $request->title_ru, 'text_ru' => $request->text_ru,'title_uk' => $request->title_uk,'text_uk' => $request->text_uk,'title_en' => $request->text_en]);
//        return redirect('create_eticket');
        $request->validate([
            'title_ru' => 'required',
            'text_ru' => 'required',
            'title_uk' => 'required',
            'text_uk' => 'required',
            'title_en' => 'required',
            'text_en' => 'required',
        ]);

        $ticket = ETicket::create([
            'title_ru' => $request->get('title_ru'),
            'text_ru' => $request->get('text_ru'),
            'title_uk' => $request->get('title_uk'),
            'text_uk' => $request->get('text_uk'),
            'title_en' => $request->get('title_en'),
            'text_en' => $request->get('text_en'),
        ]);

        $ticket->save();
        return redirect('create_eticket');
    }

    public function update($id,Request $request)
    {

        $request->validate([
            'title_ru' => 'required',
            'text_ru' => 'required',
            'title_uk' => 'required',
            'text_uk' => 'required',
            'title_en' => 'required',
            'text_en' => 'required',
        ]);

        $eticket = ETicket::find($id);

        $eticket->title_ru = request('title_ru');
        $eticket->text_ru = request('text_ru');
        $eticket->title_uk = request('title_uk');
        $eticket->text_uk = request('text_uk');
        $eticket->title_en = request('title_en');
        $eticket->text_en = request('text_en');

        $eticket->save();

        return redirect('create_eticket');
    }

}
