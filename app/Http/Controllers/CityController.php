<?php

namespace App\Http\Controllers;

use App\City;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::latest()->paginate(5);

        return view('cities.index',['cities' => $cities])
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'city_ru' => 'required',
            'city_uk' => 'required',
            'city_en' => 'required',
        ]);

        $city = City::create([
            'city_ru' => $request->get('city_ru'),
            'city_uk' => $request->get('city_uk'),
            'city_en' => $request->get('city_en'),
        ]);

        $city->save();
        $html = view('cities.city_row', ['city' => $city])->render();
        return response()->json($html);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        return view('cities.show',compact('city'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        return view('cities.edit',compact('city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'city_ru' => 'required',
            'city_uk' => 'required',
            'city_en' => 'required',
            'id' => 'required',
        ]);

        $city = tap(City::where('id', $request->get('id')))
            ->update([
                'city_ru' => $request->get('city_ru'),
                'city_uk' => $request->get('city_uk'),
                'city_en' => $request->get('city_en'),
            ])->first();

        $data = [
            'id' => $city->id,
            'city_ru' => $city->city_ru,
            'city_uk' => $city->city_uk,
            'city_en' => $city->city_en,

        ];

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $city = City::find($id);
        $city->delete();

        return redirect('admin/cities');
    }
}
