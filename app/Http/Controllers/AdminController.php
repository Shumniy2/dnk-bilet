<?php

namespace App\Http\Controllers;

use App\Address;
use App\Concert;
use App\Culture;
use App\ETicket;
use App\Groups;
use App\Orders;
use App\Organizators;
use App\Roles;
use App\User;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $cultures = Culture::all();
        $organizators = Organizators::all();
        return view('concerts.create', [
            'cultures' => $cultures,
            'organizators' => $organizators
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param \App\Concert $concert
     * @return \Illuminate\Http\Response
     */
    public function show(Concert $concert)
    {
        return view('concerts.show', compact('concert'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Concert $concert
     * @return \Illuminate\Http\Response
     */
    public function edit(Concert $concert)
    {
        return view('concerts.edit', compact('concert'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Concert $concert
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Concert $concert)
    {
        $image = $request->file('image');
        if ($image && isset($image)) {
            $file = $image->store('imagesGoods', 'public');
            $name = basename($file);
        }
        $concert->update($request->all());

        return redirect()->route('concerts.index')
            ->with('success', 'Concert updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Concert $concert
     * @return \Illuminate\Http\Response
     */
    public function destroy(Concert $concert)
    {
        $concert->delete();

        return redirect()->route('concerts.index')
            ->with('success', 'Concert deleted successfully');
    }

    public function users()
    {
        $users = User::all();
        $roles = Roles::all();
        $organizators = Organizators::all();
        $groups = Groups::all();
        return view('layouts.users.index',['users' => $users,'roles' => $roles,'organizators' => $organizators,'groups' => $groups]);
    }

    public function create_user()
    {
        $roles = DB::table('roles')->get();
        $groups = DB::table('groups')->get();
        $organizators = DB::table('organizators')->get();
        $users = DB::table('users')->get();
        return view('layouts.users.create',['users' => $users,'roles' => $roles,'groups' => $groups,'organizators' => $organizators]);
    }

    public function store_user(Request $request)
    {
//        $request->validate([
//            'name' => 'required',
//            'email' => 'required',
//            'role_id' => 'required',
//            'password' => 'required',
//            'group_id' => 'required',
//            'organizator_id' => 'required',
//            'phone' => 'required',
//            'login' => 'required'
//        ]);

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'role_id'=>$request->get('role_id'),
            'password' => Hash::make($request->get('password')),
            'group_id'=>$request->get('group_id'),
            'organizator_id'=>$request->get('organizator_id'),
            'phone'=>$request->get('phone'),
            'login'=>$request->get('login')
        ]);
        $user->save();
        $html = view('layouts.users.user_row', ['user' => $user])->render();
        return response()->json($html);
    }

    public function edit_user(User $users,$id)
    {
        $users = DB::table('users')->find($id);
        $roles = DB::table('roles')->get();
        $groups = DB::table('groups')->get();
        $organizators = DB::table('organizators')->get();
        return view('layouts.users.edit',['users' => $users,'roles' => $roles,'groups' => $groups,'organizators' => $organizators]);
    }

    public function update_user(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'role_id' => 'required',
            'group_id' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'login' => 'required',
            'organizator_id' => 'required',
            'id' => 'required',
        ]);

        $user = tap(User::where('id', $request->get('id')))
            ->update([
                'name' => $request->get('name'),
                'role_id' => $request->get('role_id'),
                'group_id' => $request->get('group_id'),
                'phone' => $request->get('phone'),
                'email' => $request->get('email'),
                'login' => $request->get('login'),
                'organizator_id' => $request->get('organizator_id'),
            ])->first();

        $data = [
            'id' => $user->id,
            'name' => $user->name,
            'role_id' => $user->roles->role,
            'group_id' => $user->groups->name,
            'phone' => $user->phone,
            'email' => $user->email,
            'login' => $user->login,
            'organizator_id' => $user->organizers->name,
        ];

        return response()->json($data);
    }

    public function destroy_user(User $user,$id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('admin/users');
    }

    public function orders()
    {
        $orders = Orders::all();
        return view('layouts.orders.index',['orders' => $orders]);
    }
}
