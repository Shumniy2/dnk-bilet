<?php

namespace App\Http\Controllers;

use App\Address;
use App\City;
use App\Concert;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::all();
        $addresses = Address::latest()->paginate(5);

        return view('addresses.index',['addresses' => $addresses,'cities' => $cities])
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addresses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'city_id' => 'required',
            'address_ru' => 'required',
            'text_ru' => 'required',
            'address_uk' => 'required',
            'text_ua' => 'required',
            'address_en' => 'required',
            'text_en' => 'required',
        ]);

        $address = Address::create([
            'city_id' => $request->get('city_id'),
            'address_ru' => $request->get('address_ru'),
            'text_ru' => $request->get('text_ru'),
            'address_uk' => $request->get('address_uk'),
            'text_ua' => $request->get('text_ua'),
            'address_en' => $request->get('address_en'),
            'text_en' => $request->get('text_en'),
        ]);

        $address->save();

        $html = view('addresses.address_row', ['address' => $address])->render();

        return response()->json($html);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function show(Address $address)
    {
        return view('addresses.show',compact('address'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function edit(Address $address)
    {
        return view('addresses.edit',compact('address'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'address_ru' => 'required',
            'text_ru' => 'required',
            'address_uk' => 'required',
            'text_ua' => 'required',
            'address_en' => 'required',
            'text_en' => 'required',
            'id' => 'required',
        ]);

        $address = tap(Address::where('id', $request->get('id')))
            ->update([
                'address_ru' => $request->get('address_ru'),
                'text_ru' => $request->get('text_ru'),
                'address_uk' => $request->get('address_uk'),
                'text_ua' => $request->get('text_ua'),
                'address_en' => $request->get('address_en'),
                'text_en' => $request->get('text_en'),
            ])->first();

        $data = [
            'id' => $address->id,
            'address_ru' => $address->address_ru,
            'text_ru' => $address->text_ru,
            'address_uk' => $address->address_uk,
            'text_ua' => $address->text_ua,
            'address_en' => $address->address_en,
            'text_en' => $address->text_en,

        ];

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function destroy(Address $address,$id)
    {
        $address = Address::find($id);
        $address->delete();

        return redirect('admin/addresses');
    }
}
