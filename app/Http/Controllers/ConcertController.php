<?php

namespace App\Http\Controllers;

use App\City;
use App\Color;
use App\Colors;
use App\Concert;
use App\ConcertPrices;
use App\Culture;
use App\Helper\Cart;
use App\Helper\ColorsHelper;
use App\Helper\Helper;
use App\Organizators;
use App\Places;
use App\Sections;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ConcertController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organizers = Organizators::all();
        $cultures = Culture::all();
        $concerts = Concert::all();
        $cities = City::all();
        return view('concerts.index', [
            'organizers' => $organizers,
            'cultures' => $cultures,
            'concerts' => $concerts,
            'cities' => $cities
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function create(Request $request)
//    {
////        $colors = Color::orderBy('id', 'desc')->get();
//        $dk = Culture::get();
//        return response()->json(['dk' => $dk]);
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'culture' => 'required',
            'name' => 'required',
            'organizer' => 'required',
            'date' => 'required',
            'city' => 'required',
            'description' => 'required',
            'ticket_order' => 'required',
            'hours' => 'required',
            'minutes' => 'required',
            'image' => 'required'
        ]);


        $image = $request->file('image');
        if ($image && isset($image)) {
            $file = $image->store('imagesGoods', 'public');
            $name = basename($file);
        }

        $concert = Concert::create([
            'name' => $request->get('name'),
            'date' => $request->get('date'),

            'image' => $name,
            'ticket_order' => $request->get('ticket_order'),
            'canceled' => $request->get('canceled'),
            'no_in_shop' => $request->get('no_in_shop'),
            'hide_concert' => $request->get('hide_concert'),
            'pay_for_organizator' => $request->get('pay_for_organizator'),
            'places' => $request->get('places'),
            'archive' => $request->get('archive'),
            'description' => $request->get('description'),
//            'before' => $request->get('before'),
//            'after' => $request->get('after'),
            'hours' => intval($request->get('hours')),
            'minutes' => intval($request->get('minutes')),
            'dk_id' => intval($request->get('culture')),
            'city_id' => intval($request->get('city')),
            'organizer_id' => intval($request->get('organizer')),
        ]);

        $concert->save();

        $html = view('concerts.concert_row', ['concert' => $concert])->render();

//        return redirect()->route('concerts.index')
//            ->with('success', 'Concert created successfully.');
        return response()->json($html);
    }


    /**
     * Display the specified resource.
     *
     * @param \App\Concert $concert
     * @return \Illuminate\Http\Response
     */
    public function show(Concert $concert)
    {
        return view('concerts.show', compact('concert'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Concert $concert
     * @return \Illuminate\Http\Response
     */
    public function edit(Concert $concert)
    {
        return view('concerts.edit', compact('concert'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Concert $concert
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'date' => 'required',
            'organizer_id' => 'required',
            'city_id' => 'required',
            'dk_id' => 'required',
            'id' => 'required',
        ]);

        $concert = tap(Concert::where('id', $request->get('id')))
            ->update([
                'name' => $request->get('name'),
                'date' => $request->get('date'),
                'organizer_id' => $request->get('organizer_id'),
                'city_id' => $request->get('city_id'),
                'dk_id' => $request->get('dk_id'),
            ])->first();

        $data = [
            'id' => $concert->id,
            'name' => $concert->name,
            'date' => $concert->date,
            'organizer_id' => $concert->organizers->name,
            'city_id' => $concert->cities->city_ru,
            'dk_id' => $concert->cultures->name,
        ];

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Concert $concert
     * @return \Illuminate\Http\Response
     */
    public function destroy(Concert $concert,$id)
    {
        $concert = Concert::find($id);
        $concert->delete();

        return redirect('admin/concerts');
    }

    public function prices($id)
    {
        session()->put('currentConcert', $id);

        $data = Helper::getDkMachineSettings();

        $concert = Concert::findOrFail($id)->with(['colors', 'places'])->get()->first();

        return view('concerts.prices', [
            'settings' => $data['settings'],
            'colors' => $concert->getRelation('colors'),
            'places' => $concert->getRelation('places'),
            'data' => $data['places']
        ]);

    }

    public function getPlaces(Request $request)
    {
        $res = [];
        if ($request->get('places') && $request->get('id')) {
            $id = intval($request->get('id'));
            if ($id) {
                $concert = Concert::findOrFail($id);
                foreach ($concert->places()->get() as $place) {
                    $place->section_id = $place->sections()->first()->slug;
                    $place->price = $place->colors()->first()->price;
                    $place->color = $place->colors()->first()->color;
                    $res[] = $place->toArray();
                }
            }
        }

        return response()->json($res);
    }

    public function savePlaces(Request $request)
    {
        $data = ($request->json())->all();
        $id = session()->get('currentConcert');
        if ($id) {
            $concert = Concert::findOrFail($id);

            foreach ($data['colors'] as $color) {
                $colors = $concert->colors()->updateOrCreate(['color' => $color['color'], 'concert_id' => $id], [
                    'price' => $color['price'],
                    'color' => $color['color'],
                    'count' => $color['count'],
                    'total' => $color['total']
                ]);
            }

            foreach ($data['places'] as $place) {
//                $section_id = Culture::findOrFail($id)->sections()->where('slug', $place['section'])->value('id');
                $section_id = $concert->cultures->sections->where('slug', $place['section'])->first()->id;
                $places = $colors->places()->create([
                    'section_id' => $section_id,
                    'row' => $place['row'],
                    'num_place' => $place['place'],
                    'price' => $colors->price,
                    'concert_id' => $id,
                    'expires' => now()->format('Y-m-d H:i:s')
                ]);
            }
            $places->save();
        }
        return response()->json(['status' => 'ok']);
    }

    public function removeColor(Request $request) {
        $status = 'error';
        $id = $request->get('color_id');
        if($id) {
            Colors::findOrFail($id)->delete();
            $status = 'ok';
        }

        return response()->json(['status' => $status]);
    }

//    public function removeTicket(Request $request) {
//        $params = [];
//        $is_empty = false;
//
//        $concert_id = $request->get('concert');
//        $id = $request->get('id');
//
//        if($request->ajax() && $concert_id && $id) {
//            Cart::removeFromCart($id, $concert_id);
//
//            $place = Places::findOrFail($id)->where([
//                ['concert_id', $concert_id],
//                ['engaged', 1]
//            ])->first();
//
//            $place->engaged = 0;
//            $place->expires = $place->created_at;
//            $place->save();
//
//            $colors = ColorsHelper::getOldColor([$id]);
//            $params['colors'] = $colors;
//
//            if (!Cart::isHasCart()) {
//                $html = view('layouts.order-ticket.cart')->render();
//                $params['html'] = $html;
//                $is_empty = true;
//            } else {
//                $cart = Cart::getCart();
//                $tickets = $cart['tickets'];
//                $places = Places::findOrFail($tickets)->where('concert_id', $concert_id);
//
//                $cart_totals = [
//                    'qty' => count($cart['tickets']),
//                    'total' => $cart['total']
//                ];
//
//
//                $html = view('layouts.order-ticket.cart', [
//                    'concert_id' => $concert_id,
//                    'places' => $places,
//                    'cart_totals' => $cart_totals
//                ])->render();
//
//
////                $colors = [];
////                foreach ($places as $place) {
////                    $place->section_id = $place->sections()->first()->slug;
////                    $place->price = $place->colors()->first()->price;
////                    $place->color = $place->colors()->first()->color;
////                    $colors[] = $place->toArray();
////                }
//
//                $params['html'] = $html;
//                $params['update'] = true;
//            }
//        }
//
//
//
//        $response = Response::json($params);
//
//        if($request->hasCookie('timer') && $is_empty) {
//            $response->withCookie(cookie('timer', 0, -1));
//        }
//
//
//        return $response;
//    }
}
