<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ETicket extends Model
{
    protected $fillable = [
        'title_ru', 'text_ru','title_uk','text_uk','title_en','text_en',
    ];
}
