<?php


namespace App\Helper;


use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Jenssegers\Date\Date;

class Helper
{
    public static function getLocalizedConcertTime(string $date)
    {
        $instance = Date::parse($date);
        $res = $instance->format('j F, H:i');
        return $res;
    }

    public static function getCityLocalization($city) {
        $cities = $city->toArray();
        $_session_locale = Helper::getDefaultLocale();
        return $cities['city_'.$_session_locale];
    }

    public static function getAddressLocalization($address) {
        $addresses = $address->toArray();
        $_session_locale = Helper::getDefaultLocale();
        return $addresses['address_'.$_session_locale];
    }

    public static function getAddressTextLocalization($address) {
        $addresses = $address->toArray();
        $_session_locale = Helper::getDefaultLocale();
        return $address['text_'.$_session_locale];
    }

    public static function getEticketLocalization($eticket) {
        $etickets = $eticket->toArray();
        $_session_locale = Helper::getDefaultLocale();
        return [
            'text' => $etickets['text_'.$_session_locale],
            'title' => $etickets['title_'.$_session_locale],
        ];
    }

    public static function getActiveLink(string $route) {
        return (strpos(Route::currentRouteName(), $route) === 0) ? 'active' : '';
    }
    public static function getActiveLocal(string $locale) {
        $_session_locale = Session::get('locale');
        return ($_session_locale === $locale) || (empty($_session_locale) && $locale === 'ru')  ? 'active' : '';
    }

    public static function getDefaultLocale() {
        $locale = Session::get('locale');
        if(empty($locale)){
            $locale = 'ru';
        }
        return $locale;
    }

    public static function getDKSlug(string $slug) {
        $file = null;
        $params = null;
        $dir = "layouts.scene_grids.";

        switch($slug) {
            case 'dk_machine':
                $file = $dir . "dk_machine";
                $params = self::getDkMachineSettings();
                break;
        }

        return ['file' => $file, 'params' => $params];
    }

    public static function getDkMachineSettings()
    {
        $settings = ['height' => 32, 'width' => 61];
        $south = [
            2 => [
                'p' => 6, 'w' => 2, 'n' => 21, 'num' => 1
            ]
        ];

        $parterre_pos = [14, 13, 12, 11, 10, 9, 10, 8, 7, 7, 6, 5, 5, 4, 7, 4];
        $parterre_num = [28, 30, 32, 34, 36, 37, 38, 40, 42, 42, 44, 45, 46, 48, 40, 44];

        for ($i = 0, $k = 2; $i <= 15; $i++, $k++) {
            $p = $parterre_pos[$i];
            $num = $parterre_num[$i];
            if ($i == 15) {
                $parterre[$k] = [
                    'p' => $p, 'h' => $k, 'n' => $p * 2 + $num, 'num' => $num, 'b' => 2
                ];
            } else {
                $parterre[$k] = [
                    'p' => $p, 'h' => $k, 'n' => $p + $num, 'num' => $num
                ];
            }
        }
        $north = [
            58 => [
                'p' => 6, 'w' => 58, 'n' => 16, 'num' => 1, 'row' => 1
            ],
            60 => [
                'p' => 6, 'w' => 60, 'n' => 21, 'num' => 1, 'row' => 2
            ]
        ];

//        $balcony = [1 => 40, 40, 41, 41, 42, 41, 45];
        $balcony = [
            25 => [
                'p' => 4, 'h' => 25, 'n' => 55, 'num' => 42, 'left' => 11, 'right' => 45,
                'left_b' => 4, 'right_b' => 4, 'row_left' => 14
            ],
            [
                'p' => 4, 'h' => 26, 'n' => 55, 'num' => 40, 'left' => 9, 'right' => 46,
                'left_b' => 5, 'right_b' => 5, 'row_left' => 13
            ],
            [
                'p' => 4, 'h' => 27, 'n' => 56, 'num' => 41, 'left' => 9, 'right' => 47,
                'left_b' => 5, 'right_b' => 5, 'row_left' => 13, 'pos' => 'center'
            ],
            [
                'p' => 4, 'h' => 28, 'n' => 55, 'num' => 41, 'left' => 8, 'right' => 47,
                'left_b' => 4, 'right_b' => 5, 'row_left' => 11, 'pos' => 'right'
            ],
            [
                'p' => 4, 'h' => 29, 'n' => 56, 'num' => 42, 'left' => 8, 'right' => 48,
                'left_b' => 4, 'right_b' => 5, 'row_left' => 11, 'pos' => 'left'
            ],
            [
                'p' => 4, 'h' => 30, 'n' => 55, 'num' => 41, 'left' => 7, 'right' => 48,
                'left_b' => 4, 'right_b' => 5, 'row_left' => 10, 'pos' => 'big_center'
            ],
            [
                'p' => 4, 'h' => 31, 'n' => 56, 'num' => 45, 'left' => 8, 'right' => 50,
                'left_b' => 3, 'right_b' => 3, 'row_left' => 10, 'pos' => 'center'
            ]
        ];
        $res['settings'] = $settings;
        $res['places'] = compact('south', 'parterre', 'north', 'balcony');

        return $res;
    }
}