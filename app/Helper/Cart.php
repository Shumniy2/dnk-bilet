<?php


namespace App\Helper;

use App\Places;
use Carbon\Carbon;

class Cart
{
    private static $cart = [];
    const TIMER = 15;
    const DEFAULT_TIMER_LABEL = "15:00";

    public static function isHasCart() {
        return session()->has('cart');
    }

//    public static function addCart($place, $id)
//    {
//        if(isset($place) && isset($id)) {
//            $price = $place->colors->price;
//            if (self::isHasCart()) {
//                self::$cart = self::getCart();
//                self::$cart['tickets'][] = $id;
//                self::$cart['total'] += $price;
//            } else {
//                self::$cart['tickets'][] = $id;
//                self::$cart['total'] = $price;
//            }
//            self::saveCart();
//        }
//    }

    public static function addCart($order_id)
    {
        if (isset($order_id)) {
            self::$cart['order'] = $order_id;
            self::saveCart();
        }
    }

    public static function saveCart($params = null) {
        $cart = isset($params) ? $params : self::$cart;
        session()->put('cart', $cart);
    }


    public static function saveTime($time) {
        session()->put('time', $time);
    }

    public static function getTimer() {
        return session()->get('time');
    }

    public static function getTime() {
        $res = [
            'status' => 'expired',
            'time' => self::DEFAULT_TIMER_LABEL
        ];
        if(session()->has('time')) {
            $finish_time = Carbon::parse(session()->get('time'));
            $now = Carbon::now();
            $seconds = $now->diffInSeconds($finish_time, false);
            if ($seconds > 0) {
                $res['status'] = 'continue';
                $res['time'] = $seconds;
            } else {
                session()->forget('cart');
                session()->forget('time');
            }
        }
        return $res;
    }

    public static function getCart()
    {
        self::$cart = session()->get('cart');
        return self::$cart;
    }

    public static function clearCart($concert_id, $tickets = null) {
        self::unlockPlaces($concert_id, $tickets);
    }

    private static function unlockPlaces($concert_id, $tickets = null) {
        $expire = Cart::getTimer();
        if($tickets) {
            $places = Places::where([
                ['concert_id', $concert_id],
                ['engaged', 1],
                ['expires', $expire]
            ])->findOrFail($tickets);
        } else {
            $places = Places::where([
                ['concert_id', $concert_id],
                ['engaged', 1],
                ['expires', $expire]
            ])->get();
        }

        foreach($places as $place) {
            $place->engaged = 0;
            $place->expires = $place->created_at;
            $place->save();
        }

        self::clearSession();
    }

    public static function removeFromCart($id, $concert_id) {
        $cart = Cart::getCart();
        $tickets = $cart['tickets'];

        if (($key = array_search($id, $tickets)) !== false) {
            unset($tickets[$key]);
            $cart['tickets'] = $tickets;
            if(count($tickets) <= 0) {
                self::clearSession();
            } else {
                $total = 0;
                $places = Places::findOrFail($tickets)->where('concert_id', $concert_id);
                foreach($places as $place) {
                    $total += $place->colors->price;
                }
                $cart['total'] = $total;
                Cart::saveCart($cart);
            }
        }

    }

    public static function clearSession() {
        session()->forget('time');
        session()->forget('cart');
    }

//    private static function clearSession() {
//        session()->forget('time');
//        session()->forget('cart');
//    }
}