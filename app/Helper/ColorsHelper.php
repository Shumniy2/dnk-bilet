<?php


namespace App\Helper;


use App\Places;

class ColorsHelper
{
    public static function getOldColor($tickets) {
        $places = Places::findOrFail($tickets);
        $res = [];
        foreach($places as $place) {
            $place->section_id = $place->sections()->first()->slug;
            $place->price = $place->colors()->first()->price;
            $place->color = $place->colors()->first()->color;
//        $place->engaged = 1;
            $item = $place->toArray();

            $colors['id'] = $item['id'];
            $colors['row'] = $item['row'];
            $colors['num_place'] = $item['num_place'];
            $colors['color'] = $item['color'];
            $colors['price'] = $item['price'];
            $colors['engaged'] = $item['engaged'];
            $colors['section_id'] = $item['section_id'];

            $res[] = $colors;
        }

        return $res;
    }
}