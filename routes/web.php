<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::match(['get','post'], '/', 'HomeController@index')->name('index');
Route::get('/search', 'HomeController@search')->name('search');
Route::get('/cashier', 'HomeController@cashier')->name('cashier');

Route::group(['prefix' => 'admin'], function () {
//    Voyager::routes();
    Route::resource('/concerts','ConcertController')->middleware('admin');
    Route::post('/concerts/store','ConcertController@store')->name('concert_create')->middleware('admin');
    Route::post('/concerts/destroy/{id}','ConcertController@destroy')->name('concert_destroy')->middleware('admin');
    Route::post('/concerts/update','ConcertController@update')->name('concert_update')->middleware('admin');
    Route::resource('cities','CityController')->middleware('admin');
    Route::post('/cities/store','CityController@store')->name('city_store')->middleware('admin');
    Route::post('/cities/destroy/{id}','CityController@destroy')->name('city_destroy')->middleware('admin');
    Route::post('/cities/update','CityController@update')->name('city_update')->middleware('admin');
    Route::resource('addresses','AddressController')->middleware('admin');
    Route::post('/addresses/update','AddressController@update')->name('address_update')->middleware('admin');
    Route::post('/addresses/store','AddressController@store')->name('address_store')->middleware('admin');
    Route::post('/addresses/destroy/{id}','AddressController@destroy')->name('address_destroy')->middleware('admin');
    Route::get('/files','FileController@files')->name('files')->middleware('admin');
    Route::get('/payments','PaymentController@payments')->name('payments')->middleware('admin');
    Route::get('/users/create','AdminController@create_user')->name('users_create')->middleware('admin');
    Route::post('/users/store','AdminController@store_user')->name('users_store')->middleware('admin');
//    Route::get('/users/edit/{id}','AdminController@edit_user')->name('users_edit')->middleware('admin');
    Route::post('/users/update','AdminController@update_user')->name('users_update')->middleware('admin');
    Route::post('/users/destroy/{id}','AdminController@destroy_user')->name('users_destroy')->middleware('admin');
    Route::get('/users','AdminController@users')->name('users')->middleware('admin');

});

Route::get('/create_eticket','ETicketController@create')->name('create')->middleware('admin');
Route::post('/store_eticket','ETicketController@store')->name('store_ticket')->middleware('admin');
Route::get('edit_eticket/{id}','ETicketController@edit')->name('ticket_edit')->middleware('admin');
Route::put('update_eticket/{id}','ETicketController@update')->name('ticket_update')->middleware('admin');
Route::get('locale/{locale}', function ($locale) {
    Session::put('locale', $locale);

    return redirect()->back();
})->name('locale');



Route::get('prices/{id}', 'ConcertController@prices')->name('prices');
Route::post('savePlaces', 'ConcertController@savePlaces')->name('savePlaces');
Route::post('getPlaces', 'ConcertController@getPlaces')->name('getPlaces');

Route::post('removeColor', 'ConcertController@removeColor')->name('removeColor');
Route::post('/removeTicket', 'CartController@removeTicket')->name('removeTicket');

Route::post('reservePlace', 'CartController@reservePlace')->name('reservePlace');
Route::post('clearCart', 'CartController@clearCart')->name('clearCart');
Route::post('processingStatus','CartController@status')->name('processingStatus');

Route::get('calculations/{id}', 'HomeController@calculations')->name('calculations');
Route::get('statistics/{id}', 'HomeController@statistics')->name('statistics');
Route::get('sales/{id}', 'HomeController@sales')->name('sales');
Route::get('report/{id}', 'HomeController@report')->name('report');
Route::get('scene/{id}', 'ConcertScene@order_ticket')->name('order-ticket');
//Route::get('ticket', 'HomeController@ticket')->name('ticket');

Route::get('ticket','ETicketController@index')->name('tickets');
Route::get('concert/{id}', 'HomeController@concert_id')->name('concert-id');

// Payment
//Route::get('checkout/{id}','CheckoutController@index')->name('checkout_id');
Route::get('invoice/{id}','CheckoutController@index')->name('invoice');
Route::match(['get','post'], '/checkout','CheckoutController@checkout')->name('checkout');
Route::post('payment','CheckoutController@payment')->name('payment');
//

Route::get('about','HomeController@about')->name('about');
Route::get('contacts','HomeController@contacts')->name('contacts');



//Route::get('/admin','AdminController@index')->name('admin');
//Route::get('dnk/admin','AdminController@login')->name('dnk_admin');

Route::get('/admin/orders','AdminController@orders')->name('orders')->middleware('admin');
