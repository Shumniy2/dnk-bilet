@extends('layouts.voyager_admin.voyager_layout')

@section('styles')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.css" rel="stylesheet">
@endsection

@section('page_title')
    <p>Адреса</p>
@endsection

@section('button_options')
    <div class="btn-group">
        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">
            <img src="{{asset('img/admin-concerts.png')}}" alt="">
        </button>
        <div class="dropdown-menu">
            <div class="col-12">
                <img src="{{asset('img/add.svg')}}" alt="">
                <button type="button" class="btn" data-toggle="modal" data-target="#addressModal">
                    Новый
                </button>
            </div>
            <div class="col-12">
                <img src="{{asset('img/statistics.svg')}}" alt=""> <span>Отчет</span>
            </div>
            <div class="col-12">
                <img src="{{asset('img/import.svg')}}" alt=""> <span>Импорт</span>
            </div>
            <div class="col-12">
                <img src="{{asset('img/sale.svg')}}" alt=""> <span>Продажи</span>
            </div>
        </div>
    </div>
@endsection


@section('page_subtitle')
    <span>Список адресов</span>
@endsection

@section('table')
    <table class="table table-bordered" id="addresses">
        <tr class="tr-1">
            <th class="tr-1-th-1">#</th>
            <th>Адресс(ru)</th>
            <th>Текст(ru)</th>
            <th>Адрес(ua)</th>
            <th>Текст(ua)</th>
            <th>Адрес(en)</th>
            <th>Текст(en)</th>
            <th>Город</th>
            <th>Действие</th>
        </tr>
        @foreach ($addresses as $address)
            @include('addresses.address_row')
        @endforeach
    </table>
@endsection
@section('modal')
    <div class="modal fade" id="addressModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content admin-user-modal-content">
                <div class="modal-header">
                    Адрес
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="address_create_form">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <select class="mdb-select md-form" id="city_id" name="city_id" required>
                                        <option value="" disabled selected>Город</option>
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}">{{$city->city_ru}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Адрес(ru)</label>
                                        <input type="text" name="address_ru"
                                               class="form-control fw-input-admin qa-phone-field"
                                               required></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content add_concert_admin">
{{--                                        <label for="name">Текст(ru)</label>--}}
                                        <textarea type="text" name="text_ru" id="text_ru"
                                               class="form-control fw-input-admin qa-phone-field"
                                                  required></textarea></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Адрес(ua)</label>
                                        <input type="text" name="address_uk"
                                               class="form-control fw-input-admin qa-phone-field"
                                               required></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content add_concert_admin">
{{--                                        <label for="name">Текст(ua)</label>--}}
                                        <textarea type="text" name="text_ua" id="text_ua"
                                               class="form-control fw-input-admin qa-phone-field"
                                                  required></textarea></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Адрес(en)</label>
                                        <input type="text" name="address_en"
                                               class="form-control fw-input-admin qa-phone-field"
                                               required></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content add_concert_admin">
{{--                                        <label for="name">Текст(en)</label>--}}
                                        <textarea type="text" name="text_en" id="text_en"
                                               class="form-control fw-input-admin qa-phone-field"
                                                  required></textarea></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary admin_cancel_concert" data-dismiss="modal"><img
                                    src="{{asset('img/delete.svg')}}" alt="">Отмена
                        </button>
                        <button type="submit" class="btn admin_create_concert"><img
                                    src="{{asset('img/admin_create_concert.svg')}}" alt="">Сохранить
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addressEditModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content admin-user-modal-content">
                <div class="modal-header">
                    Адрес
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="address_edit_form">
                    <div class="modal-body">
                        <div class="row">
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error fw-focused">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Адрес(ru)</label>
                                        <input type="text" name="address_ru" id="address_ru"
                                               class="form-control fw-input-admin qa-phone-field city-ru-edit"
                                               required value=""></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content add_concert_admin">
{{--                                        <label for="name">Текст(ru)</label>--}}
                                        <textarea type="text" name="text_ru" id="text_ru_edit"
                                               class="form-control fw-input-admin qa-phone-field city-ua-edit"
                                                  required value=""></textarea></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error fw-focused">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Адрес(ua)</label>
                                        <input type="text" name="address_uk" id="address_uk"
                                               class="form-control fw-input-admin qa-phone-field city-ru-edit"
                                               required value=""></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content add_concert_admin">
{{--                                        <label for="name">Текст(ua)</label>--}}
                                        <textarea type="text" name="text_ua" id="text_ua_edit"
                                               class="form-control fw-input-admin qa-phone-field city-ua-edit"
                                                  required value=""></textarea></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error fw-focused">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Адрес(en)</label>
                                        <input type="text" name="address_en" id="address_en"
                                               class="form-control fw-input-admin qa-phone-field city-ru-edit"
                                               required value=""></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content add_concert_admin">
{{--                                        <label for="name">Текст(en)</label>--}}
                                        <textarea type="text" name="text_en" id="text_en_edit"
                                               class="form-control fw-input-admin qa-phone-field city-ua-edit"
                                                  required value=""></textarea></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <input type="hidden" name="id" id="id">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary admin_cancel_concert" data-dismiss="modal"><img
                                    src="{{asset('img/delete.svg')}}" alt="">Отмена
                        </button>
                        <button type="submit" class="btn admin_create_concert" id="address_edit"><img
                                    src="{{asset('img/admin_create_concert.svg')}}" alt="">Сохранить
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="module" src="{{asset('js/admin/addresses/addresses.js')}}" defer></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.js" defer></script>
@endsection
