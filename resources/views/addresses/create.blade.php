@extends('addresses.layout')

@section('content')
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">

    <!-- подключаем стили Summernote -->

    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.css" rel="stylesheet">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Добавить адресс</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('addresses.index') }}"> назад</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('addresses.store') }}" method="POST">
        @csrf

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <span>Адресс(ru)</span>
                <div class="form-group">
                    <strong>Адресс:</strong>
                    <input type="text" name="address_ru" class="form-control" placeholder="Адрес">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Текст:</strong>
                    <textarea type="text" name="text_ru" id="address_ru" class="form-control" placeholder="Текст"></textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Город:</strong>
                    <select name="city_id">
                        <?php $cities = DB::table('cities')->get();?>
                        @foreach($cities as $city)
                        <option name="city_id" value="{{$city->id}}">{{$city->id}}-{{$city->city_ru}}</option>
                            @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <span>Адресс(ua)</span>
                <div class="form-group">
                    <strong>Адресс:</strong>
                    <input type="text" name="address_ua" class="form-control" placeholder="Адрес">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Текст:</strong>
                    <textarea type="text" name="text_ua" id="address_ua" class="form-control" placeholder="Текст"></textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Город:</strong>
                    <select name="city_id">
                        <?php $cities = DB::table('cities')->get();?>
                        @foreach($cities as $city)
                            <option name="city_id" value="{{$city->id}}">{{$city->id}}-{{$city->city_ru}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <span>Адресс(en)</span>
                <div class="form-group">
                    <strong>Адресс:</strong>
                    <input type="text" name="address_en" class="form-control" placeholder="Адрес">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Текст:</strong>
                    <textarea type="text" name="text_en" id="address_en" class="form-control" placeholder="Текст"></textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Город:</strong>
                    <select name="city_id">
                        <?php $cities = DB::table('cities')->get();?>
                        @foreach($cities as $city)
                            <option name="city_id" value="{{$city->id}}">{{$city->id}}-{{$city->city_ru}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>

    <!-- подключаем bootstrap.js -->

    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

    <!-- подключаем сам summernote -->

    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.js"></script>

    <script>

        $(document).ready(function() {

            $('#address_ru').summernote();
            $('#address_ua').summernote();
            $('#address_en').summernote();

        });

    </script>
@endsection