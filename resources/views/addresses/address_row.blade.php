@if(!empty($address))
    <tr class="table-row" data-city="{{$address->id}}">
        <td class="tr-1-th-1">{{ $address->id }}</td>
        <td data-td="address_ru" data-value="{{ $address->address_ru }}">{{ $address->address_ru }}</td>
        <td data-td="text_ru" data-value="{{ $address->text_ru }}">{!! $address->text_ru !!}</td>
        <td data-td="address_uk" data-value="{{ $address->address_uk }}">{{ $address->address_uk }}</td>
        <td data-td="text_ua" data-value="{{ $address->text_ua }}">{!! $address->text_ua !!}</td>
        <td data-td="address_en" data-value="{{ $address->address_en }}">{{ $address->address_en }}</td>
        <td data-td="text_en" data-value="{{ $address->text_en }}">{!! $address->text_en !!}</td>
        <td data-td="id" data-value="{{ $address->id }}" class="city-id-table">{{ $address->cities->city_ru }}</td>
        <td class="admin_actions">
            <div class="dropdown">
                <button class="btn btn-default" id="dropdownMenu" type="button" data-toggle="dropdown"
                        aria-expanded="false">
                    <b>Управление</b> <img src="{{asset('img/arrow-up.svg')}}" alt=""> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                    <li role="presentation"><img src="{{asset('img/edit.svg')}}" alt="">
                        <button class="btn edit-address" data-toggle="modal" data-target="#addressEditModal" data-id="{{$address->id}}">
                            Редактировать
                        </button>
                    </li>
                    <form action="{{route('address_destroy',$address->id)}}" method="POST">
                        @csrf
                        <li role="presentation"><img src="{{asset('img/delete.svg')}}" alt="">
                            <button type="submit" class="btn">
                                Удалить
                            </button>
                        </li>
                    </form>
                </ul>
            </div>
        </td>
    </tr>
@endif