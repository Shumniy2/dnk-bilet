@extends('layouts.voyager_admin.voyager_layout')

@section('page_title')
    <p>Заказы</p>
@endsection
@section('button_options')
    <div class="btn-group">
        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">
            <img src="{{asset('img/admin-concerts.png')}}" alt="">
        </button>
        <div class="dropdown-menu">
            <div class="col-12">
                <img src="{{asset('img/return.svg')}}" alt=""> <span>Возврат</span>
            </div>
            <div class="col-12">
                <img src="{{asset('img/send.svg')}}" alt=""> <a href=""><span>Отправить</span></a>
            </div>
            <div class="col-12">
                <img src="{{asset('img/history.svg')}}" alt=""> <span>Категория</span>
            </div>
        </div>
    </div>
@endsection

@section('page_subtitle')
    <span>Список заказов</span>
@endsection


@section('table')
    <table class="table table-bordered">
        <tr class="tr-1">
            <th>Код</th>
            <th>Дата</th>
            <th>Концерт</th>
            <th>Организатор</th>
            <th>Реализатор</th>
            <th>Кол.</th>
            <th>Сум.</th>
            <th>Оплата</th>
            <th>Доставка</th>
            <th>Примечание</th>
            <th>Пар</th>
            <th>Управление</th>
        </tr>
        <tr class="tr-1">
            <td><input type="text"></td>
            <td><input type="text"></td>
            <td><input type="text"></td>
            <td><input type="text"></td>
            <td><input type="text"></td>
            <td></td>
            <td></td>
            <td><input type="text"></td>
            <td><input type="text"></td>
            <td><input type="text"></td>
            <td></td>
            <td></td>
        </tr>
        @foreach($orders as $order)
            <tr>
                <td>{{$order->id}}</td>
                <td>{{ \Carbon\Carbon::parse($order->created_at)->format('d.m.Y H:m')}}</td>
                <td>{{$order->concerts->name}}</td>
                <td>{{$order->concerts->organizers->name}}</td>
                <td>{{$order->distributor}}</td>
                <td>{{$order->tickets->count()}}</td>
                <td>{{$order->tickets->sum('total_price')}}</td>
                <td>{{$order->fee->type}}</td>
                <td>{{$order->shipping->type}}</td>
                <td>{{$order->comments}}</td>
            </tr>
            @endforeach
    </table>
@endsection