<div class="menu-footer-mobile">
    <div class="line-1-footer-mobile"></div>
    <div class="container pt-2 pb-2">
        <div class="row">
            <div class="col-lg-12">
                <div class="d-flex flex-column text-center">
                    <div class="footer-mobile-item"><a href="{{url('/')}}">{{ __('home.general_text') }}</a></div>
                    <div class="footer-mobile-item"><a href="{{url('about')}}">{{ __('home.about_text') }}</a></div>
                    <div class="footer-mobile-item"><a href="{{url('cashier')}}">{{ __('home.cashier_text') }}</a></div>
                    <div class="footer-mobile-item"><a href="{{url('ticket')}}">{{ __('home.ticket_text') }}</a></div>
                </div>
            </div>
        </div>
    </div>
</div>