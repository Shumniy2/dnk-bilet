@include('layouts.menu-footer-mobile')
<footer class="dnk-footer pb-4 footer_voyager">
    <div class="container">
        <div class="dnk-footer-block row pt-4">
            <div class="dnk-parts-block col-6 col-sm-4 col-lg-4">
                <span class="dnk-footer-title">{{ __('home.sections_text') }}</span>
                <ul class="dnk-parts">
                    <li><a href="{{url('/')}}">{{ __('home.general_text') }}</a></li>
                    <li><a href="{{url('about')}}">{{ __('home.about_text') }}</a></li>
                    <li><a href="{{url('cashier')}}">{{ __('home.cashier_text') }}</a></li>
                    <li><a href="{{url('ticket')}}">{{ __('home.ticket_text') }}</a></li>
                    <li><a href="img/logo.png" download><img src="{{asset('img/logo_for_partners.svg')}}" alt=""></a>{{ __('home.logo_for_partners') }}</li>
                </ul>
            </div>
            <div class="dnk-contacts-block col-6 col-sm-4 col-lg-4">
                <span class="dnk-footer-title">{{ __('home.contacts_text') }}</span>
                <ul class="dnk-parts">
                    <li>{{ __('home.city_text') }}</li>
                    <li><a href="tel:+ 38 (098)7509888">0987509888</a></li>
                    <li>dnk-bilet@gmail.com</li>
                </ul>
            </div>
            <div class="dnk-social-block col-6 col-sm-4 col-lg-4">
                <span class="dnk-footer-title">{{ __('home.subscribe_text') }}</span>
                <ul class="dnk-parts">
                    <div class="social">
                        <a href="http://facebook/dnk_bilet">
                            <img src="{{asset('img/facebook.svg')}}" alt="">
                        </a>
                        <a href="http://instagram/dnk_bilet">
                            <img src="{{asset('img/instagram.svg')}}" alt="">
                        </a>
                    </div>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12 dnk-pubic-offer">
                <div class="row">
                    <div class="col-12 text-sm-center text-left">
                        <span class="dnk-offert-text"><b>{{ __('home.public_offer_agreement') }}</b></span>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-12 footer-info-block">
                        <div class="footer-text-item text-sm-center text-left">
                            <span class="footer-text">{{ __('home.all_rights_reserved') }}</span>
                        </div>
                        <div class="footer-text-item text-sm-center text-left">
                            <span class="footer-text">{{ __('home.when_copying_material_link_to_the_site_is_required') }}</span>
                        </div>
                        <div class="footer-text-item text-sm-center text-left">
                            <span class="footer-text">{{ __('home.development_of_name_of_company') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>