<div class="header_voyager">
    <div class="container row col-12">
        <div class="col-1 mt-4 offset-1">
            <a href="{{url('/admin/concerts')}}"><span class="{{App\Helper\Helper::getActiveLink('concerts')}}">Концерты</span></a>
        </div>
        <div class="col-1 mt-3">
            <div class="dropdown voyager_header_dropdown">
                <button class="btn btn-default" id="dropdownMenu" type="button" data-toggle="dropdown" aria-expanded="false">
                    <span class="{{App\Helper\Helper::getActiveLink('pages')}}">Страницы</span>
                </button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                    <li role="presentation" class="{{App\Helper\Helper::getActiveLink('users')}}"><a role="menuitem" href="{{url('admin/users')}}">Пользователи</a><i class="fa fa-users" aria-hidden="true"></i></li>
                    <li role="presentation" class="{{App\Helper\Helper::getActiveLink('reports')}}"><a role="menuitem" href="">Отчеты</a><i class="fas fa-chart-bar"></i></li>
                    <li role="presentation" class="{{App\Helper\Helper::getActiveLink('orders')}}"><a role="menuitem" href="{{url('admin/orders')}}">Заказы</a><i class="fas fa-file-alt"></i></li>
                    <li role="presentation" class="{{App\Helper\Helper::getActiveLink('addresses')}}"><a role="menuitem" href="{{url('admin/addresses')}}">Адреса</a><i class="fa fa-address-book" aria-hidden="true"></i></li>
                    <li role="presentation" class="{{App\Helper\Helper::getActiveLink('cities')}}"><a role="menuitem" href="{{url('admin/cities')}}">Города</a><i class="fas fa-city"></i></li>
                    <li role="presentation" class="{{App\Helper\Helper::getActiveLink('ticket')}}"><a role="menuitem" href="{{url('create_eticket')}}">Билет</a><i class="fas fa-ticket-alt"></i></li>
                    <li role="presentation" class="{{App\Helper\Helper::getActiveLink('payments')}}"><a role="menuitem" href="{{url('admin/payments')}}">Платежи</a><i class="fas fa-credit-card"></i></li>
                </ul>
            </div>
        </div>
        <div class="col-1 mt-4">
            <a href="{{url('admin/files')}}"><span class="{{App\Helper\Helper::getActiveLink('files')}}">Файлы</span></a>
        </div>
        <div class="col-1 mt-4">
            <a href="#"><span class="{{App\Helper\Helper::getActiveLink('control')}}">Управление</span></a>
        </div>
        <div class="col-3">

        </div>
        <div class="col-1 mt-4">
            <a href=""><span><img src="{{asset('img/preview.png')}}" alt="">Просмотр</span></a>
        </div>
        <div class="col-2 mt-4">
            <span>{{Auth::user()->roles->role}} <b>{{Auth::user()->name}}</b></span>
        </div>
        <div class="col-1 mt-4">
            <form action="{{ route('logout') }}" method="POST">
                {{ csrf_field() }}
            <span><img src="{{asset('img/exit.png')}}" alt=""><button type="submit">Выйти</button></span>
            </form>
        </div>
    </div>
</div>