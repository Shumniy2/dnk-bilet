<!DOCTYPE html>
<html>
<head>
    <title>Laravel 6 CRUD Application - ItSolutionStuff.com</title>
    <link rel="stylesheet" href="{{asset("css/app.css")}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
          integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
{{--    <link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">--}}
    @yield('styles')
</head>
@include('layouts.voyager_admin.voyager_header')
<body>

<div class="container-fluid voyager_main">
    <div class="col-10 offset-1">
        <div class="row d-flex justify-content-between pt-5">
            <div class="concerts_block_text">
                @yield('page_title')
            </div>
            <div class="admin_concerts mt-4">
                @yield('button_options')
            </div>
            <div class="main_admin_blocks">
                @yield('page_subtitle')
                <div class="row">
                    <div class="col-12">
                        <div class="concerts_block">
                            @yield('table')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@yield('modal')

</body>
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="{{asset('js/admin/input.js')}}" defer></script>
<script type="module" src="{{asset('js/admin/admin_module.js')}}"></script>

@yield('scripts')

@include('layouts.voyager_admin.voyager_footer')
</html>
