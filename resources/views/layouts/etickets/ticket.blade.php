<!doctype html>
<html lang="en">
@extends('layouts.layout')

@section('styles')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
          integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
@endsection
@section('content')
<body>
<div class="container dnk-main" id="ticket">
    <div class=" position-ref full-height">
        <div class="content">
            @if(!empty($eticket))
                <h1 class="title-for-eticket">{{ $eticket['title'] }}</h1>
                <div class="text-for-eticket">{!! $eticket['text'] !!}</div>
                @else
                <div class="row offset-4">
            <span>
             {{ __('home.no_ticket_information') }}
            </span>
                </div>
                @endif
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script src="{{asset('js/menu-mobile.js')}}" defer></script>
@endsection
</body>
</html>