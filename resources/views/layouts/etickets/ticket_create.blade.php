@include('layouts.voyager_admin.voyager_header')
<meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{asset("css/app.css")}}">
<link rel="stylesheet" href="{{asset("css/main.css")}}">
<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
      integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
<title>Laravel</title>
@if(empty($tickets))
    <div class="ticket_create">
        <div class=" position-ref full-height">
            <div class="content">
                <div class="container">
                    <form action=" {{ route('store_ticket') }} " method="POST">
                        <div class="col-12">
                            @csrf
                            <label for="title">Заголовок</label><br />
                            <input type="text" name="title_ru" id="title" required><br />
                            <label for="text">Текст</label><br />
                            <textarea name="text_ru" id="text"></textarea><br />
                        </div>
                        <div class="col-12">
                            <label for="title">Заголовок</label><br />
                            <input type="text" name="title_uk" id="title_uk" required><br />
                            <label for="text_ua">Текст</label><br />
                            <textarea name="text_uk" id="text_uk"></textarea><br />
                        </div>
                        <div class="col-12">
                            <label for="title">Title</label><br />
                            <input type="text" name="title_en" id="title_en" required><br />
                            <label for="text_en">Text</label><br />
                            <textarea name="text_en" id="text_en"></textarea><br />
                            <button type="submit" class="btn admin_create_concert">Добавить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="ticket_create">
        <div class=" position-ref full-height">
            <div class="content">
                <div class="container">
                    <form action=" {{ route('ticket_update',$tickets->id) }} " method="POST">
                        @csrf
                        @method('PUT')
                        <label for="title">Заголовок</label><br />
                        <input type="text" name="title_ru" id="title" value="{{$tickets->title_ru}}" required><br />
                        <label for="text">Текст</label><br />
                        <textarea name="text_ru" id="text">{{ $tickets->text_ru }}</textarea><br />
                        <label for="title">Заголовок</label><br />
                        <input type="text" name="title_uk" id="title_uk" value="{{$tickets->title_uk}}" required><br />
                        <label for="text">Текст</label><br />
                        <textarea name="text_uk" id="text_uk">{{ $tickets->text_uk }}</textarea><br />
                        <label for="title">Title</label><br />
                        <input type="text" name="title_en" id="title_en" value="{{$tickets->title_en}}" required><br />
                        <label for="text">Text</label><br />
                        <textarea name="text_en" id="text_en">{{ $tickets->text_en }}</textarea><br />
                        <button type="submit" class="btn admin_create_concert">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endif
<script src="{{asset("js/app.js")}}" defer></script>
<script type="module" src="{{asset('js/admin/tickets/ticket.js')}}" defer></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.js" defer></script>
@include('layouts.voyager_admin.voyager_footer')

