@php
    $top = 3;
    $balcony_top = 1;
@endphp
<div class="dk_machine_scene mt-5">
    @for($h = 1; $h <= $settings['height']; $h++)
        <div class="row place_row_{{$h}} mx-auto">
            @for($w = 1; $w <= $settings['width']; $w++)
                <div class="dnk-place dnk_tooltip">
                    <!-- South -->
                    @if(isset($data['south'][$w]) && ($h > $data['south'][$w]['p']
                    && $h <= $data['south'][$w]['n'] && $w == $data['south'][$w]['w'] ))
                        <div class="tooltiptext-bubble"
                             data-hover="bubble_south_{{$w - 1}}_{${$data['south'][$w]['num']}}">
                            <span class="tooltiptext tooltiptext-text tooltiptext-bold">Южная ложа</span>
                            <div class="tooltiptext-place-info">
                                <span class="tooltiptext tooltiptext-row-block">Ряд
                                    <span class="tooltiptext tooltiptext-row tooltiptext-bold">{{$w - 1}}</span>
                                </span>

                                <span class="tooltiptext tooltiptext-place-block">Место
                                    <span class="tooltiptext tooltiptext-place tooltiptext-bold">{{$data['south'][$w]['num']}}</span>
                                </span>
                            </div>
                            <span class="tooltiptext tooltiptext-price-block">Цена
                                    <span class="tooltiptext tooltiptext-text tooltiptext-price tooltiptext-bold"></span>
                            </span>
                            <span class="tooltiptext tooltiptext-text tooltiptext-status-info tooltiptext-bold">Не оформлен</span>
                        </div>
                        <div class="dnk_circle dnk_place" data-row="{{$w - 1}}"
                             data-place="{{$data['south'][$w]['num']}}" data-section="south">
                            <span>{{ $data['south'][$w]['num']++ }}</span>
                        </div>

                        <!-- Parther -->
                    @elseif(isset($data['parterre'][$h]) && ($w > $data['parterre'][$h]['p']
                    && $w <= $data['parterre'][$h]['n'] + $top && $h == $data['parterre'][$h]['h'] ))
                        @if($w >= 29 && $w <= 31)
                            @if($w == 30)
                                <div class="dnk_circle dnk_place dnk_transparent">{{ ($h - 1) }}</div>
                            @else
                                <div class="dnk_circle dnk_circle_invisible"></div>
                            @endif
                        @elseif(($h - 1) == 16 && ($w > 7 && $w <= $data['parterre'][$h]['b'] + 7
                        || $w > 50 && $w <= $data['parterre'][$h]['b'] + 50 ))
                            <div class="dnk_circle dnk_circle_invisible"></div>
                        @else
                            <div class="tooltiptext-bubble"
                                 data-hover="bubble_parterre_{{$h - 1}}_{{$data['parterre'][$h]['num']}}">
                                <span class="tooltiptext tooltiptext-text tooltiptext-bold">Партер</span>
                                <div class="tooltiptext-place-info">
                                <span class="tooltiptext tooltiptext-row-block">Ряд
                                    <span class="tooltiptext tooltiptext-row tooltiptext-bold">{{$h - 1}}</span>
                                </span>

                                    <span class="tooltiptext tooltiptext-place-block">Место
                                    <span class="tooltiptext tooltiptext-place tooltiptext-bold">{{$data['parterre'][$h]['num']}}</span>
                                </span>
                                </div>
                                <span class="tooltiptext tooltiptext-price-block">Цена
                                    <span class="tooltiptext tooltiptext-text tooltiptext-price tooltiptext-bold"></span>
                                </span>
                                <span class="tooltiptext tooltiptext-text tooltiptext-status-info tooltiptext-bold">Не оформлен</span>
                            </div>
                            <div class="dnk_circle dnk_place" data-row="{{$h - 1}}"
                                 data-place="{{$data['parterre'][$h]['num']}}" data-section="parterre">
                                <span>{{ $data['parterre'][$h]['num']-- }}</span>
                            </div>
                        @endif
                    @elseif(isset($data['north'][$w]) && ($h > $data['north'][$w]['p']
                    && $h <= $data['north'][$w]['n'] && $w == $data['north'][$w]['w'] ))
                        <div class="tooltiptext-bubble"
                             data-hover="bubble_north_{{$data['north'][$w]['row']}}_{{$data['north'][$w]['num']}}">
                            <span class="tooltiptext tooltiptext-text tooltiptext-bold">Северная ложа</span>
                            <div class="tooltiptext-place-info">
                                <span class="tooltiptext tooltiptext-row-block">Ряд
                                    <span class="tooltiptext tooltiptext-row tooltiptext-bold">{{$data['north'][$w]['row']}}</span>
                                </span>

                                <span class="tooltiptext tooltiptext-place-block">Место
                                    <span class="tooltiptext tooltiptext-place tooltiptext-bold">{{$data['north'][$w]['num']}}</span>
                                </span>
                            </div>
                            <span class="tooltiptext tooltiptext-price-block">Цена
                                    <span class="tooltiptext tooltiptext-text tooltiptext-price tooltiptext-bold"></span>
                            </span>
                            <span class="tooltiptext tooltiptext-text tooltiptext-status-info tooltiptext-bold">Не оформлен</span>
                        </div>
                        <div class="dnk_circle dnk_place" data-row="{{$data['north'][$w]['row']}}" data-test="{{$h}}"
                             data-place="{{$data['north'][$w]['num']}}" data-section="north">
                            <span>{{ $data['north'][$w]['num']++ }}</span>
                        </div>

                    @elseif(isset($data['balcony'][$h]) && ($w > $data['balcony'][$h]['p']
                    && $w <= $data['balcony'][$h]['n'] && $h == $data['balcony'][$h]['h'] ))
                        @if($w >= $data['balcony'][$h]['left'] && $w < $data['balcony'][$h]['left'] + $data['balcony'][$h]['left_b'] ||
                         $w >= $data['balcony'][$h]['right'] && $w <= $data['balcony'][$h]['right'] + $data['balcony'][$h]['right_b'])
                            @if(isset($data['balcony'][$h]['pos']) && ($w == $data['balcony'][$h]['row_left'] - 1 || $w == $data['balcony'][$h]['right'] + 1))
                                @switch($data['balcony'][$h]['pos'])
                                    @case('center')
                                    <div class="dnk_circle dnk_center_fake"></div>
                                    @break
                                    @case('right')
                                    @if($w == $data['balcony'][$h]['row_left'] - 1)
                                        <div class="dnk_circle dnk_right_fake"></div>
                                    @elseif($w == $data['balcony'][$h]['right'] + 1)
                                        <div class="dnk_circle dnk_center_fake"></div>
                                    @endif
                                    @break
                                    @case('left')
                                    @if($w == $data['balcony'][$h]['row_left'] - 1)
                                        <div class="dnk_circle dnk_left_fake"></div>

                                    @endif
                                    @break
                                    @case('big_center')
                                    @if($w == $data['balcony'][$h]['row_left'] - 1)
                                        <div class="dnk_circle dnk_right_fake"></div>
                                    @elseif($w == $data['balcony'][$h]['right'] + 1)
                                        <div class="dnk_circle dnk_center_fake"></div>
                                    @endif
                                    @break
                                @endswitch
                            @elseif($w == $data['balcony'][$h]['row_left'] || $w == $data['balcony'][$h]['right'])
                                <div class="dnk_circle dnk_place dnk_transparent">{{$balcony_top}}</div>
                            @else
                                <div class="dnk_circle dnk_circle_invisible"></div>
                            @endif
                        @else
                            <div class="tooltiptext-bubble"
                                 data-hover="bubble_balcony_{{$balcony_top}}_{{$data['balcony'][$h]['num']}}">
                                <span class="tooltiptext tooltiptext-text tooltiptext-bold">Балкон</span>
                                <div class="tooltiptext-place-info">
                                <span class="tooltiptext tooltiptext-row-block">Ряд
                                    <span class="tooltiptext tooltiptext-row tooltiptext-bold">{{$balcony_top}}</span>
                                </span>
                                    <span class="tooltiptext tooltiptext-place-block">Место
                                    <span class="tooltiptext tooltiptext-place tooltiptext-bold">{{$data['balcony'][$h]['num']}}</span>
                                </span>
                                </div>
                                <span class="tooltiptext tooltiptext-price-block">Цена
                                    <span class="tooltiptext tooltiptext-text tooltiptext-price tooltiptext-bold"></span>
                                </span>
                                <span class="tooltiptext tooltiptext-text tooltiptext-status-info tooltiptext-bold">Не оформлен</span>
                            </div>
                            <div class="dnk_circle dnk_place" data-row="{{$balcony_top}}"
                                 data-place="{{$data['balcony'][$h]['num']}}" data-section="balcony">
                                <span>{{ $data['balcony'][$h]['num']-- }}</span>
                            </div>
                        @endif
                        @if($w == $data['balcony'][$h]['n'])
                            @php $balcony_top++; @endphp
                        @endif
                    @else
                        <div class="dnk_circle dnk_circle_invisible"></div>
                    @endif
                </div>
            @endfor
        </div>
    @endfor
    <div class="dk_machine_overlay">
        <div class="scene-section-block parterre-section-block">
            <div class="parterre-section-text">
                Партер
            </div>
        </div>
        <div class="scene-section-block south-section-block">
            <div class="south-section-text">Ю ж н а я</div>
            <div class="south-section-text mt-2">л о ж а</div>
        </div>
        <div class="scene-section-block north-section-block">
            <div class="north-section-text">С е в е р н а я</div>
            <div class="north-section-text mt-2">л о ж а</div>
        </div>
        <div class="scene-section-block balcony-section-block">
            <div class="balcony-section-text">
                Балкон
            </div>
        </div>

        <div class="scene-section-block additional-south-label-block additional-labels">
            <div class="additional-south-label-text">
                3э
            </div>
        </div>

        <div class="scene-section-block additional-north-fst-label-block additional-labels">
            <div class="additional-north-fst-label-text">
                2э
            </div>
        </div>

        <div class="scene-section-block additional-north-snd-label-block additional-labels">
            <div class="additional-north-snd-label-text">
                3э
            </div>
        </div>
    </div>
</div>
