<table class="table" id="table_tickets">
    <tr class="bg-primary">
        <th>Цена</th>
        <th>Цвет</th>
        <th>Кол</th>
        <th>Сумма</th>
    </tr>
    @foreach($colors as $color)
        <tr class="ticket_block" tabindex="0" data-id="{{$color->id}}">
            <td class="ticket_price" data-price="{{$color->price}}">{{$color->price}}</td>
            <td>
                <div class="ticket_color" data-color="{{$color->color}}"
                     style="background-color: {{$color->color}}"></div>
            </td>
            <td class="ticket_count" data-count="{{$color->count}}">{{$color->count}}</td>
            <td class="ticket_total" data-total="{{$color->total}}">{{$color->total}}</td>
        </tr>
    @endforeach
    <tr class="ticket_total">
        <td>ВАЛ</td>
        <td></td>
        <td data-count="0">0</td>
        <td data-total="0">0</td>
    </tr>
</table>
