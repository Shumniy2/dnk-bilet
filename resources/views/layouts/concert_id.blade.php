@extends('layouts.layout')
@section('content')
<div class="container concert_id">
    <div class="row mt-5">
        <div class="col-12 d-flex flex-row mb-3 name_mobile_concert_id_1">
            <div>
                {{ __('home.general_text') }}
            </div>
            <div class="img-arrow">
                <img src="{{asset('img/right.svg')}}" alt="">
            </div>
            <div>
                {{$concert->name}}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-3">
            <img class="dnk-img-concert_id" src="{{asset("storage/imagesGoods/". $concert->image)}}">
            <div class="col-12">
                <button class="btn-watch-video">
                    <b>{{ __('home.watch_video') }} </b>
                    <img class="pl-3" src="{{asset('img/watch-video.svg')}}" alt="">
                </button>
            </div>
        </div>
        <div class="col-8 concert-info">
            <div class="row">
                <div class="col-12">
                    <span class="concert-name-id">{{$concert->name}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-auto">
                    <img class="img-concert-details" src="{{asset('img/place.svg')}}" alt="">
                </div>
                <div class="col-10">
                    <span>{{$concert->cultures->name}}{{$concert->city}}</span>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-auto">
                    <img class="img-concert-details" src="{{asset('img/calendar.svg')}}" alt="">
                </div>
                <div class="col-10">
                    <span>{!! \App\Helper\Helper::getLocalizedConcertTime($concert->date) !!}</span>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-12">
                    <span> {{ __('home.on_sale') }} <b class="text-important">62 билета</b>  {{ __('home.price') }}  {{ __('home.from') }} {{$concert->before}}  {{ __('home.to') }} {{$concert->after}}  {{ __('home.uah') }}</span>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-12">
                    <span>{{ __('home.concert_duration') }}: <b class="text-important">{{$concert->hours}} {{$text_hours}} {{$concert->minutes}} минут ({{ __('home.with_intermission') }})</b> </span>
                </div>
            </div>
            <ul class="blue-blocks">
                <li class="blue-block">
                    {{ __('home.in_russian') }}
                </li>
                <li class="blue-block">
                    {{ __('home.booking_24_hours') }}
                </li>
                <li class="blue-block">
                    {{ __('home.live_sound') }}
                </li>
                <li class="blue-block">
                    {{ __('home.age12+') }}
                </li>
            </ul>
            <a href="{{route('order-ticket', $concert->id)}}" class="link-dnk-ticket-id">
                <span class="ticket-text">{{ __('home.order_ticket') }}</span>
                <span class="ticket-text-small">Заказать</span>
                <img class="dnk-img-ticket ml-3" src="{{asset("img/ticket.png")}}" alt="">
            </a>
        </div>
        <div class="small-mobile-block">
            <a href="{{url('order-ticket/'.$concert->id)}}">
            <div class="small-mobile">
               <span>Заказать билет</span> <img src="{{asset('img/ticket-2.svg')}}" alt="">
            </div>
            </a>
        </div>
    </div>
    <div class="row mt-4 mb-4">
        <div class="col ">
            <span class="concert-details-description-text">{{ __('home.description') }}</span>
        </div>
    </div>
    <div class="row concert-details-description">
        <div class="col">
            {!!$concert->description!!}
        </div>
    </div>
</div>
    @endsection
@section('scripts')
    <script src="{{asset('js/menu-mobile.js')}}" defer></script>
@endsection