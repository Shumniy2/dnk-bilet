@extends('layouts.order-ticket.layout')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/invoice.css')}}">
@endsection
@section('content')
    <div class="col-lg-12">
        <div class="container pay_order">
            <div class="row ml-5 mt-5">
                <p>{{ __('home.ordering') }}</p>
            </div>
            <div class="row ml-5 select_place">
                <img src="{{asset('img/buy_ticket.svg')}}" alt=""><span>{{ __('home.step_1') }}</span>
            </div>
            <div class="row ml-5 select_place">
                <div class="line-select_place">

                </div>
            </div>
            <div class="row ml-5 select_place">
                <img src="{{asset('img/completed.svg')}}" alt="" class="profile1_svg">
                <img src="{{asset('img/profile.svg')}}" alt=""
                     class="profile_svg"><span>{{ __('home.step_2') }}</span><img src="{{asset('img/edit.svg')}}"
                                                                                  alt="" class="edit">
            </div>
            <div class="row"></div>
            <div class="row select_place-1-form">
                <div class="line-select_place-2"></div>
                <div class="fio">
                    <div class="fw-field-container qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                        <div class="fw-field fw-has-error qa-fw-has-error">
                            <div class="fw-field__content">
                                <label for="name">ФИО</label>
                                <input maxlength="NaN" autocomplete="off" type="text" class="fw-input qa-phone-field"
                                       id="name" required></div>
                        </div>
                        <span class="fw-field__error qa-fw-field__error"></span>
                    </div>
                    <div class="fw-field-container qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                        <div class="fw-field fw-has-error qa-fw-has-error">
                            <div class="fw-field__content">
                                <label for="tel">Телефон</label>
                                <input maxlength="10" autocomplete="off" type="tel" class="fw-input qa-phone-field"
                                       id="tel" required></div>
                        </div>
                        <span class="fw-field__error qa-fw-field__error"></span>
                    </div>
                    <div class="fw-field-container qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                        <div class="fw-field fw-has-error qa-fw-has-error">
                            <div class="fw-field__content">
                                <label for="email">Email</label>
                                <input maxlength="NaN" autocomplete="off" type="email"
                                       class="fw-input qa-phone-field" id="email" required></div>
                        </div>
                        <span class="fw-field__error qa-fw-field__error"></span>
                    </div>
                    {{--                <input type="text" placeholder="ФИО">--}}
                    {{--                <input type="tel" placeholder="Телефон">--}}
                    {{--                <input type="email" placeholder="Email">--}}
                </div>
                <div class="check-name">
                    <div class="container">
                        <div class="row"><span>ФИО</span>
                            <div class="check-name-text-name"></div>
                        </div>
                        <div class="row"><span>Телефон</span>
                            <div class="check-name-text-tel"></div>
                        </div>
                        <div class="row"><span>Email</span>
                            <div class="check-name-text-email"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row ml-5 select_place">
                <div class="line-select_place-4"></div>
                <img src="{{asset('img/completed.svg')}}" alt="" class="delivery1_svg">
                <img src="{{asset('img/car.svg')}}" alt="" class="delivery_svg"><span>{{ __('home.step_3') }}</span><img
                        src="{{asset('img/edit.svg')}}"
                        alt="" class="edit-step-3">
            </div>
            <div class="row select_place-2-form">
                <div class="line-select_place-3"></div>
                <div class="checkbox-step-3">
                    <label class="checkbox-step-3-1"><input id="online_ticket" type="checkbox"
                                                            name="online_ticket"><span>{{ __('home.electronic_ticket') }}</span></label><img
                            src="{{asset('img/info.svg')}}" alt=""><br/>
                    <label class="checkbox-step-3-2"><input id="buy_by_yourself" type="checkbox"
                                                            name="buy_by_yourself"><span>{{ __('home.pickup') }}</span></label>
                </div>
                <p>{{ __('home.payment_in_the_cashier') }}</p>
                <div class="change_cashier">
                    <span>{{ __('home.choose_cashier') }}:</span><br/>
                    <select name="change_address" id="change_address">
                        <option value="change_address">касса Театра оперы и балета, г. Днепр, улица...</option>
                    </select>
                </div>
            </div>
            <div class="row ml-5 select_place pay">
                <img src="{{asset('img/completed.svg')}}" alt="" class="pay1_svg">
                <img src="{{asset('img/cash.svg')}}" alt="" class="pay_svg"><span>{{ __('home.step_4') }}</span>
                {{--            <img src="{{asset('img/edit.svg')}}" alt="" class="edit-pay">--}}
            </div>
            <div class="row select_place-3-form">
                <div class="checkbox-step-4">
                    <label class="checkbox-step-4-1"><input id="card" type="checkbox"
                                                            name="card"><span>{{ __('home.payment_by_cart') }} Visa/MasterCard</span></label><br/>
                    <label class="checkbox-step-4-2"><input id="cash" type="checkbox"
                                                            name="cash"><span>{{ __('home.cash') }}</span></label>
                </div>
                <p>Оставить комментарий к заказу:</p>
                <textarea name="comment" id="comment" placeholder="Комментарий"></textarea>
            </div>
        </div>
    </div>
    <div class="container mobile-cart">
        <div class="row">
            <a href="#" class="btn btn-lg link-dnk-ticket-id-copy link-dnk-order link-dnk-order-copy-mobile disabled">
                <span class="ticket-text">{{ __('home.checkout') }}</span>
            </a>
        </div>
        <div class="row mobile-pay">
            <div class="col-7 mobile-pay-1">
                <img src="{{asset('img/cart-footer.svg')}}" alt="" class="cart-mobile-pay">
                <span>Итого(3)</span>
                <span>3900грн</span>
                <img src="{{asset('img/arrow-up.svg')}}" alt="" class="arrow-mobile-pay">
            </div>
            <div class="col-5 mobile-pay-2">
                <span>Бронь активна</span>
                <span class="mobile-pay-2-time">12:30</span>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="module" src="{{asset('js/modules/Timer.js')}}"></script>
    <script type="module" src="{{asset("js/checkout/checkout.js")}}"></script>
    <script type="module" src="{{asset("js/admin/input.js")}}"></script>
@endsection