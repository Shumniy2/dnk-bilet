 <div class="scene-tablet mx-auto col-12">
     @foreach($sections as $section)
        <div class="row tablet-sector col-12 mx-auto">
            <div class="tablet-sector-block">
                <span>{{$section->name}}</span><br/>
                <span>Свободных мест {{$sections_free[$section->slug]}}</span>
            </div>
            <div class="tablet-sector-block-right">
                <img src="{{asset('img/right.svg')}}" alt="">
            </div>
        </div>
     @endforeach
    </div>
{{-- @section('scripts')--}}
{{--     <script src="{{asset('js/scene/scene.js')}}" defer></script>--}}
{{-- @endsection--}}