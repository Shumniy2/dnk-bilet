<div class="filter_colors_tablet">
    @foreach($colors as $color)
        <div class="dnk_filter_circle">
            <div class="tooltiptext-bubble bubble-filter"
                 data-hover="filter_bubble_{{$color->id}}">
                <div class="tooltiptext tooltip-free">Свободно</div>
                <div class="tooltiptext tooltiptext-bold tooltip-free-price">{{$color->free}}мест
                </div>

            </div>
            <div class="dnk_circle" style="background-color: {{$color->color}}"
                 data-color="{{$color->color}}" data-filter="{{$color->id}}">
            </div>
            <div class="price">{{$color->price}}</div>
        </div>
    @endforeach
    <div class="custom-control custom-switch">
        <input type="checkbox" class="custom-control-input" id="customSwitches">
        <label class="custom-control-label" for="customSwitches">Список</label>
    </div>
    <div class="tablet-timer">
        {{ __('home.reservation_is_active') }}
        <div id="time" class="book-time">15:00</div>
    </div>
</div>