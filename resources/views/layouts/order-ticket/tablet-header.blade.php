<header class="order-ticket-header" id="tablet-header">
    <div class="row col-12">
        <div class="col-4 mt-4">
            <a href="{{url('/')}}">
            <img src="{{asset('img/order-ticket-header.svg')}}" alt="" class="tablet-header-logo">
            </a>
        </div>
        <div class="col-5 mt-4">
                <b>{{$concert->name}}</b>
            <br/>
            <div class="place-order-ticket">
                <img src="{{asset('img/place-order-ticket.svg')}}" alt="" class="order-ticket-img">
                    {{$concert->dk_id}}
            </div>
        </div>
        <div class="col-3 mt-5">
            <div class="date-order-ticket">
                <img src="{{asset('img/calendar-order-ticket.svg')}}" alt="" class="order-ticket-img">
                    {!! \App\Helper\Helper::getLocalizedConcertTime($concert->date) !!}
            </div>
        </div>
    </div>
</header>