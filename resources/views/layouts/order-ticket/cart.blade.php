@if(!empty($tickets) || !empty($tickets))
    <div class="row mt-5 ml-2 col-12">
        {{--        {{ __('home.order') }}: № 1798053604--}}
        {{ __('home.order') }}: № {{$order_id}}
    </div>
    {{--    <div class="scroll-cart">--}}
    {{--        @if(!empty($place))--}}
    {{--            @include('layouts.cart.cart_block')--}}
    {{--        @elseif(!empty($places))--}}
    {{--            @foreach($places as $place)--}}
    {{--                @include('layouts.cart.cart_block')--}}
    {{--            @endforeach--}}
    {{--        @endif--}}
    {{--    </div>--}}

    <div class="scroll-cart">
        @foreach($tickets as $ticket)
            @foreach($ticket->places()->get() as $place)
                @include('layouts.cart.cart_block')
            @endforeach
        @endforeach
    </div>
    <div class="row mt-4">
        <div class="line-block-cart">

        </div>
    </div>
    <div class="row mt-4 col-12">
        <div class="total">
            {{ __('home.total') }}
        </div>
        <div class="total_count">
            ( <span id="cart_qty">{{ $totals['qty'] }}</span> {{ __('home.pc') }}.)
        </div>
        <div class="total_price">
            <span id="cart_total">{{ $totals['total'] }}</span> {{ __('home.uah') }}
        </div>
    </div>
    <div class="row mt-5 text-center">
        <div class="col-lg-12 block-btn-checkout">
            @if(!isset($checkout))
                <a href="{{route('invoice', $concert->id)}}"
                   class="btn btn-lg link-dnk-ticket-id link-dnk-order link-dnk-order-copy">
                    <span class="ticket-text">{{ __('home.checkout') }}</span>
                </a>
            @else
                <button class="btn btn-lg link-dnk-ticket-id link-dnk-order link-dnk-order-copy" id="payment">
                    <span class="ticket-text">{{ __('home.checkout') }}</span>
                </button>
                {!! $pay_form !!}

            @endif
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-12 d-flex justify-content-center">
            @if(!isset($checkout))
                <button class="btn delete_cart" id="clearCart" data-id="{{$concert->id}}">
                    <img src="{{asset('img/delete_cart.svg')}}" alt="">
                    {{ __('home.clear_cart') }}
                </button>
            @else
                <a class="btn" href="{{route('order-ticket', $concert->id)}}">
                    <img src="{{asset('img/arrow-up.svg')}}" alt="">
                    <b>Вернутся к выбору билета</b>
                </a>
            @endif
        </div>
    </div>
@else
    @include('layouts.cart.empty_cart_block')
@endif