@extends('layouts.order-ticket.layout')

@section('content')
    <div class="col-lg-12 dnk-block-scene">
        <div class="row">
            <div class="col-lg-2">
                <div class="filter_block">
                    <img src="{{asset('img/house_order_ticket.svg')}}" alt="" class="house_prices_order">
                    <div class="filter_colors">
                        @foreach($colors as $color)
                            <div class="dnk_filter_circle">
                                <div class="tooltiptext-bubble bubble-filter"
                                     data-hover="filter_bubble_{{$color->id}}">
                                    <div class="tooltiptext tooltip-free">Свободно</div>
                                    <div class="tooltiptext tooltiptext-bold tooltip-free-price">{{$color->free}}мест
                                    </div>

                                </div>
                                <div class="dnk_circle" style="background-color: {{$color->color}}"
                                     data-color="{{$color->color}}" data-filter="{{$color->id}}">
                                </div>
                                <div class="price">{{$color->price}}</div>
                            </div>
                        @endforeach
                        <div class="price_booked">
                            <div class="dnk_circle"></div>
                            {{ __('home.engaged') }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-lg-12 admin-scene mt-5">
                        <div class="row">
                            <div class="col-lg-11 dnk-scene-text-block">
                                <div class="dnk-scene-text">
                                    <h1>{{ __('home.scene') }}</h1>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="scene-title-block">
                                    @include('layouts.scene_grids.dk_machine')
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('layouts.order-ticket.scene-list')
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="module" src="{{asset('js/scene/scene.js')}}" ></script>
@endsection

@section('mobile-footer')
        @include('layouts.order-ticket.filter-colors')
@endsection
