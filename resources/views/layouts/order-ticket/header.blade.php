{{--<header class="order-ticket-header">--}}
{{--    <div class="col-12">--}}
{{--        <div class="row">--}}
{{--        <div class="col-3 col-lg-3 col-md-4 col-sm-4 mt-4">--}}
{{--            <a href="{{url('/')}}">--}}
{{--                <img src="{{asset('img/order-ticket-header.svg')}}" alt="" class="order-ticket-header-logo">--}}
{{--            </a>--}}
{{--        </div>--}}
{{--        <div class="col-3 col-lg-4 col-md-6 col-sm-5 mt-4">--}}
{{--            <b>{{$concert->name}}</b>--}}
{{--            <br/>--}}
{{--            <div class="place-order-ticket">--}}
{{--                <img src="{{asset('img/place-order-ticket.svg')}}" alt="" class="order-ticket-img">--}}
{{--                {{$concert->dk_id}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-2 col-lg-3 col-md-3 col-sm-3 mt-5">--}}
{{--            <div class="date-order-ticket">--}}
{{--                <img src="{{asset('img/calendar-order-ticket.svg')}}" alt="" class="order-ticket-img">--}}
{{--                {!! \App\Helper\Helper::getLocalizedConcertTime($concert->date) !!}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-2 col-lg-2 mt-4 book">--}}
{{--            {{ __('home.reservation_is_active') }}--}}
{{--            <div id="time" class="book-time">15:00</div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    </div>--}}
{{--</header>--}}
<header class="order-ticket-header">
            <div class="order-ticket-header-block logo mt-3">
                <a href="{{url('/')}}">
                    <img src="{{asset('img/order-ticket-header.svg')}}" alt="" class="order-ticket-header-logo">
                </a>
            </div>
            <div class="order-ticket-header-block name mt-4">
                <b>{{$concert->name}}</b>
                <br/>
                <div class="place-order-ticket mt-4">
                    <img src="{{asset('img/place-order-ticket.svg')}}" alt="" class="order-ticket-img">
                    {{$concert->dk_id}}
                </div>
            </div>
            <div class="order-ticket-header-block data mt-5">
                <div class="date-order-ticket">
                    <img src="{{asset('img/calendar-order-ticket.svg')}}" alt="" class="order-ticket-img">
                    {!! \App\Helper\Helper::getLocalizedConcertTime($concert->date) !!}
                </div>
            </div>
            <div class="order-ticket-header-block book mt-4">
                {{ __('home.reservation_is_active') }}
                <div id="time" class="book-time">15:00</div>
            </div>
</header>