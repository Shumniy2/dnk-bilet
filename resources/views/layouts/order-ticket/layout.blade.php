<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.order-ticket.head')
    @include('layouts.menu-mobile')
    @yield('styles')

</head>
<body>
@include('layouts.order-ticket.header')
<div class="container-fluid dnk-main-order-ticket">
    <div class="row">
        <div class="col-lg-9 col-dnk-tablet">
            <div class="row dnk-main-scene-block">
                @yield('content')
            </div>
        </div>
        <div class="col-lg-3 dnk-block-cart">
            @include('layouts.order-ticket.cart')
        </div>
    </div>
</div>

@yield('scripts')
<script src="{{asset('js/input.js')}}" defer></script>
<script src="{{asset('js/menu-mobile.js')}}" defer></script>
@yield('mobile-footer')
@include('layouts.order-ticket.footer')
{{--@include('layouts.order-ticket.tablet-footer')--}}

<div class="scene-overlay" style="display: none;"></div>
<div class="ajax-loader ajax-scene text-center" style="display: none;">
    <img src="{{asset("img/loader.svg")}}" alt="loader">
</div>
</body>
</html>


