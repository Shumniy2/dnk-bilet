<footer class="order-ticket-footer">
        <div class="order-ticket-footer-block-desktop mt-4">
            <span>Все права защищены</span>
        </div>
         <div class="order-ticket-footer-block-desktop mt-4">
            <p><a href="tel:+ 38 (098)7509888">+38 097 48-900-79</a></p>
        </div>
         <div class="order-ticket-footer-block-tablet mt-4">
            <img src="{{asset('img/cart.png')}}" alt="">
        </div>
        <div class="order-ticket-footer-block-tablet mt-2">
                <span>Все права защищены</span>
                <p>+38 097 48-900-79</p>
        </div>
</footer>