@include('layouts.menu-mobile')
<header class="dnk-header">
    <div class="container">
        <div class="col-lg-12">
            <div class="row pt-2 pb-2 d-flex align-items-baseline">
                <div class="btn-dnk-toggler text-center">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
{{--                        <span class="navbar-toggler-icon"></span>--}}
                        <img class="dnk-img-toggler" src="{{asset("img/button.png")}}" alt="">
                    </button>
                </div>
                <div class="col-6 col-sm-4 col-md-4 col-lg-4">
                    <a href="{{url('/')}}"><img class="dnk-logo" src="{{asset('img/order-ticket-header.svg')}}" alt=""></a>
                    <img class="dnk-logo-small" src="{{asset('img/logo_small.png')}}" alt="">
                </div>
                <div class="col-2 col-sm-4 col-md-4 col-lg-4 offset-md-1 dnk-ajax-search">
                    <input type="text" id="ajax_search" class="ajax_search"  name="ajax_search">
                    <img class="dnk-img-search" src="{{asset('img/search.png')}}" alt="">
                    <button class="ajax_search_small">
                        <img class="dnk-img-search-small" src="{{asset('img/search_small.png')}}" alt="">
                    </button>
                </div>
                <div class="col-2 col-sm-4 col-md-3 col-lg-3 mt-sm-4">
                    <div class="row d-flex align-items-baseline">
                        <div class="dnk-localization-block col-sm-8 col-md-8 col-lg-9">
                            <ul class="dnk-localization">
                                <li class="dn-item {{\App\Helper\Helper::getActiveLocal('uk')}}">
                                    <a href="{{route('locale', ['locale' => 'uk'])}}">UA</a>
                                </li>
                                <li class="dn-item {{\App\Helper\Helper::getActiveLocal('ru')}}">
                                    <a href="{{route('locale', ['locale' => 'ru'])}}">RU</a>
                                </li>
                                <li class="dn-item {{\App\Helper\Helper::getActiveLocal('en')}}">
                                    <a href="{{route('locale', ['locale' => 'en'])}}">EN</a>
                                </li>
                            </ul>
                        </div>
                        <div class="dnk-cart-block col-sm-4 col-md-4 col-lg-3 mt-2 mt-md-1 text-center">
                            <img src="{{asset('img/cart.png')}}" alt="" class="dnk-img-cart">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    @include('layouts.navigation')
                </div>
            </div>
        </div>
    </div>
</header>
