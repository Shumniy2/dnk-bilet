@extends('layouts.voyager_admin.voyager_layout')

@section('page_title')
    <p>Платежи</p>
@endsection
@section('button_options')

@endsection

@section('page_subtitle')
    <span>Список платежей</span>
@endsection


@section('table')
    <table class="table table-bordered">
        <tr class="tr-1">
            <th>Транзации</th>
            <th>Дата</th>
            <th>Событие</th>
            <th>Статус</th>
            <th>Сумма</th>
            <th>Банк</th>
            <th>Карта</th>
            <th>Заказчик</th>
            <th>Телефон</th>
            <th>Эл.почта</th>
            <th>Доставка</th>
            <th>Заказ</th>
            <th>Управл.</th>
        </tr>
        <tr class="tr-1">
            <td><input type="text"></td>
            <td><input type="text"></td>
            <td><input type="text"></td>
            <td></td>
            <td><input type="text"></td>
            <td><input type="text"></td>
            <td></td>
            <td><input type="text"></td>
            <td><input type="text"></td>
            <td><input type="text"></td>
            <td></td>
            <td><input type="text"></td>
            <td></td>
        </tr>
    </table>
@endsection