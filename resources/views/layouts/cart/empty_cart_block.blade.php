<div id="empty-cart">
    <div class="empty_block_cart">
        <div class="row mt-5 ">
            <div class="col-lg-12 d-flex">
                <span class="mx-auto"> {{ __('home.empty_cart') }}</span>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-12 d-flex">
                <p class="mx-auto">
                    {{ __('home.select_ticket') }}
                </p>
            </div>
        </div>
    </div>
    <div class="row mt-5 text-center empty-cart">
        <div class="col-lg-12">
            <a href="#" class="btn link-dnk-ticket-id link-dnk-empty-cart disabled">
                <span class="ticket-text"> {{ __('home.checkout') }}</span>
            </a>
        </div>
    </div>
</div>
