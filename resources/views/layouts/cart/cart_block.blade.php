@if(!empty($place))
    <div class="block_cart">
        <div class="row mt-4">
            <div class="mt-2 concert_name">
{{--                {{ __('home.ticket_for') }} {{$place->concerts->name}}--}}
                {{ __('home.ticket_for') }} {{$place->concerts->name}}
            </div>
            @if(!isset($checkout))
                <div class="delete_ticket">
                    <button class="btn btn-cart" data-ticket="{{$place->id}}">
                        <img src="{{asset('img/delete.svg')}}" alt="">
                    </button>
                </div>
            @endif
        </div>
        <div class="row mt-2">
            <div class="row_order_ticket">
                {{ __('home.row') }}
            </div>
            <div class="row_2_order_ticket">
                {{$place->row}}
            </div>
        </div>
        <div class="row mt-2">
            <div class="row_order_ticket">
                {{ __('home.place') }}
            </div>
            <div class="row_2_order_ticket">
                {{$place->num_place}}
            </div>
        </div>
        <div class="row mt-2">
            <div class="row_order_ticket">
                {{ __('home.price') }}
            </div>
            <div class="row_2_order_ticket">
                {{$place->price }} грн
            </div>
        </div>
    </div>
@endif
