<nav class="navbar-dnk mt-lg-3 mb-2">
    <div class="navbar-collapse" id="navbarNav">
        <ul class="navbar-nav dnk-navbar">
            <li class="nav-item {{App\Helper\Helper::getActiveLink('index')}}">
                <a class="nav-link" href="{{url('/')}}">{{ __('home.general_text') }}</a>
            </li>
            <li class="nav-item {{App\Helper\Helper::getActiveLink('about')}}">
                <a class="nav-link" href="{{url('about')}}">{{ __('home.about_text') }}</a>
            </li>
            <li class="nav-item {{App\Helper\Helper::getActiveLink('contacts')}}">
                <a class="nav-link" href="{{url('contacts')}}">{{ __('home.contacts_text') }}</a>
            </li>
            <li class="nav-item {{App\Helper\Helper::getActiveLink('cashier')}}">
                <a class="nav-link" href="{{url('cashier')}}">{{ __('home.cashier_text') }}</a>
            </li>
            <li class="nav-item {{App\Helper\Helper::getActiveLink('ticket')}}">
                <a class="nav-link" href="{{route('tickets')}}">{{ __('home.ticket_text') }}</a>
            </li>
            <li class="nav-item-phone">
                <a class="nav-link nav-link-phone" href="tel:+ 38 (097) 48-900-79"> + 38 (097) 48-900-79</a>
            </li>
        </ul>
    </div>
</nav>

