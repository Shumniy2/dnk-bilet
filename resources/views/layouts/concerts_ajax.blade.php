@foreach($concerts as $concert)
    <div class="col-6 col-sm-4 col-lg-3">
        <div class="dnk-concert-block">
            <a href="{{url('concert/'.$concert->id)}}" class="concert_ajax_link">
            <div class="dnk-concert-overlay">
                <div class="prices">
                    <div class="before">{{ __('home.from') }} {{$concert->before}}</div>
                    <div class="after">{{ __('home.to') }} {{$concert->after}} {{ __('home.uah') }} </div>
                </div>
                <br/>
                <div class="time">
                    <svg width="14" height="14" viewBox="0 0 14 14" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M10.3335 1.99999H13.0002C13.177 1.99999 13.3465 2.07023 13.4716 2.19525C13.5966 2.32028 13.6668 2.48985 13.6668 2.66666V13.3333C13.6668 13.5101 13.5966 13.6797 13.4716 13.8047C13.3465 13.9298 13.177 14 13.0002 14H1.00016C0.823352 14 0.653783 13.9298 0.528758 13.8047C0.403734 13.6797 0.333496 13.5101 0.333496 13.3333V2.66666C0.333496 2.48985 0.403734 2.32028 0.528758 2.19525C0.653783 2.07023 0.823352 1.99999 1.00016 1.99999H3.66683V0.666656H5.00016V1.99999H9.00016V0.666656H10.3335V1.99999ZM12.3335 5.99999V3.33332H10.3335V4.66666H9.00016V3.33332H5.00016V4.66666H3.66683V3.33332H1.66683V5.99999H12.3335ZM12.3335 7.33332H1.66683V12.6667H12.3335V7.33332ZM3.00016 8.66666H6.3335V11.3333H3.00016V8.66666Z"
                              fill="white"/>
                    </svg>
                    <span>
                        {!! \App\Helper\Helper::getLocalizedConcertTime($concert->date) !!}
                    </span>
                </div>
                <div class="place">
                    <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M12.6665 14H3.33317C3.15636 14 2.98679 13.9298 2.86177 13.8047C2.73674 13.6797 2.6665 13.5101 2.6665 13.3333V7.33333H0.666504L7.55117 1.07466C7.67391 0.962983 7.83389 0.901093 7.99984 0.901093C8.16578 0.901093 8.32576 0.962983 8.4485 1.07466L15.3332 7.33333H13.3332V13.3333C13.3332 13.5101 13.2629 13.6797 13.1379 13.8047C13.0129 13.9298 12.8433 14 12.6665 14ZM3.99984 12.6667H11.9998V6.10466L7.99984 2.46866L3.99984 6.10466V12.6667ZM5.33317 10H10.6665V11.3333H5.33317V10Z"
                              fill="white"/>
                    </svg>
                    <span>
                        {{$concert->cultures->name}}
                    </span>
                </div>
            </div>
            </a>
            <div class="dnk-ticket">
                <a href="{{route('order-ticket', $concert->id)}}" class="link-dnk-ticket">
                    <span class="ticket-text">{{ __('home.order_ticket') }}</span>
                    <span class="ticket-text-small">Заказать</span>
                    <img class="dnk-img-ticket ml-3" src="{{asset("img/ticket.png")}}" alt="">
                </a>
            </div>
            <img class="dnk-img" src="{{asset("storage/imagesGoods/". $concert->image)}}"
                 alt="{{$concert->name}}">
            <h5>{{$concert->name}}</h5>
        </div>
    </div>
@endforeach