<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.head')
    @yield('styles')
</head>
    <body>
    @include('layouts.header')
        <div class="container dnk-main">
            <div class="row">
                <div class="col-md-12">
                    <div class="row concerts-block">
                        @yield('content')
                    </div>
                    <div class="ajax-loader text-center" style="display: none;">
                        <img src="{{asset("img/loader.gif")}}" alt="loader">
                    </div>
                </div>
            </div>
        </div>
        @yield('scripts')
        @include('layouts.footer')
    </body>
</html>


