@extends('layouts.voyager_admin.voyager_layout')


@section('page_title')
    <p>Пользователи</p>
@endsection

@section('button_options')
    <div class="btn-group">
        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">
            <img src="{{asset('img/admin-concerts.png')}}" alt="">
        </button>
        <div class="dropdown-menu">
            <div class="col-12">
                <img src="{{asset('img/add.svg')}}" alt="">
                <button type="button" class="btn" data-toggle="modal" data-target="#userModal">
                    Новый
                </button>
            </div>
        </div>
    </div>
@endsection


@section('page_subtitle')
    <span>Список пользователей</span>
@endsection

@section('table')
    <table class="table table-bordered" id="users">
        <tr class="tr-1">
            <th class="tr-1-th-1">Фамилия Имя</th>
            <th>Роль</th>
            <th>Группа</th>
            <th>Телефон</th>
            <th>Почта</th>
            <th width="200">Логин</th>
            <th><img src="{{asset('img/plane.svg')}}" alt=""></th>
            <th>Организатор</th>
            <th>Пароль</th>
            <th>Управление</th>
        </tr>
        <tr class="tr-1">
            <td><input type="text"></td>
            <td><input type="text"></td>
            <td><input type="text"></td>
            <td></td>
            <td></td>
            <td><input type="text"></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        @foreach ($users as $user)
            @include('layouts.users.user_row')
        @endforeach
    </table>
@endsection

@section('modal')
    <div class="modal fade" id="userModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content admin-user-modal-content">
                <div class="modal-header">
                    Пользователь
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="user_create_form">
                <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <select class="mdb-select md-form" id="role_id" name="role_id" required>
                                        <option value="" disabled selected>Роль</option>
                                        @foreach($roles as $role)
                                            <option value="{{$role->id}}">{{$role->role}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Имя</label>
                                        <input type="text" name="name"
                                               class="form-control fw-input-admin qa-phone-field"
                                               required></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Email</label>
                                        <input type="email" name="email"
                                               class="form-control fw-input-admin qa-phone-field"
                                               required></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Пароль</label>
                                        <input type="password" name="password"
                                               class="form-control fw-input-admin qa-phone-field"
                                               required></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Подтверждение пароля</label>
                                        <input type="password" name="confirm_password"
                                               class="form-control fw-input-admin qa-phone-field"
                                               required></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <select class="mdb-select md-form" id="group_id" name="group_id" required>
                                        <option value="" disabled selected>Группа</option>
                                        @foreach($groups as $group)
                                        <option value="{{$group->id}}">{{$group->name}}</option>
                                   @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <select class="mdb-select md-form" id="organizator_id" name="organizator_id" required>
                                        <option value="" disabled selected>Организатор</option>
                                        @foreach($organizators as $organizator)
                                        <option value="{{$organizator->id}}">{{$organizator->name}}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Телефон</label>
                                        <input type="text" name="phone"
                                               class="form-control fw-input-admin qa-phone-field"
                                               required></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Логин</label>
                                        <input type="text" name="login"
                                               class="form-control fw-input-admin qa-phone-field"
                                               required></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary admin_cancel_concert" data-dismiss="modal"><img
                                src="{{asset('img/delete.svg')}}" alt="">Отмена
                    </button>
                    <button type="submit" class="btn admin_create_concert"><img
                                src="{{asset('img/admin_create_concert.svg')}}" alt="">Сохранить
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="userEditModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content admin-user-modal-content">
                <div class="modal-header">
                    Пользователь
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="user_edit_form">
                    <div class="modal-body">
                        <div class="row">
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error fw-focused">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Фамилия Имя</label>
                                        <input type="text" name="name" id="name"
                                               class="form-control fw-input-admin qa-phone-field city-ru-edit"
                                               required value=""></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <select class="mdb-select md-form" id="role_id" name="role_id" required>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->role}}</option>
                                    @endforeach
                                </select>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <select class="mdb-select md-form" id="group_id" name="group_id" required>
                                    @foreach($groups as $group)
                                        <option value="{{$group->id}}">{{$group->name}}</option>
                                    @endforeach
                                </select>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error fw-focused">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Телефон</label>
                                        <input type="text" name="phone" id="phone"
                                               class="form-control fw-input-admin qa-phone-field city-ua-edit"
                                               required value=""></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error fw-focused">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Email</label>
                                        <input type="text" name="email" id="email"
                                               class="form-control fw-input-admin qa-phone-field city-ua-edit"
                                               required value=""></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error fw-focused">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Логин</label>
                                        <input type="text" name="login" id="login"
                                               class="form-control fw-input-admin qa-phone-field city-ua-edit"
                                               required value=""></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <select class="mdb-select md-form" id="organizator_id" name="organizator_id" required>
                                    @foreach($organizators as $organizator)
                                        <option value="{{$organizator->id}}">{{$organizator->name}}</option>
                                    @endforeach
                                </select>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <input type="hidden" name="id" id="id">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary admin_cancel_concert" data-dismiss="modal"><img
                                    src="{{asset('img/delete.svg')}}" alt="">Отмена
                        </button>
                        <button type="submit" class="btn admin_create_concert" id="user_edit"><img
                                    src="{{asset('img/admin_create_concert.svg')}}" alt="">Сохранить
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script  type="module" src="{{asset('js/admin/users/users.js')}}" defer></script>
@endsection