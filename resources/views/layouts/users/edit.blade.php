@extends('layouts.voyager_admin.voyager_layout')

@section('content')
    <div class="card-body">
        <form method="POST" action="{{ route('users_update',$users->id) }}">
            @csrf
            @method('PUT')
            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Роль:') }}</label>

                <div class="col-md-6">
                    <select name="role_id">
                        @foreach($roles as $role)
                            <option value="{{$role->id}}">{{$role->role}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="group_id" class="col-md-4 col-form-label text-md-right">{{ __('Группа:') }}</label>

                <div class="col-md-6">
                    <select name="group_id">
                        @foreach($groups as $group)
                        <option value="{{$group->id}}">{{$group->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="organizator_id" class="col-md-4 col-form-label text-md-right">{{ __('Организатор:') }}</label>

                <div class="col-md-6">
                    <select name="organizator_id">
                        @foreach($organizators as $organizator)
                            <option value="{{$organizator->id}}">{{$organizator->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Телефон:') }}</label>

                <div class="col-md-6">
                    <input type="tel" name="phone" class="form-control" required value="{{$users->phone}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="login" class="col-md-4 col-form-label text-md-right">{{ __('Логин:') }}</label>

                <div class="col-md-6">
                    <input type="text" name="login" class="form-control" required value="{{$users->login}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Имя') }}</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$users->name}}" required autocomplete="name" autofocus>

                    @error('name')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Изменить') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection