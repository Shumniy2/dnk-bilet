@if(!empty($user))
    <tr class="table-row" data-city="{{$user->id}}">
        <td class="tr-1-th-1" data-td="name" data-value="{{ $user->name }}">{{ $user->name }}</td>
        <td data-td="role_id" data-value="{{ $user->roles->role }}">{{ $user->roles->role  }}</td>
        <td data-td="group_id" data-value="{{ $user->groups->name }}">{{ $user->groups->name }}</td>
        <td data-td="phone" data-value="{{ $user->phone }}">{{ $user->phone }}</td>
        <td data-td="email" data-value="{{ $user->email }}">{{ $user->email }}</td>
        <td data-td="login" data-value="{{ $user->login }}">{{ $user->login }}</td>
        <td>-</td>
        <td data-td="organizator_id" data-value="{{ $user->organizers->name }}">{{ $user->organizers->name }}</td>
        <td>@if(!empty($user->password))
                <img src="{{asset('img/verify_mark.svg')}}" alt="">
            @else
                no
            @endif</td>
        <td class="admin_actions">
            <div class="dropdown">
                <button class="btn btn-default" id="dropdownMenu" type="button"
                        data-toggle="dropdown" aria-expanded="false">
                    <b>Управление</b> <img src="{{asset('img/arrow-up.svg')}}" alt=""> <span
                            class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                    <li role="presentation"><img src="{{asset('img/edit.svg')}}" alt="">
                        <button class="btn edit-user" data-toggle="modal" data-target="#userEditModal" data-id="{{$user->id}}">
                            Редактировать
                        </button>
                    </li>
                    <form action="{{ route('users_destroy',$user->id) }}" method="POST">
                        @csrf
                        <li role="presentation"><img src="{{asset('img/delete.svg')}}" alt="">
                            <button type="submit" class="btn">
                                Удалить
                            </button>
                        </li>
                    </form>
                </ul>
            </div>
        </td>
    </tr>
@endif