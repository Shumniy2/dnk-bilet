@extends('layouts.checkout.layout')
@section('content')
    @if(!empty($status))
        <div class="text-center">
            <h1>Ошибка платежа</h1>
            <h3>Код ошибки: {{$status['err_code']}}</h3>
            <h3>Причина ошибки: {{$status['err_decription']}}</h3>
        </div>
    @endif
@endsection

