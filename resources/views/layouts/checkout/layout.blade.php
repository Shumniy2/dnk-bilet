<!doctype html>
<html lang="en">
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/main.css') }}" rel="stylesheet">
<body class="checkout-block">
@include('layouts.checkout.header')
<div class="container-fluid d-flex flex-column justify-content-center">
{{--    <div class="row justify-content-center bg-errors">--}}
    <div class="row justify-content-center">
        <div class="col-12">
            @yield('content')
        </div>
    </div>
</div>
@include('layouts.checkout.footer')
</body>
</html>