@extends('layouts.checkout.layout')
@section('content')
<div class="row col-12 ticket_after_pay_block_1">
<div class="col-3 offset-1 mt-5">
    <img src="{{asset('img/test.png')}}" alt="">
    <img src="{{asset('img/after_payment_code_ticket.png')}}" alt="">
</div>
    <div class="col-8 mt-5">
        <p>Rock Symphony</p>
        <span>Дніпропетровський академічний театр опери та балету</span>
        <span>Дніпро, пр. Д. Яворницького 72-А</span>
        <p>Понеділок, 9 червня 2020</p>
        <span>Початок о 20:00</span>
        <div class="row ticket_after_pay_info_place col-9">
            <div class="col-4 mt-2">
            <span>Сектор</span>
            </div>
            <div class="col-5">
            <p>Партер</p>
            </div>
        </div>
        <div class="row ticket_after_pay_info_place col-9">
            <div class="col-4 mt-2">
            <span>Ряд</span>
            </div>
            <div class="col-5">
            <p>17</p>
            </div>
        </div>
        <div class="row ticket_after_pay_info_place col-9">
            <div class="col-4 mt-2">
                <span>Місце</span>
            </div>
            <div class="col-5">
                <p>21</p>
            </div>
        </div>
        <div class="row ticket_after_pay_info_place col-9">
            <div class="col-4 mt-2">
                <span>Ціна</span>
            </div>
            <div class="col-5">
                <p>1000 грн</p>
            </div>
        </div>
        <div class="row ticket_after_pay_info_place col-9">
            <div class="col-4 mt-2">
                <span>Покупець</span>
            </div>
            <div class="col-5">
                <p>Іванов Іван Іванович</p>
            </div>
        </div>
        <div class="row ticket_after_pay_info_place col-9">
            <div class="col-4 mt-2">
                <span>Заказ №</span>
            </div>
            <div class="col-5">
                <p>1798053604</p>
            </div>
        </div>
    </div>
</div>
<div class="ticket_after_pay_line_1"></div>
<div class="row col-12 ticket_after_pay_block_2 mb-5">
    <div class="col-5 offset-1 mt-5">
        <span>
            КУПУЮЧИ ТА ОТРИМУЮЧИ ЦЕЙ КВІТОК НА ЗАХІД, ВИ АВТОМАТИЧНО ПОГОДЖУЄТЕСЬ ВИКОНУВАТИ ВСІ ПРАВИЛА ТА ІНСТРУКЦІЇ, ВСТАНОВЛЕНІ В МІСЦІ ПРОВЕДЕННЯ ЗАХОДУ:
1.	КУПЛЕНИЙ КВИТОК ПОВЕРНЕННЮ ТА ОБМІНУ НЕ ПІДЛЯГАЄ.
2.	ПРИ ВТРАТІ ЧИ СУТТЄВОМУ ПОШКОДЖЕННІ КВИТКА ВЛАСНИК КВИТКА НА ЗАХІД НЕ ДОПУСКАЄТЬСЯ, ВАРТІСТЬ КВИТКА НЕ ВІДШКОДОВУЄТЬСЯ.
3.	ВІДШКОДУВАННЯ ОРГАНІЗАТОРОМ ЗАХОДУ ВАРТОСТІ КВИТКА МОЖЛИВЕ ТІЛЬКИ ЗА УМОВИ СКАСУВАННЯ ЗАХОДУ. ЗАТРИМКА ПОЧАТКУ ЗАХОДУ АБО ПЕРЕНЕСЕННЯ ДАТИ ПРОВЕДЕННЯ ЗАХОДУ НЕ Є ПІДСТАВОЮ ДЛЯ ПОВЕРНЕННЯ КВИТКА.
4.	ВЛАСНИК КВИТКА МАЄ НАЛЕЖНИМ ЧИНОМ ЗБЕРІГАТИ КВИТОК ДО ЗАКІНЧЕННЯ ЗАХОДУ, ЗІПСУВАННЯ КВИТКА МОЖЕ УСКЛАДНИТИ ПРОХІД НА ЗАХІД. У ВИПАДКУ, ЯКЩО ВЛАСНИК КВИТКА САМОВІЛЬНО ЗАЛИШИВ ЗАХІД, КВИТОК СТАЄ НЕ ДІЙСНИМ І НЕ ПІДЛЯГАЄ ПОВЕРНЕННЮ.
5.	АДМІНІСТРАЦИЯ НЕ НЕСЕ ВІДПОВІДАЛЬНОСТІ ЗА ПІДРОБЛЕНІ КВИТКИ, ПРЕТЕНЗІЇ ЩОДО ПІДРОБЛЕННИХ КВИТКІВ НЕ РОЗГЛЯДАЮТЬСЯ.
6.	ВІДВІДУВАННЯ ЗАХОДУ ОСОБАМИ В СТАНІ АЛКОГОЛЬНОГО АБО НАРКОТИЧНОГО СП’ЯНІННЯ СУВОРО ЗАБОРОНЕНО.
7.	ВІДВІДУВАЧІ, ЯКІ ПОРУШУЮТЬ ВСТАНОВЛЕНИЙ ПОРЯДОК І НЕ ВИКОНУЮТЬ ВИМОГИ ОФІЦІЙНИХ ОСІБ, БУДУТЬ ЗМУШЕНІ ПОКИНУТИ ЗАХІД БЕЗ ВІДШКОДУВАННЯ ВАРТОСТІ КВИТКА.
        </span>
    </div>
    <div class="col-1">

    </div>
    <div class="col-4 mt-5">
        <div class="row">
<p>КАТЕГОРИЧНО ЗАБРОНЕНО ПРИНОСИТИ НА ЗАХІД:</p>
        </div>
        <div class="row mt-4">
        <img src="{{asset('img/prohibited.png')}}" alt="">
        </div>
        <div class="row mt-5">
            <span>УСІ ВИЩЕЗАЗАНАЧЕНІ РЕЧІ БУДУТЬ ВИЛУЧАТИСЯ СЛУЖБОЮ БЕЗПЕКИ, АДМІНІСТРАЦІЯ ЗАЛИШАЄ ЗА СОБОЮ ПРАВО ПРОВОДИТИ ОСОБИСТИЙ ОГЛЯД ВІДВІДУВАЧІВ З МЕТОЮ ГАРАНТУВАННЯ БЕЗПЕКИ ЗАХОДУ.</span>
        </div>
    </div>
</div>
<div class="ticket_after_pay_line_2"></div>
<div class="row col-11 ticket_after_pay_block_3 offset-1 mt-5 mb-5">
    <div class="row col-12">
<p>Увага!</p>
    </div>
    <div class="row">
    <ul>
        <li>Вхід на захід за квитком можливий лише один раз і тільки одній особі.</li>
        <li>Не купуйте Електронний квиток з рук.</li>
        <li>Не допускайте копіювання Елетронного квитка.</li>
        <li>Відвідати захід за е-тікетом зможе лише одна особа. Допускаючи копіювання, Ви ризикуєте не потрапити на захід.</li>
        <li>При відвідуванні заходу організатор може попросити надати паспорт або інший документ, що засвідчує особу.</li>
    </ul>
    </div>
    <div class="row col-12">
        <p>За збереження даних, що містяться на роздрукованому квитку, сервіс DNK-bilet.com відповідальності не несе.</p>
    </div>
</div>
@endsection