@if(!empty($city))
    <tr class="table-row" data-city="{{$city->id}}">
        <td data-td="id" data-value="{{ $city->id }}" class="city-id-table">{{ $city->id }}</td>
        <td data-td="city_ru" data-value="{{ $city->city_ru }}">{{ $city->city_ru }}</td>
        <td data-td="city_uk" data-value="{{ $city->city_uk }}">{{ $city->city_uk }}</td>
        <td data-td="city_en" data-value="{{ $city->city_en }}">{{ $city->city_en }}</td>
        <td class="admin_actions">
            <div class="dropdown">
                <button class="btn btn-default" id="dropdownMenu" type="button" data-toggle="dropdown"
                        aria-expanded="false">
                    <b>Управление</b> <img src="{{asset('img/arrow-up.svg')}}" alt=""> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                    <li role="presentation"><img src="{{asset('img/edit.svg')}}" alt="">
                        <button class="btn edit-city" data-toggle="modal" data-target="#cityEditModal" data-id="{{$city->id}}">
                            Редактировать
                        </button>
                    </li>
                    <form action="{{ route('city_destroy',$city->id) }}" method="POST">
                        @csrf
                        <li role="presentation"><img src="{{asset('img/delete.svg')}}" alt="">
                            <button type="submit" class="btn">
                                Удалить
                            </button>
                        </li>
                    </form>
                </ul>
            </div>
        </td>
    </tr>
@endif