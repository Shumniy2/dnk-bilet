@extends('layouts.voyager_admin.voyager_layout')


@section('page_title')
    <p>Города</p>
@endsection

@section('button_options')
    <div class="btn-group">
        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">
            <img src="{{asset('img/admin-concerts.png')}}" alt="">
        </button>
        <div class="dropdown-menu">
            <div class="col-12">
                <img src="{{asset('img/add.svg')}}" alt="">
                <button type="button" class="btn" data-toggle="modal" data-target="#concertModal">Новый</button>
            </div>
            <div class="col-12">
                <img src="{{asset('img/statistics.svg')}}" alt="">
                <button type="button" class="btn">
                    Отчет
                </button>
            </div>
            <div class="col-12">
                <img src="{{asset('img/import.svg')}}" alt="">
                <button type="button" class="btn">
                    Импорт
                </button>
            </div>
            <div class="col-12">
                <img src="{{asset('img/sale.svg')}}" alt="">
                <button type="button" class="btn">
                    Продажи
                </button>
            </div>
        </div>
    </div>
@endsection


@section('page_subtitle')
    <span>Список городов</span>
@endsection

@section('table')
    <table class="table table-bordered" id="cities">
        <tr class="tr-1">
            <th class="tr-1-th-1">#</th>
            <th>Город(ru)</th>
            <th>Город(ua)</th>
            <th>Город(en)</th>
            <th width="280px">Действие</th>
        </tr>
        @foreach ($cities as $city)
            @include('cities.city_row')
        @endforeach
    </table>
@endsection

@section('modal')
    <div class="modal fade" id="concertModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content admin-user-modal-content">
                <div class="modal-header">
                    Город
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="city_create_form">
                    <div class="modal-body">
                        <div class="row">
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Город(ru)</label>
                                        <input type="text" name="city_ru"
                                               class="form-control fw-input-admin qa-phone-field"
                                               required></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Город(ua)</label>
                                        <input type="text" name="city_uk"
                                               class="form-control fw-input-admin qa-phone-field"
                                               required></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Город(en)</label>
                                        <input type="text" name="city_en"
                                               class="form-control fw-input-admin qa-phone-field"
                                               required></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary admin_cancel_concert" data-dismiss="modal"><img
                                    src="{{asset('img/delete.svg')}}" alt="">Отмена
                        </button>
                        <button type="submit" class="btn admin_create_concert"><img
                                    src="{{asset('img/admin_create_concert.svg')}}" alt="">Сохранить
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="cityEditModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content admin-user-modal-content">
                <div class="modal-header">
                    Город
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="city_edit_form">
                    <div class="modal-body">
                        <div class="row">
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error fw-focused">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Город(ru)</label>
                                        <input type="text" name="city_ru" id="city_ru"
                                               class="form-control fw-input-admin qa-phone-field city-ru-edit"
                                               required value=""></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error fw-focused">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Город(ua)</label>
                                        <input type="text" name="city_uk" id="city_uk"
                                               class="form-control fw-input-admin qa-phone-field city-ua-edit"
                                               required value=""></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error fw-focused">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Город(en)</label>
                                        <input type="text" name="city_en" id="city_en"
                                               class="form-control fw-input-admin qa-phone-field city-en-edit"
                                               required value=""></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <input type="hidden" name="id" id="id">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary admin_cancel_concert" data-dismiss="modal"><img
                                    src="{{asset('img/delete.svg')}}" alt="">Отмена
                        </button>
                        <button type="submit" class="btn admin_create_concert" id="city_edit"><img
                                    src="{{asset('img/admin_create_concert.svg')}}" alt="">Сохранить
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
{{--    <script src="{{asset('js/admin/voyager.js')}}" defer></script>--}}
<script type="module" src="{{asset('js/admin/cities/cities.js')}}" defer></script>
@endsection