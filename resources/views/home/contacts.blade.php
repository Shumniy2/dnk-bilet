@extends('layouts.layout')
@section('styles')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
          integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
@endsection
@section('content')
    <div class="container contacts-page">
        <div class="row">
            <p>{{ __('home.contacts_text') }}</p>
        </div>
        <div class="row contacts">
            <div class="contact mt-5">
                <img src="{{asset('img/phone.svg')}}" alt=""><span><a class="contacts_tel" href="tel:+ 38 (097) 48-900-79">+38 (097) 48-900-79</a></span>
            </div>
            <div class="contact">
            <img src="{{asset('img/mail.svg')}}" alt=""><span>info@dnk-bilet.com</span>
            </div>
            <div class="contact">
            <img src="{{asset('img/geolocation.svg')}}" alt=""><span>{{ __('home.city_text') }}</span>
            </div>
        </div>
        <div class="row mt-5">
            <p>{{ __('home.subscribe') }}</p>
        </div>
        <div class="row contacts">
            <div class="contact mt-5">
                <a href="http://facebook/dnk_bilet">
                <img src="{{asset('img/facebook-2.svg')}}" alt=""><span>http://facebook/dnk_bilet</span>
                </a>
            </div>
            <div class="contact">
                <a href="http://instagram/dnk_bilet">
                <img src="{{asset('img/instagram-2.svg')}}" alt=""><span>http://instagram/dnk_bilet</span>
                </a>
            </div>
            <div class="contact">
                <a href="http://youtube/dnk_bilet">
                <img src="{{asset('img/youtube-2.svg')}}" alt=""><span>http://youtube/dnk_bilet</span>
                </a>
            </div>
        </div>
    </div>
@endsection