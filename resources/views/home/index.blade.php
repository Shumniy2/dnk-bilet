@extends('layouts.layout')

@section('content')
    @if(isset($concerts))
        @include("layouts.concerts_ajax")
    @endif
@endsection

@section('scripts')
    <script src="{{ asset('js/main.js') }}" defer></script>
    <script src="{{ asset('js/menu-mobile.js') }}" defer></script>
@endsection
