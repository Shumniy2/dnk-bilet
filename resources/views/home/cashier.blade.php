@extends('layouts.layout')

@section('styles')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
          integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
@endsection
@section('content')

    <div class="container" id="cashier">
        <div class="row mb-5">
            <div class="col-lg-12">
                <span class="text-cashier">{{ __('home.cashier_text') }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="map" style="width: 100%; height: 409px;"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 p-0">
                <div class="accordion" id="accordionExample">
                    @foreach($cities as $city)
                        <div class="card mt-4">
                            <div class="card-header" id="heading_{{$city->id}}">
                                <h2 class="mb-4">
                                    <button class="btn btn-cashier collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapse_{{$city->id}}" aria-expanded="true"
                                            aria-controls="collapse_{{$city->id}}">
                                        {{App\Helper\Helper::getCityLocalization($city)}}
                                    </button>
                                </h2>
                            </div>

                            <div id="collapse_{{$city->id}}" class="collapse"
                                 aria-labelledby="heading_{{$city->id}}"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    @foreach($city->addresses as $address)
                                        <div class="row p-1">
                                            <div class="col-12 pb-4 address">
                                                <span class="btn-link-address" data-target-marker="">{{App\Helper\Helper::getAddressLocalization($address)}}</span>
                                            </div>
                                            <div class="col-12 address_text">
                                                {!! App\Helper\Helper::getAddressTextLocalization($address)!!}
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="{{asset('js/mapbox-gl.js')}}"></script>
<script src="{{asset('js/mapbox.js')}}" defer></script>
<script src="{{asset('js/menu-mobile.js')}}" defer></script>
@endsection
