@extends('layouts.layout')
@section('styles')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
          integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
@endsection
@section('content')
    <div class="container about-page ml-5">
        <div class="row about">
            <span>{{ __('home.about_text') }}</span>
        </div>
        <div class="row mt-5">
            <span>{{ __('home.about_agency') }}</span>
        </div>
        <div class="row ml-5 mt-5 mb-5">
                <div class="col-4 mr-4 more-than-1000">
                    <img src="{{asset('img/ticket.svg')}}" alt="">
                    <p>{{ __('home.more_than_1000') }}</p>
                    <span>{{ __('home.more_than_1000_details') }}</span>
                </div>
                <div class="col-4 mr-4 more-than-700">
                    <img src="{{asset('img/corona.svg')}}" alt="">
                    <p>{{ __('home.more_than_700') }}</p>
                    <span>{{ __('home.more_than_700_details') }}</span>
                </div>
                <div class="col-3 year-1996">
                    <img src="{{asset('img/house.svg')}}" alt="">
                    <p class="year">1996</p>
                    <span>{{ __('home.we_work_for_you') }}</span>
                </div>
        </div>
        <div class="row about-page-text">
            <span>{{ __('home.more_agency_details') }}</span>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('js/menu-mobile.js')}}" defer></script>
@endsection
