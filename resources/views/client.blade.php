@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <?php
                $concerts = DB::table('concerts')->get('image');
                ?>
                здесь будут премьеры
            </div>
        </div>
    </div>
    <!-- Place somewhere in the <body> of your page -->
    <div class="posts" style="display: flex;border: 1px solid black;">
    </div>









    <!--Carousel Wrapper-->
    <div id="carousel-with-lb" class=" carousel slide carousel-multi-item" data-ride="carousel">

        <!--Controls-->
        <div class="controls-top">
            <a class="btn-floating btn-secondary" href="#carousel-with-lb" data-slide="prev"><i
                        class="fas fa-chevron-left"></i></a>
            <a class="btn-floating btn-secondary" href="#carousel-with-lb" data-slide="next"><i
                        class="fas fa-chevron-right"></i></a>
        </div>
        <!--/.Controls-->

        <!--Indicators-->
        <ol class="carousel-indicators">
            <li data-target="#carousel-with-lb" data-slide-to="0" class="active secondary-color"></li>
            <li data-target="#carousel-with-lb" data-slide-to="1" class="secondary-color"></li>
            <li data-target="#carousel-with-lb" data-slide-to="2" class="secondary-color"></li>
        </ol>
        <!--/.Indicators-->

        <!--Slides and lightbox-->

        <div class="carousel-inner mdb-lightbox" role="listbox">
            <div id="mdb-lightbox-ui"></div>
            <!--First slide-->
            @php $count = 0;@endphp
            @foreach($concerts as $concert)
                @if($count % 6 == 0)
                    <div class=" carousel-item  {{$count == 0 ? 'active' : ' '}}  text-center">

                        <figure class="col-md-4 d-md-inline-block">
                            @endif
                            <img src="{{ asset('storage/imagesGoods/'.$concert->image) }}" width="200">
                            @php $count++;@endphp
                            @if($count % 6 == 0)
                        </figure>
                    </div>
                @endif
            @endforeach
            {{--        <div class=" carousel-item active text-center">--}}

            {{--            <figure class="col-md-4 d-md-inline-block">--}}
            {{--                @foreach($concerts as $concert)--}}

            {{--                            <img src="{{ asset('storage/imagesGoods/'.$concert->image) }}" width="200">--}}

            {{--                @endforeach--}}

            {{--            </figure>--}}


            {{--        </div>--}}
        <!--/.First slide-->
            {{--        <div class=" carousel-item text-center">--}}

            {{--            <figure class="col-md-4 d-md-inline-block">--}}
            {{--                @foreach($concerts as $concert)--}}

            {{--                    <img src="{{ asset('storage/imagesGoods/'.$concert->image) }}" width="200">--}}

            {{--                @endforeach--}}

            {{--            </figure>--}}


            {{--        </div>--}}

        </div>
        <!--/.Slides-->

    </div>
    <!--/.Carousel Wrapper-->
@endsection
