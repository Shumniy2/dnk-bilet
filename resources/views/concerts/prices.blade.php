@include('layouts.voyager_admin.voyager_header')
    <link rel="stylesheet" href="{{asset("css/app.css")}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
      integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{asset('css/circle-styles.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/admin/admin.css')}}">
    <link rel="stylesheet" href="{{asset("css/scene.css")}}">

    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content" id="modalConcert">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Новая цена</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-save-body">
                    <label for="ticket_price">Цена: </label>
                    <input type="number" id="ticket_price" name="ticket_price" min="1" autofocus required>
                    <span class="msg text-danger"></span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="save_price">Сохранить</button>
                    <button type="button" class="btn btn-danger" id="cancel_price">Отмена</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="row block-scene-admin">
            <div class="col-lg-3 mt-5">
                <div class="row">
                    <div class="col-lg-12">
                        @include('layouts.scene_grids.color_table')
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary" id="add_color">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-primary">
                            <i class="fa fa-pen" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-primary" id="remove_color">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-primary" id="update_colors">
                            <i class="fa fa-retweet" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
                <div class="row apply_prices_block">
                    <div class="col-lg-12">
                        <div class="modal-content modal-apply-prices" id="modal_place">
                            <div class="modal-header">
                                <h5 class="modal-title">
                                    Применение
                                    <span id="apply_price" data-apply-price></span>
                                </h5>
                            </div>
                            <div class="modal-body modal-apply-body">
                                <div>Выбрано</div>
                                <div class="apply_count" id="apply_count" data-apply-count></div>
                                <div>место</div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success" id="save_place">Сохранить</button>
                                <button type="button" class="btn btn-danger" id="cancel_place">
                                    Отмена
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 admin-scene">
                <div class="row">
                    <div class="col-lg-12 mx-auto mt-2 mb-5">
                        <div class="row">
                            <div class="col-lg-6 offset-2">
                                <div class="dnk-scene-text">
                                    <h1>Сцена</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="scene-block">
                            @include('layouts.scene_grids.dk_machine')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="{{asset("js/app.js")}}" defer></script>
    <script src="{{asset("js/admin/admin.js")}}" defer></script>
@include('layouts.voyager_admin.voyager_footer')