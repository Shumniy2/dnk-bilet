@extends('layouts.voyager_admin.voyager_layout')

@section('content')
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">

    <!-- подключаем стили Summernote -->

    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.css" rel="stylesheet">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Редактировать концерт</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('concerts.index') }}"> Назад</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('concerts.update',$concert->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Наименование:</strong>
                    <input type="text" name="name" value="{{ $concert->name }}" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Площадка:</strong>
                    <input type="text" name="place" value="{{ $concert->place }}" class="form-control" placeholder="Место">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Организатор:</strong>
                    <input type="text" name="organizer" value="{{ $concert->organizer }}" class="form-control" placeholder="Организатор">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Город:</strong>
                    <input type="text" name="city" value="{{ $concert->city }}" class="form-control" placeholder="Город">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Дата время:</strong>
                    <input type="datetime-local" name="date" class="form-control" placeholder="Дата" required>
                </div>
            </div>
{{--            <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--                <div class="form-group">--}}
{{--                    <strong>Фото:</strong>--}}
{{--                    <input type="file" name="image" class="form-control" placeholder="Фото">--}}
{{--                    <img class="dnk-img" src="{{asset("storage/imagesGoods/". $concert->image)}}">--}}
{{--                </div>--}}
{{--            </div>--}}
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Описание:</strong>
                    <textarea type="text" name="description" class="form-control" placeholder="Описание" style="height: 200px;" id="text">{{ $concert->description }}</textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>От:</strong>
                    <input type="number" name="before" value="{{ $concert->before }}" class="form-control" placeholder="От">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>До:</strong>
                    <input type="number" name="after" value="{{ $concert->after }}" class="form-control" placeholder="До">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Часы:</strong>
                    <input type="number" name="hours" value="{{ $concert->hours }}" class="form-control" placeholder="Часы">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Минуты:</strong>
                    <input type="number" name="minutes" value="{{ $concert->minutes }}" class="form-control" placeholder="Минуты">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </div>

    </form>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>

    <!-- подключаем bootstrap.js -->

    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

    <!-- подключаем сам summernote -->

    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.js"></script>

    <script>

        $(document).ready(function() {

            $('#text').summernote();

        });

    </script>
@endsection