@extends('layouts.voyager_admin.voyager_layout')

@section('styles')
    <link href="{{ asset('js/datatimepicker/jquery.datetimepicker.min.css') }}" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.css" rel="stylesheet">
@endsection

@section('page_title')
    <p>Концерты</p>
@endsection

@section('button_options')
    <div class="btn-group">
        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">
            <img src="{{asset('img/admin-concerts.png')}}" alt="">
        </button>
        <div class="dropdown-menu">
            <div class="col-12">
                <img src="{{asset('img/add.svg')}}" alt="">
                {{--                            <a href="{{ route('concerts.create') }}"><span>Новый</span></a>--}}
                <button type="button" class="btn" data-toggle="modal" data-target="#concertModal">
                    Новый
                </button>
            </div>
            <div class="col-12">
                <img src="{{asset('img/statistics.svg')}}" alt="">
                <button type="button" class="btn">
                    Отчет
                </button>
            </div>
            <div class="col-12">
                <img src="{{asset('img/import.svg')}}" alt="">
                <button type="button" class="btn">
                    Импорт
                </button>
            </div>
            <div class="col-12">
                <img src="{{asset('img/sale.svg')}}" alt="">
                <button type="button" class="btn">
                    Продажи
                </button>
            </div>
        </div>
    </div>
@endsection

@section('page_subtitle')
    <span>Список концертов</span>
@endsection

@section('table')
    <table class="table table-bordered" id="concerts">
        <tr class="tr-1">
            <th width="50">Код</th>
            <th>Наименование</th>
            <th>Дата</th>
            <th>Организатор</th>
            <th width="50">Статус</th>
            <th>Город</th>
            <th>Площадка</th>
            <th>Бил-зак</th>
            <th>Управление</th>
        </tr>
        <tr class="tr-1">
            <td><input type="text"></td>
            <td><input type="text"></td>
            <td><input type="text"></td>
            <td><input type="text"></td>
            <td><input type="text"></td>
            <td><input type="text"></td>
            <td><input type="text"></td>
            <td></td>
            <td></td>
        </tr>
        @foreach ($concerts as $concert)
            @include('concerts.concert_row')
        @endforeach
    </table>
@endsection

@section('modal')
    <div class="modal fade" id="concertModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    Концерт
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="concert_create_form">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <select class="mdb-select md-form" id="select_culture" name="culture" required>
                                    <option value="" disabled selected>Площадка</option>
                                    @foreach($cultures as $culture)
                                        <option value="{{$culture->id}}">{{$culture->name}}</option>
                                        @endforeach
                                        </select>
                                </div>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Наименование</label>
                                        <input type="text" name="name"
                                               class="form-control fw-input-admin qa-phone-field"
                                               required></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <select class="mdb-select md-form" id="select_organizer" name="organizer" required>
                                        <option value="" disabled selected>Организатор</option>
                                        @foreach($organizers as $organizer)
                                            <option value="{{$organizer->id}}">{{$organizer->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                    <input id="date-create" name="date" class="data_concert" type="text" placeholder="Дата" required>

                                    <span class="fw-field__error qa-fw-field__error"></span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Параметры:</strong><br/>
                                    <div class="parametrs">
                                        <div class="parametrs_block">
                                            <input type="checkbox" name="canceled" value="1"><span>Отменен</span>
                                        </div>
                                        <div class="parametrs_block">
                                            <input type="checkbox" name="no_in_shop"
                                                   value="2"><span>Нет в продаже</span>
                                        </div>
                                        <div class="parametrs_block">
                                            <input type="checkbox" name="hide_concert"
                                                   value="3"><span>Скрыть концерт</span>
                                        </div>
                                        <div class="parametrs_block">
                                            <input type="checkbox" name="pay_for_organizator" value="4"><span>Оплата организатору</span>
                                        </div>
                                        <div class="parametrs_block">
                                            <input type="checkbox" name="places"
                                                   value="5"><span>Приставные места</span>
                                        </div>
                                        <div class="parametrs_block">
                                            <input type="checkbox" name="archive" value="6"><span>Архивный</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="media">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="fl_upld">
                                        <label><input id="fl_inp" type="file" name="image"><img src="{{asset('img/photo.svg')}}" alt=""><span>Загрузить фото</span></label>
{{--                                        <div id="fl_nm">Файл не выбран</div>--}}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="fl_upld">
                                        <label><input id="fl_inp" type="file" name="video"><img src="{{asset('img/photo.svg')}}" alt=""><span>Загрузить видео</span></label>
{{--                                        <div id="fl_nm">Файл не выбран</div>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <select class="mdb-select md-form" id="select_city" name="city" required>
                                        <option value="" disabled selected>Город</option>
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}">{{$city->city_ru}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content description_admin">
{{--                                        <label for="name">Описание</label>--}}
                                        <textarea type="text" name="description" id="description"
                                                  class="form-control fw-input-admin qa-phone-field"
                                                  required></textarea>
                                    </div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group ticket_order_admin">
                                    <strong>Бил-Зак:</strong>
                                    <input type="text" name="ticket_order" class="form-control"
                                           placeholder="Бил-Зак"
                                           readonly value="{{mt_rand()}}">
                                </div>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content">
                                        <label for="name">Часы</label>
                                        <input type="text" name="hours"
                                               class="form-control fw-input-admin qa-phone-field" required></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error">
                                    <div class="fw-field__content">
                                        <label for="name">Минуты</label>
                                        <input type="text" name="minutes"
                                               class="form-control fw-input-admin qa-phone-field"
                                               required></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary admin_cancel_concert" data-dismiss="modal"><img
                                        src="{{asset('img/delete.svg')}}" alt="">Отмена
                            </button>
                            <button type="submit" class="btn admin_create_concert"><img
                                        src="{{asset('img/admin_create_concert.svg')}}" alt="">Сохранить
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="concertEditModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content admin-user-modal-content">
                <div class="modal-header">
                    Концерт
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="concert_edit_form">
                    <div class="modal-body">
                        <div class="row">
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field fw-has-error qa-fw-has-error fw-focused">
                                    <div class="fw-field__content add_concert_admin">
                                        <label for="name">Наименование</label>
                                        <input type="text" name="name" id="name"
                                               class="form-control fw-input-admin qa-phone-field city-ru-edit"
                                               required value=""></div>
                                </div>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                    <input id="date" name="date" type="text" class="data_concert" placeholder="Дата" required value="">

                                    <span class="fw-field__error qa-fw-field__error"></span>
                                </div>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <select class="mdb-select md-form" id="organizer_id" name="organizer_id" required>
                                    @foreach($organizers as $organizer)
                                        <option value="{{$organizer->id}}">{{$organizer->name}}</option>
                                    @endforeach
                                </select>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <select class="mdb-select md-form" id="city_id" name="city_id" required>
                                    @foreach($cities as $city)
                                        <option value="{{$city->id}}">{{$city->city_ru}}</option>
                                    @endforeach
                                </select>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <div class="fw-field-container-admin qa-fw-field-container b-auth-last-form-field fw-field-container--has-span">
                                <select class="mdb-select md-form" id="dk_id" name="dk_id" required>
                                    @foreach($cultures as $culture)
                                        <option value="{{$culture->id}}">{{$culture->name}}</option>
                                    @endforeach
                                </select>
                                <span class="fw-field__error qa-fw-field__error"></span>
                            </div>
                            <input type="hidden" name="id" id="id">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary admin_cancel_concert" data-dismiss="modal"><img
                                    src="{{asset('img/delete.svg')}}" alt="">Отмена
                        </button>
                        <button type="submit" class="btn admin_create_concert" id="concert_edit"><img
                                    src="{{asset('img/admin_create_concert.svg')}}" alt="">Сохранить
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script  type="module" src="{{asset('js/admin/concerts/concerts.js')}}" defer></script>
    <script src="{{asset('js/datatimepicker/jquery.datetimepicker.full.min.js')}}" defer></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.js" defer></script>
@endsection