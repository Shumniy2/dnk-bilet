@extends('layouts.voyager_admin.voyager_layout')

@section('content')
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">

    <!-- подключаем стили Summernote -->

    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.css" rel="stylesheet">
    <link href="{{asset('css/circle-styles.css')}}" rel="stylesheet">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Создать новый концерт</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('concerts.index') }}"> Назад</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('concerts.store') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Площадка:</strong>
                    <select name="dk_id" id="dk_id">
                        @foreach($dk as $place)
                            <option value="{{$place->id}}">{{$place->name}}</option>
                            @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Наименование:</strong>
                    <input type="text" name="name" class="form-control" placeholder="Name" id="name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Организатор:</strong>
                    <input type="text" name="organizer" class="form-control" placeholder="Организатор" id="organizer">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Дата время:</strong>
                    <input type="datetime-local" name="date" class="form-control" placeholder="Дата" id="date">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Параметры:</strong><br/>
                    <input type="checkbox" name="canceled" value="1" id="canceled">Отменен<br/>
                    <input type="checkbox" name="codes_without_d" value="2" id="codes_without_d">Коды без «D»<br/>
                    <input type="checkbox" name="no_in_shop" value="3" id="no_in_shop">Нет в продаже<br/>
                    <input type="checkbox" name="hide_concert" value="4" id="hide_concert">Скрыть концерт<br/>
                    <input type="checkbox" name="another_codes" value="5" id="another_codes">«Чужие» коды<br/>
                    <input type="checkbox" name="pay_for_organizator" value="6" id="pay_for_organizator">Оплата организатору<br/>
                    <input type="checkbox" name="places" value="7" id="places">Приставные места<br/>
                    <input type="checkbox" name="archive" value="8" id="archive">Архивный<br/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Фото:</strong>
                    <input type="file" name="image" class="form-control" placeholder="Фото" id="image">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Город:</strong>
                    <input type="text" name="city" class="form-control" placeholder="Город" id="city">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Описание:</strong>
                    <textarea type="text" id="text" name="description" class="form-control" placeholder="Описание" style="height: 200px;"></textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Бил-Зак:</strong>
                    <input type="text" name="ticket_order" class="form-control" placeholder="Бил-Зак" readonly value="{{mt_rand()}}" id="ticket_order">
                </div>
            </div>
{{--            <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--                <div class="form-group">--}}
{{--                    <strong>От:</strong>--}}
{{--                    <input type="number" name="before" class="form-control" placeholder="От">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--                <div class="form-group">--}}
{{--                    <strong>До:</strong>--}}
{{--                    <input type="number" name="after" class="form-control" placeholder="После">--}}
{{--                </div>--}}
{{--            </div>--}}
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Часы:</strong>
                    <input type="number" name="hours" class="form-control" placeholder="Часы" id="hours">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Минуты:</strong>
                    <input type="number" name="minutes" class="form-control" placeholder="Минуты" id="minutes">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Создать</button>
            </div>
        </div>

    </form>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>

    <!-- подключаем bootstrap.js -->

    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

    <!-- подключаем сам summernote -->

    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.js"></script>

    <script>

        $(document).ready(function() {

            $('#text').summernote();

        });

    </script>
@endsection