@extends('layouts.voyager_admin.voyager_layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Концерт</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('concerts.index') }}"> Назад</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Код:</strong>
                {{ $concert->id }}
            </div>
            <div class="form-group">
                <strong>Название:</strong>
                {{ $concert->name }}
            </div>
            <div class="form-group">
                <strong>Дата:</strong>
                {{ $concert->date }}
            </div>
            <div class="form-group">
                <strong>Фото:</strong>
                <img src="{{ asset('storage/imagesGoods/'.$concert->image) }}" alt="">
            </div>
            <div class="form-group">
                <strong>Организатор:</strong>
                {{ $concert->organizer }}
            </div>
            <div class="form-group">
                <strong>Статус:</strong>
                {{ $concert->status }}
            </div>
            <div class="form-group">
                <strong>Город:</strong>
                {{ $concert->city }}
            </div>
            <div class="form-group">
                <strong>Место:</strong>
                {{ $concert->place }}
            </div>
            <div class="form-group">
                <strong>Бил-Зак:</strong>
                {{ $concert->ticket_order }}
            </div>
            <div class="form-group">
                <strong>Описание:</strong>
                {!! $concert->description !!}
            </div>
            <div class="form-group">
                <strong>От:</strong>
                {{ $concert->before }}
            </div>
            <div class="form-group">
                <strong>До:</strong>
                {{ $concert->after }}
            </div>
            <div class="form-group">
                <strong>Часы:</strong>
                {{ $concert->hours }}
            </div>
            <div class="form-group">
                <strong>Минуты:</strong>
                {{ $concert->minutes }}
            </div>
        </div>
    </div>
@endsection