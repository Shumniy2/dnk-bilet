@if(!empty($concert))
    <tr class="table-row" data-city="{{$concert->id}}">
        <td class="tr-1-th-1">{{ $concert->id }}</td>
        <td data-td="name" data-value="{{ $concert->name }}">{{ $concert->name }}</td>
        <td data-td="date" data-value="{{ $concert->date }}">{{ $concert->date }}</td>
        <td  data-td="organizer_id" data-value="{{ $concert->organizer_id }}">{{ $concert->organizers->name }}</td>
        <td>
            @if(isset($concert->canceled))
                <i class="fas fa-window-close"></i>
            @endif
            @if(isset($concert->no_in_shop))
                <i class="fas fa-store-alt"></i>
                @endif
            @if(isset($concert->hide_concert))
                <i class="fas fa-eye-slash"></i>
                @endif
            @if(isset($concert->pay_for_organizator))
                <i class="fas fa-user"></i>
                @endif
            @if(isset($concert->places))
                <i class="fas fa-chair"></i>
                @endif
            @if(isset($concert->archive))
                <i class="fas fa-archive"></i>
            @endif
        </td>
        <td data-td="city_id" data-value="{{ $concert->city_id }}">{{ $concert->cities->city_ru }}</td>
        <td data-td="dk_id" data-value="{{ $concert->dk_id }}">{{ $concert->cultures->name }}</td>
        <td>{{ $concert->ticket_order }}</td>
        <td class="admin_actions">
            <div class="dropdown">
                <button class="btn btn-default" id="dropdownMenu" type="button"
                        data-toggle="dropdown" aria-expanded="false">
                    <b>Управление</b> <img src="{{asset('img/arrow-up.svg')}}" alt=""> <span
                            class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                    <li role="presentation"><img src="{{asset('img/prices.svg')}}" alt=""><a
                                role="menuitem" href="{{route('prices', $concert->id)}}" class="btn">Цены
                            на концерт</a></li>
                    <li role="presentation"><img src="{{asset('img/calculation.svg')}}"
                                                 alt=""><a role="menuitem" href="#" class="btn">Расчет с
                            релизаторами</a></li>
                    <li role="presentation"><img src="{{asset('img/statistics.svg')}}"
                                                 alt=""><a role="menuitem" href="#" class="btn">Статистика
                            по билетам</a></li>
                    <li role="presentation"><img src="{{asset('img/sale.svg')}}" alt=""><a
                                role="menuitem" href="#" class="btn">Продажи</a></li>
                    <li role="presentation"><img src="{{asset('img/report.svg')}}" alt=""><a
                                role="menuitem" href="#" class="btn">Отчет</a></li>
                    <li role="presentation"><img src="{{asset('img/edit.svg')}}" alt="">
                        <button class="btn edit-concert" data-toggle="modal" data-target="#concertEditModal" data-id="{{$concert->id}}">
                            Редактировать
                        </button>
                    </li>
                    <form action="{{ route('concert_destroy',$concert->id) }}" method="POST">
                        @csrf
                        <li role="presentation"><img src="{{asset('img/delete.svg')}}" alt="">
                            <button type="submit" class="btn">
                                Удалить
                            </button>
                        </li>
                    </form>
                </ul>
            </div>
        </td>
    </tr>
@endif