<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'general_text' => 'General',
    'about_text' => 'About',
    'contacts_text' => 'Contacts',
    'cashier_text' => 'Cashiers',
    'ticket_text' => 'E-ticket',
    'sections_text' => 'Sections',
    'subscribe_text' => 'Subscribe',
    'logo_for_partners' => 'Logo for partners',
    'city_text' => 'c. Dnipro',
    'public_offer_agreement' => 'Public offer agreement',
    'all_rights_reserved' => 'All rights reserved.',
    'when_copying_material_link_to_the_site_is_required' => 'When copying material link to the site is required',
    'development_of_name_of_company' => 'Development of Name of company',
    'about_agency' => 'Concert agency "DNK" is one of the oldest in Ukraine.',
    'more_than_1000' => 'over 1000',
    'more_than_700' => 'more than 700',
    'we_work_for_you' => 'We have been working for you since 1996',
    'more_than_1000_details' => 'During our work, we have organized over 1000 various concerts and performances',
    'more_than_700_details' => 'We have  successfully organized more than 700 Ukraine, Russian and foreign popular artists and groups.',
    'more_agency_details' => 'We work with top stars, with leading production agencies. A concert or a performance from DNK is always top quality and prestige. Our guests feel at home. A separate segment of our activity is corporate events - city days, festivals, professional holidays, weddings, anniversaries, etc. We are always open to proposals for cooperation. We know how to predict the desires of our viewers, and please them with first-class shows and performances. We have been working for you since 1996, and as long as the viewer exists ...',
    'dnipro' => 'Dnipro',
    'kamyanske' => 'Kamenskoe',
    'subscribe' => 'Subscribe',
    'order_ticket' => 'Order the ticket',
    'from' => 'From',
    'to' => 'To',
    'uah' => 'uah',
    'scene' => 'Scene',
    'engaged' => 'Engaged',
    'reservation_is_active' => 'Reservation is active',
    'order' => 'Order',
    'total' => 'Total',
    'pc' => 'pc',
    'checkout' => 'Checkout',
    'clear_cart' => 'Clear cart',
    'ticket_for' => 'Ticket for',
    'row' => 'Row',
    'place' => 'Place',
    'price' => 'Price',
    'empty_cart' => 'Cart is empty',
    'select_ticket' => 'Select place',
    'ordering' => 'Ordering',
    'step_1' => 'Step 1. Select place',
    'step_2' => 'Step 2. Your data',
    'step_3' => 'Step 3. Delivery',
    'step_4' => 'Шаг 4. Payment',
    'electronic_ticket' => 'Electronic ticket',
    'pickup' => 'Pickup',
    'payment_in_the_cashier' => 'Payment at the cashier only in cash.',
    'choose_cashier' => 'Choose a checkout where it is convenient to pick up your order',
    'payment_by_cart' => 'Payment by card',
    'cash' => 'Cash',
    'on_sale' => 'On sale',
    'concert_duration' => 'Concert duration',
    'with_intermission' => 'with intermission',
    'watch_video' => 'Watch video',
    'description' => 'Description',
    'in_russian' => 'In Russian',
    'booking_24_hours' => 'Booking 24 hours',
    'live_sound' => 'Live sound',
    'age12+' => 'Age 12+',
    'concerts' => 'Concerts',
    'pages' => 'Pages',
    'users' => 'Users',
    'reports' => 'Reports',
    'orders' => 'Orders',
    'addresses' => 'Addresses',
    'cities' => 'Cities',
    'ticket' => 'Ticket',
    'files' => 'Files',
    'control' => 'Control',
    'no_ticket_information' => 'No ticket information',

];
