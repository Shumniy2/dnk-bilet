<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameTitleRuTextRuETicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('e_tickets', function (Blueprint $table) {
            $table->renameColumn('title','title_ru');
            $table->renameColumn('text','text_ru');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('e_tickets', function (Blueprint $table) {
            //
        });
    }
}
