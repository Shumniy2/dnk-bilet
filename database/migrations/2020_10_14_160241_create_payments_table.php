<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('transaction_id');
            $table->date('payment_date');
            $table->integer('concert_id');
            $table->integer('status_id');
            $table->integer('sum');
            $table->integer('incoming');
            $table->string('bank');
            $table->string('card');
            $table->string('customer');
            $table->string('phone');
            $table->string('email');
            $table->integer('ship_id');
            $table->integer('order_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
