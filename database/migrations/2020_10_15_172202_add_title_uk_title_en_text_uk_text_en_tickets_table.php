<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTitleUkTitleEnTextUkTextEnTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('e_tickets', function (Blueprint $table) {
            $table->text('title_uk');
            $table->longText('text_uk');
            $table->text('title_en');
            $table->longText('text_en');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('e_tickets', function (Blueprint $table) {
            //
        });
    }
}
