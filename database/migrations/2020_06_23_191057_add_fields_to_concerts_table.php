<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToConcertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('concerts', function (Blueprint $table) {
            $table->integer('canceled')->nullable();
            $table->integer('codes_without_d')->nullable();
            $table->integer('no_in_shop')->nullable();
            $table->integer('hide_concert')->nullable();
            $table->integer('another_codes')->nullable();
            $table->integer('pay_for_organizator')->nullable();
            $table->integer('places')->nullable();
            $table->integer('archive')->nullable();
            $table->longText('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('concerts', function (Blueprint $table) {
            $table->dropColumn('canceled');
            $table->dropColumn('codes_without_d');
            $table->dropColumn('no_in_shop');
            $table->dropColumn('hide_concert');
            $table->dropColumn('another_codes');
            $table->dropColumn('pay_for_organizator');
            $table->dropColumn('places');
            $table->dropColumn('archive');
            $table->dropColumn('description');
        });
    }
}
