<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('concert_id');
            $table->string('distributor')->nullable();
            $table->unsignedBigInteger('fee_id');
            $table->unsignedBigInteger('shipping_id');
            $table->string('comments')->nullable();
            $table->timestamps();

            $table->foreign('concert_id')->references('id')
                ->on('concerts')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('fee_id')->references('id')
                ->on('fee')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('shipping_id')->references('id')
                ->on('shipping')->onUpdate('cascade');
        });

        DB::statement("ALTER TABLE orders AUTO_INCREMENT = 1000000;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
