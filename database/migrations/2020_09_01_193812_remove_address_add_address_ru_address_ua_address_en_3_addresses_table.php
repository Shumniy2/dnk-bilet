<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveAddressAddAddressRuAddressUaAddressEn3AddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->dropColumn('text');
            $table->string('text_ru');
            $table->string('text_ua');
            $table->string('text_en');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->addColumn('text');
            $table->dropColumn('text_ru');
            $table->dropColumn('text_ua');
            $table->dropColumn('text_en');
        });
    }
}
