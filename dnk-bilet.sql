-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:8889
-- Время создания: Окт 19 2020 г., 17:19
-- Версия сервера: 5.7.26
-- Версия PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `dnk-bilet-vadim`
--

-- --------------------------------------------------------

--
-- Структура таблицы `addresses`
--

CREATE TABLE `addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `address_ru` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_uk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_ru` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_ua` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `addresses`
--

INSERT INTO `addresses` (`id`, `city_id`, `created_at`, `updated_at`, `address_ru`, `address_uk`, `address_en`, `text_ru`, `text_ua`, `text_en`) VALUES
(25, 17, '2020-10-19 16:13:41', '2020-10-19 16:13:41', 'Dnepropetrovsk Academic Opera and Ballet Theater (Dnipro, D. Yavornytsky Ave. 72-A)', 'Dnepropetrovsk Academic Opera and Ballet Theater (Dnipro, D. Yavornytsky Ave. 72-A)', 'Dnepropetrovsk Academic Opera and Ballet Theater (Dnipro, D. Yavornytsky Ave. 72-A)', '<p>Dnepropetrovsk Academic Opera and Ballet Theater (Dnipro, D. Yavornytsky Ave. 72-A)<br></p>', '<p>Dnepropetrovsk Academic Opera and Ballet Theater (Dnipro, D. Yavornytsky Ave. 72-A)<br></p>', '<p>Dnepropetrovsk Academic Opera and Ballet Theater (Dnipro, D. Yavornytsky Ave. 72-A)<br></p>');

-- --------------------------------------------------------

--
-- Структура таблицы `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `city_ru` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_uk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cities`
--

INSERT INTO `cities` (`id`, `city_ru`, `city_uk`, `city_en`, `created_at`, `updated_at`) VALUES
(13, 'rtre', 'ereyt', 'erterty', '2020-10-08 13:16:26', '2020-10-08 13:16:26'),
(14, 'eyrt', 'yrtuy', 'ytut', '2020-10-08 13:17:11', '2020-10-08 13:17:11'),
(15, 'twreteryte', 'yeryrty', 'erytryt', '2020-10-08 13:18:06', '2020-10-08 13:18:06'),
(16, 'ertjeri', 'rietj', 'ierjter', '2020-10-08 13:18:51', '2020-10-08 13:18:51'),
(17, 'wejtuj', 'jierjt', 'jeritrjet', '2020-10-08 13:20:11', '2020-10-08 13:20:11'),
(18, 'terotk', 'okerotk', 'koerkt', '2020-10-08 13:20:28', '2020-10-08 13:20:28'),
(19, 'yery', 'ijwiejt', 'eturtu', '2020-10-08 13:57:00', '2020-10-08 13:57:00'),
(20, 'yery', 'ijwiejt', 'eturtu', '2020-10-08 13:57:20', '2020-10-08 13:57:20'),
(21, 'retyr', 'weirjew', 'owkew', '2020-10-08 13:58:21', '2020-10-08 13:58:21'),
(22, 'retyr', 'weirjew', 'owkew', '2020-10-08 13:58:37', '2020-10-08 13:58:37'),
(23, 'dtin', 'ijwitrej', 'iwrjetre', '2020-10-08 14:00:19', '2020-10-08 14:00:19'),
(24, 'dtin', 'ijwitrej', 'iwrjetre', '2020-10-08 14:00:35', '2020-10-08 14:00:35'),
(25, '23542', 'wrytrwe', 'rwetwrt', '2020-10-08 14:06:53', '2020-10-08 14:06:53'),
(26, '23542', 'wrytrwe', 'rwetwrt', '2020-10-08 14:07:07', '2020-10-08 14:07:07'),
(27, '23542', 'wrytrwe', 'rwetwrt', '2020-10-08 14:14:40', '2020-10-08 14:14:40'),
(28, 'tyr', 'yrety', 'rtyrty', '2020-10-08 14:17:56', '2020-10-08 14:17:56'),
(29, 'eryt', 'ryrty', 'trytru', '2020-10-08 14:24:49', '2020-10-08 14:24:49'),
(30, 'erwe', 'trwetwet', '4356', '2020-10-08 14:34:10', '2020-10-08 14:34:10'),
(31, 'erwe', 'trwetwet', '4356', '2020-10-08 14:34:29', '2020-10-08 14:34:29'),
(32, 'rwet', 'erwt', 'erwt', '2020-10-08 14:34:55', '2020-10-08 14:34:55'),
(33, 'rwet', 'erwt', 'erwt', '2020-10-08 14:35:21', '2020-10-08 14:35:21'),
(34, 'укн', 'нунке', 'нкен', '2020-10-08 14:49:28', '2020-10-08 14:49:28'),
(35, 'укнerter', 'нункеyeyet', 'нкенrt', '2020-10-08 15:31:26', '2020-10-08 15:31:26'),
(36, 'укнer', 'нункеy', 'нке', '2020-10-08 15:31:59', '2020-10-08 15:31:59'),
(37, 'укнer424', 'нункеy545', 'нке4545', '2020-10-08 15:32:55', '2020-10-08 15:32:55'),
(38, 'укнer424', 'нункеy545', 'нке4545', '2020-10-08 16:39:06', '2020-10-08 16:39:06'),
(44, 'Днепр', 'Днепр', 'Днепр', '2020-10-19 14:57:38', '2020-10-19 14:57:38');

-- --------------------------------------------------------

--
-- Структура таблицы `colors`
--

CREATE TABLE `colors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `price` int(11) NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `count` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `concert_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `colors`
--

INSERT INTO `colors` (`id`, `price`, `color`, `count`, `total`, `concert_id`) VALUES
(13, 500, '#7EED62', 3, 1500, 19),
(14, 11, '#1F298A', 2, 22, 19);

-- --------------------------------------------------------

--
-- Структура таблицы `concerts`
--

CREATE TABLE `concerts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) DEFAULT NULL,
  `ticket_order` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `canceled` int(11) DEFAULT NULL,
  `codes_without_d` int(11) DEFAULT NULL,
  `no_in_shop` int(11) DEFAULT NULL,
  `hide_concert` int(11) DEFAULT NULL,
  `another_codes` int(11) DEFAULT NULL,
  `pay_for_organizator` int(11) DEFAULT NULL,
  `places` int(11) DEFAULT NULL,
  `archive` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `before` int(11) DEFAULT NULL,
  `after` int(11) DEFAULT NULL,
  `hours` int(11) NOT NULL,
  `minutes` int(11) NOT NULL,
  `dk_id` bigint(20) UNSIGNED NOT NULL,
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `organizer_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `concerts`
--

INSERT INTO `concerts` (`id`, `name`, `date`, `image`, `status`, `ticket_order`, `created_at`, `updated_at`, `canceled`, `codes_without_d`, `no_in_shop`, `hide_concert`, `another_codes`, `pay_for_organizator`, `places`, `archive`, `description`, `before`, `after`, `hours`, `minutes`, `dk_id`, `city_id`, `organizer_id`) VALUES
(19, '324325', '2020-10-01 15:42:00', 'VM8z75KFkWFsoMzgAsgVHPB1tDoa5OzFt4kKP3dk.jpeg', NULL, 1413686140, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, '<p>retretert</p>', NULL, NULL, 2, 20, 6, 13, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `cultures`
--

CREATE TABLE `cultures` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cultures`
--

INSERT INTO `cultures` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(6, 'ДК Машиностроителей', 'dk_machine', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `distributors`
--

CREATE TABLE `distributors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `e_tickets`
--

CREATE TABLE `e_tickets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title_ru` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_ru` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_uk` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_uk` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_en` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `e_tickets`
--

INSERT INTO `e_tickets` (`id`, `title_ru`, `text_ru`, `created_at`, `updated_at`, `title_uk`, `text_uk`, `title_en`, `text_en`) VALUES
(21, 'Electronic', '<p class=\"p1\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial;\"><span class=\"s1\" style=\"font-kerning: none;\">An electronic ticket is the equivalent of a paper ticket. This is a file (in PDF format) that can be received by YOU by e-mail after paying for the order. You can print an e-ticket on any printer, color or black and white. You can also save an electronic ticket in your smartphone.</span></p><p class=\"p2\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial; min-height: 16px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p class=\"p2\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial; min-height: 16px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p class=\"p1\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial;\"><span class=\"s1\" style=\"font-kerning: none;\">Each e-ticket has its own unique barcode that distinguishes it from all others. This barcode protects against unauthorized reproduction of your ticket. The barcode of your e-ticket will be scanned at the entrance to the event, after which you will receive the right to enter the concert hall.</span></p><p class=\"p2\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial; min-height: 16px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p class=\"p2\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial; min-height: 16px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p class=\"p1\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial;\"><span class=\"s1\" style=\"font-kerning: none;\">In order to avoid unauthorized copying of the ticket, it is STRONGLY NOT RECOMMENDED to post his photos and images on the Internet.</span></p><p class=\"p2\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial; min-height: 16px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p class=\"p2\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial; min-height: 16px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p class=\"p1\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial;\"><span class=\"s1\" style=\"font-kerning: none;\">Service \"DNK-BILET\" is not responsible for copying barcodes of tickets posted on the Internet.</span></p><p class=\"p2\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial; min-height: 16px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p class=\"p1\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial;\"><span class=\"s1\" style=\"font-kerning: none;\">On the DNK-BILET website you can buy an electronic ticket for any presented event.</span></p><p class=\"p2\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial; min-height: 16px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p class=\"p1\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial;\"><span class=\"s1\" style=\"font-kerning: none;\">An entrance e-ticket is valid on a par with a physical ticket and does not need to be exchanged - it can be presented at the entrance to the event as a regular ticket purchased at the ticket window.</span></p>', '2020-09-02 09:20:04', '2020-09-02 09:20:04', '', '', '', ''),
(22, 'Електронний квиток', '<p class=\"p1\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial;\"><span class=\"s1\" style=\"font-kerning: none;\">Електронний квиток - еквівалент паперового квитка. Це файл (в форматі PDF), який може бути отриманий ВАМИ на електронну пошту після оплати замовлення. Роздрукувати електронний квиток можна на будь-якому принтері, кольоровому або чорно-білому. Так само електронний квиток можна зберегти в смартфоні.</span></p><p class=\"p2\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial; min-height: 16px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p class=\"p2\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial; min-height: 16px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p class=\"p1\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial;\"><span class=\"s1\" style=\"font-kerning: none;\">У кожного електронного квитка є свій унікальний штрих-код, який відрізняє його від всіх інших. Цей штрих-код є захистом від несанкціонованого відтворення Вашого квитка. Штрих-код Вашого електронного квитка буде відсканований при вході на захід, після цього Ви отримуєте право на прохід в зал для глядачів.</span></p><p class=\"p2\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial; min-height: 16px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p class=\"p2\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial; min-height: 16px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p class=\"p1\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial;\"><span class=\"s1\" style=\"font-kerning: none;\">Щоб уникнути несанкціонованого копіювання квитка КАТЕГОРИЧНО НЕ РЕКОМЕНДУЄТЬСЯ викладати в інтернет його фото і зображення.</span></p><p class=\"p2\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial; min-height: 16px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p class=\"p2\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial; min-height: 16px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p class=\"p1\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial;\"><span class=\"s1\" style=\"font-kerning: none;\">Сервіс \"DNK-BILET\" не несе відповідальності за копіювання штрих-кодів квитків, викладених в мережу Інтернет.</span></p><p class=\"p2\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial; min-height: 16px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p class=\"p1\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial;\"><span class=\"s1\" style=\"font-kerning: none;\">На сайті «DNK-BILET» можна придбати електронний квиток на кожний представлений захід.</span></p><p class=\"p2\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial; min-height: 16px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p class=\"p1\" style=\"margin-bottom: 8px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Arial;\"><span class=\"s1\" style=\"font-kerning: none;\">Вхідний електронний квиток діє нарівні з фізичним квитком і не потребує обміні - його можна пред\'являти при вході на захід як звичайний квиток придбаний в касі.</span></p>', '2020-09-02 09:20:42', '2020-09-02 09:20:42', '', '', '', ''),
(23, 'Электронный билет', '<p><!--(figmeta)eyJmaWxlS2V5IjoiQVZubTRFNEpwZlN5Slo5bnNEUldLWSIsInBhc3RlSUQiOjE2ODQ5NTQ4OTYsImRhdGFUeXBlIjoic2NlbmUifQo=(/figmeta)--><!--(figma)ZmlnLWtpd2kCAAAA8R0AALV7+Z8jyVVnREqqo6unp+fwiTHGGGMMmJ6e8czYGOOsVKqUVZIyJzOl6h6M5SwpqyqnVZJQqqq7BmOM13ux3Bizaxav8bJgDGvuc40B78HuYgwYc4MxxpwfPnz4E/h+I/JSV5vfmM9nKt578fLFixfvvXgRoX62G6dpdBSH5/NYiPt2Xac3DELTDwX+67lNe2i1zd6OHQCV/cD2K7ihuO1eE3AtcHZ6ZgdQPQhvdmwADQUMA5uy1hSvkjwM9hxv6Nsd1+SX6z03dFo3h0Hb7Xeaw76345tNfr+RgcOm2yO+meO+3fLtoA3SpcCye/YQZK89fKpv+zdB3KoSfdvrkHjZvJOkUPkGYEGCNEcjTB0k3zabQ7en2IRC9n0n5IiyNxvH3nGUxmCz0BXa1BhMXXegQLmfTMfJ9Mg/nZCn5/aetn0XHcJtqn5K0LZ9AJ02SKLpWv2u3aNVpGX2BmYAyNjx3b4HoNbyzS756tuu27HN3tD1bN8MHbcHYmNgW6HrA1qjLdGudxwldsPudBwvILjpgwmLpFbhkm/v9DumP/Tczs0dJWQLQ/WadhPGKfkuh/YNqnRf0HEsEq4EN7vbLlf0fqeHwXqKenV7Ek/HXcxKiIc8MwiGYRvidrga8Be/q3xANk1/z+ZYRrffCR29BjWqCk22+z676pbbcQus0XF22qH6Zi2ArRWkJocvmm5zxwa+oT/J0U2sgt8xKftS4LbCoZIBbKtt+s0Cu9x0Wi3bt/UM7rNvWJ1+oO15pd0n7f7ADPuFka+qUQA80Ol3nZ4bOCGHeNCLkulSL+Z64HYcLrCAmzUdrCZGo6qgyILEVtkDqwuQJJibqwFaraCBqeuqKKo7XVPNrAEP23UArDkniM5gFE1ibXSEl2+HlrJ3y+H0ZMvpqEFCR61kzT48jEeZonWn10PQBm2z6e6jUzR91ytR2XLhH1jAXnO43elTL2PbtPZWSbUwvrO0VBisub6z4+hIF30ProlWdtx9BUCFUOsQwBE6Q8v06Nz1Ehu2XN9SodOg0GY8mi2iZTKb4ps8QDAylhXmBCwxXWfPLp3M6J2eHMSL/jRZpvjGNzkN4Tk37E4AQEIjhD/tYlizabpcVBYNiwm6YL9SV3ZN5gMDY2QmrQWWqSZQb0Fic6i/aGSI4l4LlovZrdicJEdTfFAIE4gSRyUu6fbDDDQ0sxXNISWfH6aiVlsGT/VNnyTD9H13X7kQJ1HTqP1U3+kg5/jK2nUtaneWcNguMpQaddse2OyWuVRjezabxNHUnce5Zev9nvZs6IjPAoQ9YBn0t0PfVLBxQzm8Wmg1s/ZskTw7my6jCT7v2C0yVQwHJ1ChZez2A2RvR61o+fUgXiwT+Cxproeuyqfbbhi6XUBGd3aaxtbpIp0tsDhNu2UiV6BDWL4bwEUdH7C0b9r0WawrMAObjxrKMzEV5A4LvgG87ql80UBjOR1AawOEwGzRTRYLSi98C/Gn1lcqAOGINGH3dkIuvtGM0mMdZYaFZAqSKF1DqkjUnlT3ejsgiV3PZiuDARvDa7bQ1Ow789liebf31SwXJoDSuYuJnLDvNNX4Mie07dy8neh8drrcWSRjLaSuHbJizlJBQ/tnrfzGi5bLeDFFF7gcT/kWEpZKXFIt1uly5sdp8ixEFyZS6ijLFHrIAoJDn0/iIM4mBYP7gZulg9A2ua7Sgk/oFcY+jV25ZzF91kK767m+qfZoOLMWAyst48JEF7IqQJnnRAwdjW7p9SmUbSMfPQ2zKQ0ktoPQgXkBa27ljWC/YDZtK81kzU6hxCLjbXxW3sLSNbMfcpPHLPD57mm6TA7PgX7WLz3TsocIVF1k1NRngbK6oVILiCgqAudpexi6wyytwzRTODEiWFunyB/wfNuHzYesz4DLvq9mvI3EjbZmdVxVB9QdTiuqiLjs9obwbMUmzBbEDEOnayNfAZddFwXeUM3B0LDuqOGrNrdawHXdgd2DbA2NqcJlDVweJkEnQ02nVN1o+iYjYxN9e/bN/LNLQAeuLjO2wkU0TZNSxxcgj6LaCIfITcio2Z4smk4A7xjYAGULVSBaA6UKysGW73YRxCpF1SqkPEfVKzSdjRoVSpGO1rx+0Na0TNh6ScllbZQkLWqzJBSSLrE61LRM0lZJySVdLkla0n0loZB0RSuKZQBTLuz+FWIu7+oKVYt8YIVWSH1QjZRRM6EPVWm5zIerRC3yOVVSIfG5CFHHGrIP2POw3aPMN3uIXFVHP9+OUhTJen03cA6w+tuOhQ5BQTkiUWpVUIO7oa6U8AV9u+iqk2+F0tDfrtDWdB4q8PVgtJhNJs1koaMKcjI3/WeyCKaoIl5/i5BcMqDiMUJ4GaPfvuEhU+oItiCB+6nC5E4fqUwaKQ4ZGAzwupCTGTZFBaI4mWBjkvWF2BTyCH+MA/ypRfhT13sXPr4DTJ7jj+GDBO6ScBt/asf4U1eSguVsjg9GhEUo5HymYwoMRjdaLpI7Qq6dXLsGXJ5cewSNcXLtOpraySMk1k8eIbFx8giJa160QFZ2puMY3xlHp8lY+BWhW3kZhs6zaHIa4xt5qkqy5wujBSv1opNYyNphdJJMzsEvUyZ8AAaELNPRIpkvgdXIO4gWSYRPTk/iRTJqJUenC5gWKT47TQh4gqM2XOl2mupUCVgNs/ppMI9G8LOVbz3UDi7WM9ukZGhuZwX4PQS0uLicYFUCynOcmxSM3RYepta3+rUVzVP4V/kJAkCV5BLNMEcMz0Z5TNVrIAwLjNWZZaqM3QAJk90BuFaR7+V2r6qF6g1/UcRhCwag9AmUkbE4BZcDn1beL1EPqgzbiqOlMvBfSg8VObqEdd1TLJkWhuUFpNeoDVqlINpGdrZdC5weC51112/20G6YLZ/9m82eygaXev0uVdrCAcxEexlbDqd0X1O3V9q6vR9FPturpqnqvgcs3T7oW6p9KND4w/5AHRGfw8BE+9xgX53+n2cF+2yfj8Uh/QWW1aXeLwz0Nv05bScg/UXcWNF+ruv3qN+LaRS0n4cNhkv5kmaozg6f3+qYnMdLuzs+d8gvCOBraF+2h+0W7Re2UBqhfXlbt1/U1uO+ItT4Fz+l21d6uv0SFsdov7TT2ib+Za6n2lf5oWq/3NPfX/P2erTTIx2kD7TX0VLPR/2wQ/wxtMRfbW77A7SPm9sD4k+gpd5PDrSc1wygENrXbnf2uT5fgZZ8r0NLvq8099qcx+utXVX0f5XVUoHwBstTuGn1ffJtY68lbiG5sW22tHy7hYMf2hba62h30D6Kto1hOZ6DlvJ323o+GG2H+nTa7i79BuWRqmx6DnZytO6u98STaL1d70nKeWrXe801tP6ud+0xtEFnt8vvwo5rkb+PbYXrMujaTZ6N99FSjxvdvS7pN3sdVdk83evvhWi/GuUI9Xoj2gDt1wxgcLRv8oKQ9CFa0t/s7/nEI99rsz3w+9tc91HQ9cg/DrUecdhTBe4hlonrdzTArQHa44HuTwZ63s8M9pS/3Br4oY92gvY62pMgQOYVYoqW+Azto2jnaB9D+7VoX412gfZxtCnaJ9Au0dJOp2hfg/YsCJCzhbiNlvLuoKW8c7SU9yxayvs6tJT3FrSU9/VoKe+taCnvG9BS3ttkEFynwG+U1kBp+HYCFPmvCFDmOwhQ6L8mQKn/hgDF/lsClPvvCFDwvydAyd8EQKn6HwhQ8jcToORvIUDJ30qAkr+NACV/OwFK/g4ClPydBCj5uwhQ8jsBKJ2/mwAlv4sAJX8PAUr+jwQo+T8RoOR3E6Dk7yVAyf+ZACV/HwFKfg+ARyn5vxCg5PcSoOTvJ0DJ7yNAyf+VACX/AAFK/m8EKPkHCVDyDxGg5PcDeIySf5gAJX+AACX/CAFK/lEClPzfCVDyBwlQ8o8RoOQfJ0DJP0GAkn8SwKsp+acIUPJPE6DknyFAyT9LgJJ/jgAl/zwBSv4FApT8iwQo+X8QoOQPAXickn+JACV/mAAl/zIBSv4VApT8qwQo+SMEKPl/EqDk/0WAkv83AUr+PwCeoORfI0DJ/5cAJf8/ApT8/wlQ8q8ToOSPEqDk3yBAyR8jQMm/SYCSfwvAk5T82wQo+eMEKPl3CFDyJwhQ8u8SoOTfI0DJv0+Akv+AACX/IQFK/iMAKkX9MQFK/hMClPynBCj5kwQo+c8IUPKnCFDynxOg5E8ToOS/IEDJn5F33xqgtFpiuxbXhMxLLIM1ZTeaz1nkSONwMTthWbac4a+xPZkdCCkPzpdxKmpSX1cIo4br62PiU1ZkqL/G0TJSvOuovpIJTogWi0Zz/AzOsUJuLDk2yrn0OBrPbqcAjePk6BiH42OUdygYx/EySiaA6jFUTllLoHA8w+E5xnUD4LVlfKIun3TX+llygDPeiPCGukPVw2YvD8K49C875AiF0SLC3DbF5sGCMqcYGdglpYwwHlB2viLkiIZA9WzMWEguWWfXzpI0OUBRJUUdTXb1fVk0UhTcqXharkH2ND2cLU7EG8V6oox+R2woIDxGkTyl5qjboyloODk47AHhqiagrEPViaVZFw8Ar971XhWXFjOcM8ACTbZSdgC4fKjMZ1HZbNWeFffNOZeW6hFvEVfik9kziQUpHu4LYcR1eT8LxC4M2YQDCKNxKz4XcJhDUDvJNG7HtAzEG6Q0k6MYcmuo4IHpsnIq6kT2NWMDxSpukLSwrdFxxNI5XqRwMVlg6kOnyeGNlLB7Fi9wMRWHEYyJpCBrE3VbpS5DBjAxLpkn0CbF9iIbR5Pz+XGKfUWujYuL4hS7ilzXnw0wIEiw3QZVK2b3Nik3D6PJ5AD3LC10pOJAXjrGKi8g/Nb27A4GeIeUW+0KSRj1A1wEjVNxA2eZxQQTyQ8+teOcD3VdAy8zmZbCWIc36tJ8IOTtZLzkicxg300ANQKFYevEzHSEgxWw9cNkkS6t3FKYQgPeVcXXdjh9YayNZicnERTLorY8hg2Etiq0QjAfYqLKjhjqovBofJYFxFqzsKUwjAUOlpiylKUkQ58/lSWN2plCevHy9mxxK1dhCn+PJhhsrEbMFbm4vMxYuIbENCSNmQpfyuD85GA2ycSnCsG4SGQazoWkFGDgVMngCuj8LcwGAQrD5mLzZGgYaqHkHDRUFLgawtyhnBLox4cxjrOYvLF5mEziPXg+vDRVnWpkA0PSkdoRciVOpVTVg6hMfIoCRNbzzNqYJEgui3PqEM6C0wOeZg/ARoJYSq7XfDbFMuuB1k+nhxPexE7BU5W4kaT9vCseI8Nsaq2t/PtulGL1MkONcqqWKuenB5MkPYYwjkttw1kYRyedUjsOYtw9SPY8BN9Vae9BUUl7h4dpvMRq1hbRODlljqyX+a+Bpsh/a+l8EUdjcKyHTIDKIZ3p4QxLoeTuCjk+zbwMIeEhk83Y0YzPklF+jZ7fv7BAV/f40sKRSR0iDUXDlQyP7sBr+kM/T4g85OqPLWt/qDZledcgiGgiqPGw2JmbQmtM0RnDjslhgtDAauMrLfM9SPL0PiQXL4uxkALw7ghN1DFb4BosvzOThIseg1h+c1bDvRHmkXPWM7RgbmSEnH+ta/b66nCynimwjdR1tGBCcspbXIxSzJqXvHi21ve5vJHLnpzkBQF6DsWXOHw5zWH+xnmR3ZzPYyQOFS/GQUFWUn4QpixJVu44vQh7sbKh4sLCmgOc/dX1hsClYPZIK4N9deFgsOWPARRD9l4SYCuHM6fC2EhPDw9xYQUnVzuikvkqgcutojZaiFp6dsTI6HHvxLoBRW1E5/wgXBWYe7pkwuUmhX4EJcyIncCd4j5KinVwtGaLURyoVzVE2q0U5I1Ml8FOJkwYTmvYs+3sis/s7Js3AwCyozYgvrIgqyyp4XUhI/7IwEA6KYKmNj09CRBuME0qsHVkIYbqJdXUgI6JpHx0ighdZNj6KLPsxpyBiyecx8TmDpISlqSWDSILUcU25SEJYM1uJ+VvEjYEwntl+8CbBzJ1SI15ZaV9IntCxv2W7+6RYmQ/B6jZeDpXtwN13AbgIg1QI3tCWtPpSsmr5Fi9FWRZCs5VSax5WiYDTI6ZcQkxp5SU7BM8B5Y3XvimTNnZLex2fAx/g30gD0+FHXV5gyngIWW437YREm2n0xy6LTy1sRsXarhX17+qkOZiVIwZ4ZVyemROj2AolJ/IbhXUSPD8svDzRFjzJqdHyTT7dq4QWAH66uocit7SSnqqz48n0el0dHyPD05QfsKtAcKl1doArPGXItixp+gw/k5tllhS1YbREdbt1fNjlB1iTRgK0MTH57BnflH7JlGroJrhiSUX+pJQmVCTnpxGCJkt0WCrSa8pHXYtA3XHa3FjfUttG+sa0uSvKAvkjQzUHa/Dl8UesVkguvMrR0gaSwCXFKCJr0/p8gOUt2g16asQ5EVJfblAdOcbxnAlODp8BxuLvK+CagYzUo+anBzUvlJiunu7TF/2lGUKJ3f/BaJmtk6QF9B/la0mNWOVIazVdPLARapmt/mlk7o6GYHzwRWCZmqVw7vamjDsQxeImnkHxVvVMR+u4pqlXSkFn5PDusuBh0RHi2h+TCfBamyK595F0oy7BTW/Vd8Uz7ubpln3GEkOo0VVaWB8/ipFs3VOEqxSJ0EDlhegyTDd3U1VJs6OF5vihVVcs/SWSOYh9sJbGBssn1PFNYt7XPxwAYsBEWq7eKF40b3o+hMPGAYycdyciheJz62gmuEpTbGiuXixeHGB6E5f4+pnGS8Rn1diujugikpWu1BAvEy85B5k/UFY9AzyH1G8XHz+BaJm7pNuIRuIh8VLc1h3DYhWKv3nii9YpWi2/YO7fy3yUvGyu2ma9cZZNnZpQBj2Cy9SNftN5O2JRzwVb5UvLzHd/TSYYSlNAsMXVXHN8tWMkOz49GbxihLT3W+kZ/cQ2DjGfnEO666vUVNl5n27FK/MEd33pljVvymuB+WXZLDuGSL7jrExqd+MwKPEK8WX3kXSjG/W0R7kFcuPSvllqyTNF3FkUyWgFBEoHhevWqVotoNJUUukuNyTX17BNcdIH8I4CdygimslqvvHav/E7rIuHslA3RGXScTKSorrd5E04yHXZyeencTLxTluD+WjVYLmOdJLlBPJ9dgqSfMdI/qzH2q8TiQFojufUXiWPxDDt6q4ZpkokheNuR2D5aSKa5Yp90WkfXXYm+WI7pvrgx2tg6tk8bUlqvsXh7xv6SINN5NUJXuk5fQCUTMvF3qhZi1kLSlwvVSgmuFML/w2NNRGzb8H922luAUqUp0KX/EGcUcRd3Ftwx+NbIvzVNdESsuyjHqnFM8mqaZ6+qhHsZD6dYCKD6rnybeM8YZ5lvdwwjxBf32VfaDLMez+b2XEZO+LFqqQ2bTDEp4TxBjfsNIL9e8sT6NJleNt6uYoY8GcR4uYqQInzyrXN1a52vAFRBFyRZXl7VUWd4E1RsqTeHmokIMJdvl4/HS8mKHrHdWuXvZyq1+Nx3ifuNiZeZc4xK3Sxd4WNhCqLo7xlFHpRopPxTO4farQirp0gkcOuh2m8JNSfpNE5ssOwazjEIk+3jzmOFmq82iA7XBZdHxz2VG6jYpFhOe3SCRAFDPRhEUEZvutcnamLkSwN+tFVEK+Lbu/aMYwPR7TMTJW9dsl73NQRGLTnc078SFWr6wOEEvfscLgM5vexfGdJcf2bLmcndxDynfdzXMvQe8smcqehDUGXqIxGcbTd9/NE2LrWWV5F63FEMMMU/giMk6EnYDx9T1S+zb8V1+NIH5hO+X575JI8GDNVkVdTbxb4imopIVYANxTvLtCapZXFt8r46j41UoHlwiwMw4f5c9HenghGimlg5PZbMmbGHz2XplMj+FWvIudBDoXY7nek5MDlTDLjvflHSFirCT/UE62VXopO95fdKitq+z44byD+0ZJ/kBOrujT4u9FqAb6f0EmadEFm34fcN2ZU75fpoSUHX5W4slLoat+98vyNL9kghmqSekH5AR7ImyUxeCb8UKGL2FJV59fB7hUgINnaelHsiUNOEZlOX9RYpetdJWJ8ucl3sqSdDVHflDGym40qpniOE8A9B8DZzA7RBkErTJRIP84yL3ZtD8fY8vORPxEpiZcDv4xUtzoFR4CflTSsBtgCj8lcWEBRzxOJmOo1UzOkBp4ufTTFefykPDixRlu+CkXQ/wMBU2xiuhU5m3BwCWJP+vbEj9H/9fZIrvWeq/EpFMlpDgKf0jCRCpzQC18G4JdtPGOVw4fJicxygr46IeqnN0ICP5XEfVLEkjeUwmGD8txjA1rqnCcXrBgKFLwwa9UroN07YZa7lflPX1uu+CE331ERtlJ/MMSz4Sw1urW1VE10CBzjAZ2silmim0DqJrcByTeEmdnSpE8LauO90m8LWYdPIUvIThnwFL9Wt5XquOUc8ZDLt4hL3CY5Z3Y+yXeJ9WWkOm2hWfKJdyrj8XtrGi9jndLffDnjGAr+VE81h0h2Y3dqRu29C9xUjGXv1HQDw9XOj5WWcDgeHY6GQcn2EtM9XZEN/1NmbKe0NXF6/H2qdDyVJGVTVjD39ZdUE7V62XHx3XHvnq+aIrf0agutYF/QpkDSUXdwvh4MZ3rWMPq8h7h93Jc5ZvfhwNn2ZjsW3hFTVLrOjT9w5wvHg8yA23hRVUbiAtVXJd8RMo/hhioD7dcBKdzhnKWpZiZTG6XTLysH/5Eq5tVWYhHTqqJ99hCQJpJ+CwCPimL5x2YD8+2t+LzcJEcHSGAPybFp6B/wMjfgSfMwf/nZUBWHCeF08lPy7MZItQ+w+y9Yzw60j5/AW/BRWP/syTIz2j9vQVWdHFe6P+XK2S1vA4eOVNUjH+lu7IZV7peL/4662Ieyj6F2vyt4N/onmzVfbW2m+JvV6h6Lwf578qr9MoUcZ+uL6Vizg9hIgzdkSICZC1Js6wKG9WL723Fa6wlpZwQZOQmWSGtJCHcvBUdlRxUW12X+j1M3SiDpcx2a4UuphIpDH2npNiKoqwSZyFYURcYJaWqxUpVUC95ygErWtwzZHkXnaV4JabF+96copP+BkNucs/8tXkxD14qB7ywx2xpfVsILvbjyuxyZsSm3k1EY8++ue2aPq90Rb+313P3cYXK37Li7lS9Vsgb2+4NvDXYgA0veAxNLdh3Qqs99NQvAut75dIYvAflyzPuuyWsqqjZWB/F4rahtA42w9DPrKqQxhIYgyS+zUCFo42i6VmUsmhAhYkEjCdKIefYYyeY4Rn4MDVD4c2YU+eNWU1/tKOl1dU/L6Eo/S9MXiGk7ldCO7ORMiEMalTIATxY2YkPwjfulmhNktEtgSe1MVI7nmHhp/xe27pYImlMMXvwZ3s0Urquc4xQGwNZXq+ukLWZhrIR5PFsmc5nyww10tvRPINrd3+sMqXRmGks4/rnBMzzMHDK8Mr66tln27jJnuOeY+mMce4QDdg6RXxhX2YCxIIdIM4CHPViXmVjuGz0FFlYYnq5F5ZFsjBarr+fOZdv4ykooBdJHD468RlWE18fIAPlN99rM4SLGuyTeGqaxrcLxLigY5M61gDlswEFc0nStuZ0pr349l1TwKTGhXKfQiyuKuDxah3ezV8qOnieUmqb266vQWm53a4TasRY/XQvPj/kHQIv+5WUz2ACsPkRFOBxyFgiQaTL6GQOJHvJyPYBfHKwOqvcsDwK3YGzGaniL1wJjwx6bF2ywg9wQl7mRtUKiU9DzkVyADvgfPZXsKgWmimRYouQtaqSdeXhKGrKWGn0kV20HGE8uPLb9iyF6Qw+pSHwqDHKTrV15L6jWOe2BsNXhfnHYf8TFaWfwANDmsee8OXG7AADnUEtsS43xzGzR0/LvITVw4KqWExRfMgtPY08/lJUHBIZfjVaUlQa8j4MtYC2W+KKskvOk/kLnOf+Fbp3j4jB5764usxZcvFNRiOC4IE5sHJhUmyl8sEACqN8wL35U6cxLs74RIYdAbPrZZkCB17UXlhnErR90268jCAjEoY8wNMd6p94wvqQHgB7d2F82FQYD1ND+KOsLocBFRRQm5YSUUTLOm2Q438vJV71IVucybUUdUQ0yay8Ho1GECfqYiNlAR/E6noVPZs5HnLYJ8WlHLdQb2FhFPm1YmsOS3JrbYjLCsw0H/CnSkBRjDFkIPCKHtiLziezaAzC/emKvRIo+g9SXq1MpDDNP+Lp5RCSBijlMHXM90El3Uk9GjiaTM7d02WajGN7OprAQ7ATqiCV4iHF6MGESHa+ePifAIhNAADtvXucllPb/39e1zldTWkzKoTUSKWUSkI013mN7ELcodvudpNbyC4hbtyMaT9tVNooSaa0pc20n2rKlEo7mSGMJElSRKlQVH6fz+fcdMzz3L/f8/z++b6+39f3uV8P6+h8n8daxzrWsdY61jqvPLFY3HGdKvlHD6ypUjXPcab1iznVql1+a7fHWl/V+rruD9zy3HV3XtrtqStvvu36O5yaTi0nlubGHCfupMUqXPn4fU8/dn+3Hk4ilv6S4ziVnAynhuPEHNXpnO2kxSt0vPfB+zMvOPFGFRb4n4rqMSrUpNwqZ+OGDeE/fPBPVDQ4lsGaYqipaoWSmSVLSxaULCntVZpbsrxkWcmy0kElizNL5pQUEpT2ytyROyKz9OWSJSVz8WhuSQEf461eeKe0d0kRHiyE2vKSeSXL8QjvgJf2KilonlkyE+XyzNI+eGdxydLMxiVz+YflaAlqYAsyO155dZNmmah8Of4I4DdehOoWshZUWDoIpg3OLHkXz5aW9i7tz9YzS4aXDCsZVzImE00X0Dw0Wq4TvUuHSKe0Px729sWefCsTL7wLAe2XDsosWQTbluAflM0zS/LxuCfwAjSDF/gP2v431cvMqLOhxXCDbxAsHQK8vKQILUOpECroLqQFzTJL+8GN0MKbeoGVFGaiRXI8Ox+aC/AMECZNhHFLMlH5gv9/dqAfy0v74sUC/LEQKvAh3N8TrxTAz704ECAL0MRbHAAO43w84jD+54bwb4ITTbGTC0p74o3BrHQuXliciXhAW6psaengwLzSPLyUCxP6ng+yvGR+MzZXbrz5B/gALihApejGAr81PIfRqH5BaV/+Cc3hjaDq0r4wXRGGt/5TI5mlQ2HUUvwbFUJxqIa6dABegUbQaCbMRSdQ5zJ0th9MZ2cLUdFyKPNp1HF0EC9yLJfjtUX48wK4C6GI11H3cNSdhz/qXeMkmDgN7ZezrNzL/01Xc6qhOZoMu2EJfcy2c/H6XMpBoMHS0r54hpfZO6gWQY1vCZcORX2FjEJEOXuEFmADHgatDuekAILrONc0HnhF2gVoCa+oVv4Z8YUX56NJOBd/pmsRuP7w0MXweMlSuGA41eg1BvZCPITlrPS/7X067t1yCD2BV6CDDqBZWDQWK8LEklElI0rGl+SXjCmZCnkCno7JLHmjZBTn9ij8aTxWjVF4MLLkLZQT8cZs2A9zl6CmgpL5kNAAXDwYj2l0OHFpK7wfjBomD57DskL8Hzq2HCNEB2Ga4k0Yh15PgEYuaiss7ZlZ/8obrz+/3bUdrupUPxMvLNC/Snvin16ZqgrxhEYZ4JAA0XX9CfXDi//WA6jlP4U9/oHVJ/yCPzbLVP+4nvjWYXA4nbQY4B32dAwemm7C+DfQKDgWboLMle9EHVi5lDHFymCjHwgcOfYfPcLbWMv+U1DDAMRDZFcmnqFT4SoJd7AazChVwDjDa3gHLRD+2xCGkcP9ANRrWGj+61YxK9AhtjEXE0qPYAheZ5PoOrrMseWIcjVCf+DWQi7SUSWoGX/EsPB1NNEbjpgPfSBUPJR+xStzaLNe4fYJkVGDZ+Xcxv4OxAqF3rJTHAg+Rt1zg46xBToKlQH8BwcwKLQ3oDkMaX8Y8x/7q/fF0Rq0aFLwFsyEOkxGEDRHJlHRCXKJkwY4uaf0d5z3/KQiz8n9XXlFP8d5pqpTTcnFKb3x4FTnNOf0nhDOID7TqeOcFa/r1HMykaPUd85xGjqNYjHkNH5a4vJfSG9izrlO45ceeGCX0+SGx7v1eOr+J5+8t4dzy/2PPdTu8Ue7OCeenR89Oy9WMpM9+v8aWroZAfA/WYqtXmZGnQ0thht8g2CpP/8Q0H6kwGF4E7HS7H+3LKVq5cr/k6fQSf/FTokO4kWO5v/kKayVf0aE4cX5aBLOxZ/pWoSuPzx0MTyOPIUhNpyK9BuD+/+WTIUdnwClXFT4f2CyUrUyvITllePzv2+2AiuH+3Go97De/NfNYnKgR2zkf9IVqcNkhAF86aSFWUqF6jGn97Q118cHOPlx98GtDRJpSE62V+yFf1VwEt1wcdKn0zej4nnOzb+4JAZ3S5ThWsXp+/bP7cB/b+v2wVPDy2KJcXH/hSW4MunrOF0NHRdPvEHab/Ozp6P1bSe5fY//+ZV54Y14YjjuWfjCX/HCgRoucqhm5oXhbqJvmv/CKzDgzvf4gm2ib1pid/DCBzAAdJKhu9MSe0j7P1yUhvr3nsIXrP170hJlFfwX6sf7OcUvufBYNfNCWYXET8EL2agf1Jr3U4XEvoDeDfOOHuIL15kX9lVIbE34L7yAF9KK+YK1f2sisaSi/8JYWLi4Fl/oZ15YUjExO91/oRA11G/EF2wfZ6cnfkp3Et+mM5PkOLsOqYOhzcns+4gTi8Xx3n2WxBI5Bz66Addl8d5O7u2WxBM52xpc7cRckj8scRM5uQ9n4RoujhDoYElaIifeFH2oAFL8siUVEjkr19/rxBIg2bssSSRy+hRf58Qqxvs6uXUsqZjIeXb5PT7p3t6S9ICkg3TuYUmlRM7uP6Y7sUog+dMtqZzIGfzog06sMsiwTy05KfFi8ZtnOLGTQLJ3W1IlkZPe869OrAoC4tlKllQNaouB7K9nSbXIo/2cktaWVA88mgYyrL0lGYHfKoM4d1hycmBbVZCyey2pkchB4euUPWFJzUCnGkjB85bUSuR8+mwTjmk/p3258TklGNPqIBkTLDk1kTP8RxD6YPt8S04LfECd2zdYUjvQyQBp/JUlpydyjk6/wPdb9gFLzoj81t8Z5lpyZuC3iiDFVS2pE8RBGkhRbUvOijza3zmUaUndwDsngXRuaEm9YLRPBilpYUlmImfI6OZOrAbIlZdacnYip/KMcexpf6fx1ZbUD3paE2T+Xyw5J5HTocpKvz+r/m5Jg6A/CZDirpY0DGZJLZDaT1nSKJFTC4J0isqN9rmBDn1Qq68ljY0PcgdY0iTwQRWQZiMtOS8Y7RhI1zctaWpGrmy6Jc2CkeP4FM6z5PxgfOi3/UstaR747RSQF1Zb0iLxQvuFB/3+dFlnScugPw5IyQeWXBCsfLQg7XNLWgUW0G9bt1tyofFb6x8saR20cypI/j5LLqJ3pvr9KT5qycVBfyphyW5ULq4voc6D9GiesyrdkjaBRyuDHK1qyaWBBaeBtMqw5LLEi4dvuY+zMc85XMOStsFsZG17T7MkK6jtVJD6Z1iSpG1T6Z08pzjTEi/wTgWQvQ0tSQVrfA2QHk0tyY5mSZ6z6wJLLg+8kwYy42JL2gXjEwcZc5klVwRRxf4UpCy5MuhPbZBdl1tyVSLn6oVPObHTQfKvseTqRM7ze6Y4sTNAsm605JqgnTNBrrvFkvaJF+ve2cWJ1QGpcasl10YxmuckbrfkusA2FyTrTkuuD9Ze+rryPZZ0CHxN71TrYskNgXeo8/uDltwY6HB8Gj1myV+C8UkHmfWkJR2jXTPPufU5S25iHDzo9+dQjiU3B/3haJ/X05JbgtGmbeP6W9IpsK0myKTBlvw1WBM52nkjLLk1GAX64NkxltwW+IC2jRxnye2BbSeBrB9vyR3B+kYfZE6y5M7AB2xn7duW/C1o5yyQnbMsuSuR8/Ar5/sWjJ1nyd8DC+qClCyw5G565y7fO+3KrXz3BN7hevCPYks6B+vBySBjV1tyb7Az1QPptN6SfyRyCg8mfY92+NCS+wKPVgTpstmSLsH+w/7s+NyS+4P+ZILU3mrJA2znRXwhBcnYYcmDtPplJ1YfpP1uS7omcm6v9qMTOwfk6D5LHkrktPxykm/BsF8teTiwgFavOWzJI4HVXF2uPG7Jo8HqUhn5dN+YJY8FtVWL68QTPu4WJEgxJMq5jSx5nJ25ge7s7WRfZkn3wJ1MlLO/tOSJYGIzUd6eZcmTQUgxUc7PseSpYJIyUc78zJIeQXgwHd5fy5KnAweQbM+25JmAnAxS8qgl/wwCpwFI/nBLnk3kpDV9n05DQr7BkucCp9UFaVmup89HQd3P6Zxmyb8Cq+uBZJxuyQsMnCS9gxS6mSUvBt45GSQ3aUlOYDWT3q4dLXkpsg2J5R2W5MaMcemPW9QTSNY1BCrsbVEvoCAUYN4oi3oDBbGAFP9ti/oAKRhoYMt3LeobCyyUGWst6gctmUHjC7da1B9IxtOM3AMW5QHJDOYcBa5FA4AU+Ex7sqpaNBBIzk0H6nGKRYOAtAQy8VlV36LBofGNgFqea9HL0No5+UzfjCotLRoCJDNcoDGXWjQUSHODWp2usGgYkLTY1qoOFr0CFLWV/1eLhgNJi3ljs3ssGhGLNmUkwvdZNDLs17lAuQ9aNAoVrsns7re16kmLXgVSWw2BOr5g0WgghU1doHF9LRoDpKFknpw7zKLXgBQ29YAKxlo0FkjThGbkT7LodSCZwVFuNsuicUAaZWp1WWjRG0CR1v53LRoPJK2zgBqXC9E3gYKtrr8zstSi/NCH1YCcTy2aAC15g4eG1l9ZNBFI0UtvTNpj0VtA8gbDZvNBiyYBKWxo/LhjFk0GkvFMfL6PWzQFKFhU85wn0y2aCqSpx21lfLmZMg1IiydRnZoWTQ8R04ixtS16G0iTiLlUu3oWvQMUJVPbG1g0I/QhM5ZTz7VoJrSUslQBerSZRbOAVGEMqHIri2YDBe6FGZdYVAAk9zLTaZVl0Rwg+ZAW3lhuJ5kbWsh8r087i+ZBK0r4fm1v0XwguZcZ3/EbLFoApJSP7k2Um8sLgeReVtjnTosWAalCWlhwj0WFoYWscNe9Fi2GlipkitDlQYuWAGk6sMLzHrVoaVgh3du5m0VF0JJ7GwCNecqiZUDaPzmU85+3aDmQhpL9qpxr0btA6hfde3G5daMYSO6lhdMGWbQitJBp5raXLVoprbv86dBxhEXvAWk6MKLKxli0CkgRRR8+M96i1UDyYRrQobcsWgOksGGwbZ9q0ftACjaacXiGRWuBZAa1cudatA5IWnWAxiyyaP2JxTzPGbzYog2hNxoDrVxq0UZU2PLLLv7p+NYVFn0ApOMxzbhujUWbgGQGu3zZBos+BFKX6ahbP7SoBEiO4ihv3mxRKVA0ys4Wiz4Cikb5ye0WfRz2i+M1bYdFm6Gl8WKIXr3bok+AFKLUyvnRok+BpMW26hy06LOwrXpx3uS3C5+XQUXbUByZseNZ9DmQvFQBCbDTzqItQBriikQHLfoCKPBSH6fzTRZtBZKXmhBNtuhLmPdkciAnF+6RP7NoG7Q0ueLITjueatFXQLIwBpRfzvjtQPJSAqhzV4u+BpIZNYCKX7FoB5AG6xSgjqst+uZEfCJN32zRThgv354HlL/Lom9R4aTucC9vTLMTFu0C0gLFS9vbT7foOyDthsw2M8+3aHfYVk2gwxdZtAdaWniZvubdZNH3QHIUK9x9v0U/hBXyVrm4m0V7oRXs8rju7WnRj0ByL9tKf9Win4DUFvvV9W2L9gGpXzwaHF5m0X4gzbsKQF03WfQzkIKNCXbeTosOAGkoKwK1OWLRQSDFYUVkFGMqWnQoREyVR9aw6BegYJfv7+ysZ9GvQMHk6u9kNrXot9CHvHcd3MKiw9BS9PLitc1lFh0BUoVMonpcZdHvQIF7YcZfLPoDSO5lprTqDouOAsmHtPDovRYdCy3kTXfXByw6Di3FfAKow1MW/Qkk9/Ku+64XLcqNB8FG9z46yKKeQHIvK+xaLjZ6AalCWlicb1HveGAhc+9mky3qAy3FISu8fZZFfYFUYVOgroUW9QMatyqTEdXf6b7Cov5Aiihl0RstygPSrGS/9n9i0QAg9YtDuXe7RQOBNJQclMZ7LRoEpEFh7t35kEWDgaLce+Mxi14OvcF9rU7MoiHQ0r7GRT4jYdHQUKsh0NXpFg2DlnzIHWpvFYteAVIAMIm6u6ZFw4HkjQpADWpbNAJIPuQe2ucsi0YCqcu8fu5+tkWjgKL752qNLXoVSEPJnfeycoveaCB5nlt5YSuLxgBpOpwCVHKJRa/FoxUb9/CXWjQ2dFSzOD8rnx4+x8dqeOkhquCe6CGLxocqLpDzm0VvQkvLWgZ2w+zRFuUDyYE1sa85tS2aAKQZdD5Qbp5FE4HqtPg7vYTvrV9Z9BaQvFQLe0326RZNAgq+GOFS6BKLJgNJqzJQy3stmhL2qxrQ9ictmgotxQy314LRFk0DChYofMNdYdF0II2IC9Tye4veBpKjuP5PS7foHSDFDNf/2uX6NQNIkcb1v00Li2YCqV9c/3dfYdEsIMUM0fY7LJodIlZY9rhFBUCqsBZQXh+L5gAF7sXt0AiL5gJJi9tr7jSL5oXuPQno9jkWzYdWsDXge+4aixYAaT3RzvulRQuBAs9j5z1g0SIgeZ6r0M64RYVAci9XvDonWbQYSCHKVWhaLYuWhMZzG6p1ukVLoaW2GgJdeY5FRUAKG5rxQjOLlgHJDC6G81tbtBwoWgwzPYveDc1gW4PbWVQMLbVVHajseotWAGmd5K5R/FeLVgJpvLjL1+5s0XtA8jzN6PSIRatCM/gdubhc2KyGlu4vuWu0fs6iNUAKNu4aLctF1PtAaov3IY8OtWgtkGYKx6vxaxatA9J4MW2YP9Gi9UCKDbq3wUyLNgDJvbRw2EKLNgLJQqZDg8vN5Q+AonRo6yaLNgHJeDqq9haLPgwd1RyozzaLSqC16pfdfmxs/NGiUiDFRgWgtF8t+ghIKwCPPAMdiz4GkhlNgRonLNoMpASgCdCzlS36BBbquOECDa5q0afQkue5Q91Zw6LPgBQ25wM9f5pFZUBasXmw/amORZ/bbWhHXYu2wAw5imbUPtuiL1BhZMauhhZtBZIZ3Hm/P8+iL4HkQ2p92tyibUDS4lYeLzf1vgKSe9OBBl5q0XagIABwp5Rt0ddA8jz7VXKVRTvCftGMRHuLvoGWzKDxlW+waCeQjOeZd2tHi74FUmAzAGrdatEuIJnBL2Vz/2bRd0A62zKxebuzRbuBNImYUXS/36I9QFrZaGHnhy36HkgWsssvdLPoh7DLrHBud4v2QksV1gJa/7RFPwIFewo+qT5n0U9AchTbWptj0b6wLWodz7Vof6hF4+P9LPoZKDK+7kCLDoQVMtvsNNiig9DSKtoQqP0rFh0C0trLLm8bZdEvQOoyZ2XZWIt+BdKspBl3vWnRb6EZ7NfwCRYdhpb6RTOWT7HoCJDMYJfvfMei34HUZQbAM7Mt+gNIAcBgy5pv0VEgBRu/C/dYbNExoGAVxaXicouOAwWrKC5z3rPoTyBNInqj43qLct3AG0xfN2+yqKdr1o21JRb1cgNHOUDHP7KoNyoMfhiDG7Yyi/oAyRt0b5+tFvUFknuplfe1Rf2ApEUzJn1rUf/QjOZAW76zKA9awTqPS6p9Fg0AUoUVgMYetGggkBYiTvP6RywaBKRpTh8WH7doMJB82ARfg/NiFr0MC4N1foBTw7VoCLSCBXaA81wFi4YCyRvnA1VKt2gYkNZ5fnl+r7JFr6AteaOFzhR/DZ8Ph4pmUHUeHJIWjQhRDAeH8mgkkIKwBdFOi0YBSSuO00H25Ra9CqR5xy/T2cMsGg0ktzcEcj60aAyQZlALZOzFJ1n0GpDa4sGh84UWjQ372wgo4yqLXodW8DkPn5q7WjQOSPOuJVDBEIveAFLMVAEq2WDReCCNfhpQ8WGL3gRSv5iWZ1SxKB9I41gJqDjToglAqpAZe5tLLJoY9ksVZlv0FrRUYRpQZieLJgHJDFY4rItFk8MKeRLp+phFU6ClRYPJfOfeFk0F0lA2BCoebdE0II0XLxVvf9ui6UByL9vqvsyit4HUlu7lPrboHSDNhcZALTmRIzQDSBfsPKTkHrNoJpB8yAT7aCWLZgHJUcw2O9WyaDaQzGA+3LGeRQVAqpAZ4ODzLJoDpJWBbY27yKK5QGqLR4BDKYvmASl6mUVvvdai+UCBo3BxdItFC4BkIVGXuy1aGCKa0fkhixYByQz+krPwKYsKgYJdHtdo/7JoMZC0mEXP72vRkjBs2K/MgRYthZb6xQrTR1pUBKQKGwJ1Hm/RMiCFDT9hz5hq0XKg4KMSTgdzLXoXKPJ8/jKLioHUFofy8PsWrQDSUDYCqv2xRSuBtDiwX4e+sOg9IPWL582t31q0CkjT4Xyg/fssWg2kNZlaZb9btAZIWtyv+8Qteh9IQ8m9Jp5u0VogGd8CKL+qReuAZCF3w5E1LFofjlcm0PpaFm2Aln5LxTxk5RkWbQRSHHIPzatn0QdAWg+ZXzU716JNQOoXU8orm1n0IVAQbPji3NKiEiCNF41vV24SlYbGnwT07CUWfQSt4Joiz9mUZdHHQHIUk6hH21m0GUj7F42fdI1FnwDJeOYGIztY9CmQVlE6qqCjRZ8ByVHMlJ75q0VlNlN6+DaLPg/7dQFQ/h0WbXETLx3fXNs344F/WPQF2pIZTFF6PWDRViBNB3qj8qMWfQkkbzQFmvaERduAghQFX0uftugrWBikKHlOlecs2g4tLcscyi0vWvQ1kIaSR9Hfe1m0A0jTgd7olmfRN2hL3mCITiq3pOyEVhSi44ZZ9C2QPM8Q/ccoi3YBRSGaeMOi74A0ygzRuhMt2g0Uhej+yRbtAVK/aHyddyz6PjS+GtDNMy36AVpa2RiHe+dYtBcoisMGiyz6EUgW0vNZRRb9BCTPMzYyV1i0DyiKjazVFu0HUmywX6vWW/QzkPrFhWj/hxYdANJCRNTtE4sOhogV7t1i0SEgVUj31t5u0S9AkXu//8aiX4GkRfeeusei30L3ngTU4QeLDkMrWgEm7rfoCJBinp7P/tWi34Eizz/7h0V/AMnz/BtbrR2LjgLJvRlA7eIWHQNSHDL3npxm0fETxg9wiitY9Ce0ZPzJQHelW5SbFtzaMc8fXdminkCaRHGgm6pa1AsoMn5ddYt6A8l4WvjZyRb1SQssTOh08Er4vC9UNCJpPB1cblE/INXWkEeAuhb1B1LA87epxXMtygNS6GYgz88+16IBQHJgRaD8pywaCKRIqwKUu8GiQUAa4grIvQtqWDQYKAh4fBP3LHoZSP2qDtSxq0VDgLSHZgDtH2rRUKDAwr5O5kKLhgHJQp5EMnZY9EroW/5+tfigRcOhFSQ2+OJQ3aIRQDKeme3ahhaNBFKXmcxnZ1k0KmyLfzmq9lUWvQot9asp0O7bLRoNpPWfZ4quD1s0BkiOOh9od45FrwEFQdjPKRxp0VggBSEtrD3dotdDC3kE2D/HonHQkg95cNj9vkVvAMnzrND5wqLxYYU1gMq4nkToTWjp0yEtzD9mUT6QLGRK6VSyaAKQPJ8Ayqhl0UQgeYM5ate6Fr0FFAwlvlM3sWgSkCpkInpXa4smA2koWwCNSVk0BUjjxbvo3GstmgqkqccKC2+yaBqQKmS2eegui6YDqct1gBo8aNHbaVGK0t+p8ohF74TuZZeffdyiGahQXaaj7nreoplAclRDoPl9LJoFpMWBHwhKXrZoNlCwJvd3ssdYVAAk42lh94kWzQktrAa0dYpFc6GltujDstkWzQOSD2l8AXfeCM0HkvE8K6WvtGgBULB54ZCyzqKFQNKihWmbLVoUWpgJ1L3MokJoKbHh143OOy1aDKR7OVZY8LNFS8IKOSh1frVoKbSCQclzcsst5kVAspC7YX4Fi5YByfMnA5VVtmg5kLYh3mAPr27Ru0DRDfbAWhYVA2nCcnvteLpFK4A0lMxesstNopVAGhRqNatv0XtA0mJucOW5Fq064Q18+Ghi0WpoyRtMh/Y2t2gNkGYljX+gtUXvA8n46kDNLrVoLZBmJd2r74ARWgck99LCDldYtD60kElUr6ss2gCtYNHLc5zrLdoIJDNY4Zq/WPRBWCG7fPQmizZBK+pyt9ss+hBIXabxv5ZbHEqAZHxToOP3WlQKFOwOec7dD1j0EZC0mOfnPGLRx0DaHWj8ge4WbQ6Np3sbP2XRJ9CSe6lV51mLPg21GgHdXG61+QxaOrOzy8U5FpUBqctMDtv3sehzIC2VjMNdAyzaAhTF4Ufl1qgvgBSHnERbh1u0FUiTiDfztcZY9CVQdDO/fpxF24AUALyZbz/Roq+AtAJUAto/zaLtQDKejhoyy6KvraNWFVi0A1pyFIOt1QKLvgFSsLFfhxdbtBNI/eLZtuW7Fn17YuPA+WWFRbtCMzLj/E8HpIXP96A2rXgJppQ5Fn0PpHCqjLwxt4tFP4S1VQHKfseivdCSKxohOcxvbtGPQOovf7eZ+7RFPwEp8eY19fb1Fu0D0hDXRMKWXdWi/UD6VQyTw+1XWvQzkBzI5DDjEYsOAGmIeXXsjLboIJCM5y9V9r9n0SEgmcGUsmCvRb+E3mBbnY9b9Cu01BZznsanWPQbkCpsCJTXxKLDQNoomXplZll0BCjoF+6i/2LR70Bqi3e2Le+36A8g5fm8sz38rEVHgaI72/ZDLToGJG8wES2ebNFxIMUGzei+xKI/gWQGK+xaYlFuhaDCCkDbd1nUE0grA9tyjlnUC0htVcf2WpZuUW8gLVC8sy2uYVEfoMBRyADrWNQXSBYStT/Xon4hYkZxZSuL+gPJDOYh47IsygOK8pDscnE4AEhaTBvG3GjRwApB2PAXHWm3WDQIWhovVph/l0WDgVQhk43DD1j0MpD2GiaiW5+waAhQ4Hlcsb5o0VCgwPP9ncZ5Fg0DUlv0fO5wi14BijzfdZxFw4Eizx8qFzYjgOReWri/wKKRQLKwBVCzIotGAakt+rDWGoteDX14ElBuuXVjNLS0pLCtZp9YNAZIbTHp7brdoteAtLoy6S373qKxQJqwTF8LD1n0OpB2KHpj/1GLxgHJG1yTPdeiN0LjWwH1qGDReGgdn/6EEzsb6EBli94E0t+oPQ9oR4ZF+UD6AfmFQGWnWDQByKt8mxNrDdT6TIsmAmX2berELgLy6ln0FlDZWwVO7Fwgp5FFk4DW8G/GnQM0o6lFk4H093cvBqrdyqIpQN2++5dvYXobi6YCycJLgO5OWjQN6PtG8AYdVf8Ki6YDyVE0o7C9RW8DyYzKQB3+YtE7oefbAPW6yaIZ0NpVMtG3sNrtFs0EkoWs8OG/WzQrrJCOyr/HotnQkqNYYYf7LSoAUoXs17SHLZoDpH5xUC7rbtFcIA1KfaCjz1g0D0h/p5ptFeRYNB9IbbHLVfpYtABIXb4UaOBAixYCrVw/wYk1A6rxikWLgPTzV7Y19lWLCoHU1nlAv75u0WIghShHuSzfoiVAGuXLgHKmWLQUaOHaZ3zP3znDoqLQ80xfh82yaBm0tG4wl+szz6LlQMrlXKBm5favd4G09saA6r5rUTGQFgcmbLeusmgFkKY5c+866yxaCaTcm2YM22TRe0Ayg8a3+cSiVUAynm0122LRaiC1xVT5wDaL1gBpIWLSW+0bi94HUtJLH676zqK11oe/77FoHbRkBrV++smi9aFWFaAGP1u0AVpaYCsCrfzVoo1A2gJofHa5pfIDIBnPe9RTHYs2AWntrQA0MW7Rh0DavyoCPVDBohIgtcUKG6ZbVAqkCnkx+0Zliz4Ckhm8mH2nikUfh11mhWvK5aKboaUKawHdnGHRJ0BBejDAaVvDok+BtMOyrdJaFn0WtnWqMvb64fMyqAS/iu/l5E616HMg1ZbGtLyHRVuA1KkEUDEHK0JfAEmrMnLv/Ost2hrawJte53GLvoSWhpha29datM1qZe+16KtQqxFS5fz6Fm0H0ukgAZRxtUVfA8lC/se5upczYweQdnleOGeOtegbIAUGTwf7V1u0E0gziGeKzHIWfgukMwUz9vyTLdoFpCFm0qvdMELfASnSmPQebmvRbiB1uQVQ95st2gOkyXUKUPYDFn1fIREe5fo5Gd0s+iF0L9HuZyzaG6JLFDN3h8/3oSEtrfqLew9atB9InaqAwMi91aKfgeTA6hjHYteiA0CynEPs3GLRQSD1l78d2j7DokNAOtcwZrJ/s+iX0PK2GJGOnFkR+hVI/3Ge04GKz7XoN1QolAWkPDlCh4E6FY53Ykmg7sMsOgKUk3rawQfWvk5JkUW/oy19YPWAunNBjtAf0No4oa8TS8HtGeW8cRRa+6de7cSygdqXM/4YtOoXtHJilwN1z7ToOJB2yXZA2y+y6E+gOy9tTW9A61qLchOBNzj608qNV08g+ZDxmXuPRb0SQXzGgZ592qLeQAoAVlg2wKI+tsKC4Rb1hVZU4bRpFvUDUoUNgdKXWdQfSAFArc4fWpQHJK00oMxvLBoApLWLFg47aNHA0EIeG7setWgQtLRoMF3PSrdoMJAqrA7UI8Oil4EU2HGgu86waAhQYCEqbGDRUCBV2BCoVjOLhgGpy9TqcJFFrwBJi2b0SVo0HCgy49FrLBoBJDN4vBp3k0UjgTQoRGPusmhUiHjKG/mgRa8CyVE0fu8TFo0Gioxv8C+LxgDJeB6v7upt0WtAWgF4XhszxKKx4Xg1AkobYdHr0NL6zzPU9tctGgek5ZoVPjrZojfCCnmILptu0XhoKZFLAPWYZ9GbQOoyD9Ebl1mUDxQdogvWWjQBSP2qALT3Y4smAmmpZFtHmZJF6C0gtcVR7rTHoklAGmV2OeugRZOB1GUO5dqjFk0B0lAy79rhWjQVSBbGgXIrWTQNSGHDRG5MNYumhz5sDrSp3HR4G1r6zQaztSdrW/QOkAKA17xD6lg0A0jeoIWncpeP0EwgWcib7fnnWjQLSDfbTYDSyk2i2bBQyzLT9dotLCqAVjDKuDe+0KI5QPI8c/LEpRbNBVJOTq3nPYvmAUmLyfCUKyyaD6Txog/HtrdoASyUD08CKrnOooXQUopCbzTqaNEiIHmDZ41Vf7WoEEhnDQ7l0TstWgykoeSg7C23zi8B0qCwX2X3WbQUKOpX2kMWFQGpX/wLKYcfs2hZIspD0NbjFi0Pu8wK6z9p0bthhTzynPpPi4qBdORhl5e/aNEKIHWZxv/ay6KVQDK+JtDx/ha9BxQkbJgOQyxaBSRH8Rjy+wiLVgNpEhE9+5pFa0JE438db9H7QDK+KdD4cgvRWiBFLx1V/22L1llH3T7DovXQkqOo1WGORRtCrYZAveZZtBFaWpbZ5dqFFn0ApC4zNspvvpuAFBs8GzZYYdGHQNHZcMtqi0pCM84BylhrUSm0dMFCH07aZNFHQPIhzai/2aKPgWQGja9WZtFmIBlfAajRlxZ9AqQlhW3N2mHRp0BqixU2223RZ0CqkGbU+dGiMiCZcQrQR/st+tzG/JoDFm0JvdFGCfYL4fOtqE1XKBlMsN+y6EsgreT8jJL9sEXbwtoaAjkc/Qh9BS0NMX+Zs72+RduBVGEloGxOyQh9DaT1hD/pz1xn0Q4g9VefNlyLvgGSl/Rpo5lFO0MLrwDq7Fn0LbT6/XCyn5bnd7ZoF5ByVKbluaMs+g4oSss7lrNwN1CUlhf8YtEemKH134vjM0pFi76HVpSW59az6AdoRWl5YROL9kIrSsu3Z1n0I1CUlne82aKfgJSWXwlU+36L9gHJG5WB2jxj0X6YIR/WAMp4yaKfoaWfxCSAysZYdAAoWPFwopxh0UEgrXgVgWqvs+gQkOYCK8z4yqJfgFQhLSw+aNGvoYV1gcYcteg3aCkdYs7TspJFh4E0JZkpZdey6AiQ4pDp0Av1LPodSGbwxn5zE4v+ANJGybZyW1t0FEhtMR8elrLoGJCityFQ8bUWHQfSJGL6erjcUP4JpOlA41v+zaLcioHxyjZ5Ko9Qz4qBo/jjlrJyc7kXtNQWvylU6WFRbyDtazT+hRyL+gDJeKavM/Is6gukxIbG9xhuUT8gGV8BaMZYi/oDyVH0fNFki/KA5HmmlLUKLBoApLAhylhq0cAQ8ScxVdZYNAhIuyH/wu9dH1s0GCjy4ZhtFr0c+pABkPaNRUOgpQDgoBRwuY7QUCBVSPfuPWzRMKDAvdgaYha9AiT3cv0fn7BoOJB8yEV+SWWLRoQWcq/ZX8WikdCSN3gr262GRaOAtCyzwtanWfRqWCFznodPt2g0tGR8A6CJdS0aAxT9N8SWN7ToNSA5iplSraYWjQXSKNMb2S0teh1I3qCFt19k0bjQQv6CZcglFr0BrWAFwDegpEXjgRRsbCuvnUVvAqkt5t7Dr7EoH0i5d0OgxA0WTQDSJEoDmniTRROBNF70/PK/WvQWkDzP/KrPnRZNAlJ+Rc8/e49Fk4HkeRpf0MWiKUAyngGw9iGLpgIpAIiuLrf5TgsRB2VVuRVgOlA0KPufs+htILXFQUl/yaJ3wkGpAeT1tGgGtLRxsMKC/hbNBFKFFYDWDrZoFpDGixZ2HG7RbCBZyAB4YbRFBUBRAPw0zqI5QKqQR5tnJlo0F0gTtgVQrWkWzQPSoHCU02daNB9Io8wKG8yxaAGQKmSXVy60aCGQusxcbku55WtRxcSJXG7TMosKQ/f6P4kZGz5fgtr0k5imzOVyLFoKpCSft+jO9RYVAcmB5xPts2gZkAKeP6UuvtGi5UBBzODufYpF7wKpQqZ5nY9YVAwkVzBhy29i0YqwU/wvtHRsa9FKaGly8b/QktnZoveAgrnQ19k+wKJVQPKtC5Q/16LVQMEOhYvZzy1aA6Rx5LV8/u8WvQ+kmGGK0r2mRWuB1GUmNpmNLVoHJEcR1fYsWh+idKCMThZtAAp2KFxgdrNoI1DgQ6SUfS36IPThSUDOEIs2QUsLbxWg29+y6EMgVRgDKllsUQlQ4F6YscmiUiC5Nw2o+w6LPgKSD2nh9gMWfRxaeD5Q4R8WbYaWgo3Jxqp0iz4BUlvcrw/VsOhTIBnfEKhlXYs+A1LYMA/peq5FZUCykFdb+S0s+hxIc5z//ZO8Sy3aAqSfsNLC4nYWfQEkC9lWfgeLtgKpLWYUZeVG+cvQG8xerrzDom3QUmzwhi2bi3yEvgLSrlEBaONjFm0HUohSq/hZi74Gkha73KGXRTuA1GWa0XqwRd8AyQx2uSVX1wjtBFKX2a/OEyz6NuxXQ6CNky3aBS0NCrVqz7bou1CL//8p9NU7QruhpU+HzK+Kiy3aI/Sgo4vZ7Rss+h5IF7PUKtps0Q9A0uJ/svjwVxbtBdKvkdnl3Xss+hFIXWa/ah+y6Ccg9Yv5cJdjFu0D0mrDrWGSa9F+IJnBjbJdJYt+BtJGyfV/fnWLDoSOYn71/ckWHYRWMM2RRJ1m0SEgtcWMYlsdi34BCqY5br3qW/QrkLrMLS9RbhL9BqTA5qZ84DyLDgNpPWS/6jKXi9ARoKhfwy626PewXzR+TRuL/oCWjKdWF8+io6EWE5ux2RYdg1YQvbhuutqi40DqFytseb1Ff9oKH7jBotz0oEL+vn3nzRb1BNLiwIu+zrdZ1AtIx16mlF3vtqg3kBY9Jodb77OoD5AmLI2f2NWivkAynoMy/DGL+gFpUBoC5TxhUX8ghSgTm8QzFuUBaQWgN35/3qIB6YE3qgO1fNGigVarQS+LBoVajI07+1g0GFqKDUZvvwEWvQyk6GXYbB1i0RAghQ29ceMoi4YCyRsar7EWDQPSeFFr55sWvQIkLRp/dJJFw0PjqwK1mmrRCGih9LVavWPRyFCLbXWdadEoaKmtWkC5cy16FSj4iUWec/NCi0YDyVFsK2epRWPCtuio+csseg1achQrbLTKorFAqpAh2nydRa8DKUTpw4tLLBoHJB+ywvmfWPQGkCqkhdu3WDQ+tJBp+alfWvQmtKK0PG+HRflA2r/ow4zdFk0Akg/PBzrjR4smAmkSXQU05IBFbwH1++EmP+bzfrNoEpCitzrQC0ctmhyiWHyAU+hYNAVIS2ULoE6uRVOBpMVf5myqYNE0IM3Ks4C2VrRoOlBwgz3AuaSyRW+f8OEA5+aTLHoHWoEPBzgnVbNoBpB8WAXowwyLZgIFq+gAZ3xNi2aFbVXXmaIwfD4bKuqUzhRJiwqAojNF7nkWzQFSYPBMkbvJorlAGiz+YD7Xs2gekIaYv7Jwhlk0PzSPJ5HO8yxaAC3FJ38tk0+3R2ghkJZQHjdKmlq0KKyQ/3nk3HL9KoSWZhDPFBlcQiO0GEgW8nRQ8ppFS4Dkdv0KaI1FS4HkjbpA3fdZVAQUjGM/J7eqRcuAVCEz9vxzLFoOpHFsAVT7MoveBdJ4MS3ffYNFxUAKQlZ4+90WrQBShczzS3pYtBJIXWYy33KARe+FPnSB0odbtApaynkygHInW7QaSINCrTGFFq0Bkhbb6rzRovfDtnj40g8OI7QWWnIvTz1K2CK0DkgbCk9Y2X9atB5IJyxmgLori9AGIGWAzIe3n2rRRiAFGy8Vt55t0QdAqpCHlGYtLNoEJPcy6e1abrw+DPvFS8XNnkUl0JLxTF9zr7WoFEgV8lKxqJNFHwFpgWL62uweiz4G0lDykFLlIYs2Ayk2aOGV5WL+k9BC/uSg4GmLPoVWEL39nZG5Fn0GFERvf2fSIIvKgGR8Amj7qxZ9DhQMZX+nzQSLtgDJG2xr5zsWfQGkttjlQwst2goUdXlzsUVfAqnL1Jqx1qJtQNJiAGz9yKKvgBQARGVfWrQ9RIyNzbst+hooio0Gv1q0A0je4B5a4Fj0Teh57lC74hbthJamObW2cEOJ0LehlguUUdmiXdDS/GK2+VM1i74D0qxsCDS/pkW7gZQ3El1X26I9IeJ+vb2ORd8DyYe82jp8tkU/pCdOXG3tLbey7Q2MT69UKRa74fFuPZ66/8kn7+3h3HL/Yw+1e/zRLo4Tf+kfGW3cWg9MXzo+XtSwWvotNy257ZMqA/98vM9jadiTKjlOM6e581IsNxbrGXN6xZzeMadPzOkbc/rFnP4xJy/mDIg5o2PO5JgTXx9zNsTiH8ScTTHHSVy9No4RmjHOG7byopR777clEi6++3fvniVNUm7182ukFh08S/+890t6yuXT8fu+E2194TxPD4b/+ILnv9HYkwrGPck6lvWomVSlJ312ThFbYelmX3S3BFCUb2W5y3qMSH6wsUNyyuursj6q/0Uynnfq58mfXkh57t+6tPUOHduczDv1NZWu2/R9Cc/v2eXtKqnvuT1HuamP6v/NQ1indk5+3Yv/2vguLxJu/EeJF98/NZGi4H58Qzz12Hd/eC2/3Om9esVJKXfDhNXe0yNqp1g9S7VHgQYcvsVNxWnSlk2npWTjgw80StHof9VqnlIv6Ct2i6XzRcyJPdX15NRl89NTcQoYXCe+q2SHh9IX0Dkv/ufmZ33h6oVPwZRJnjtkdDfvzBZ/9+5vew9d5sFlHb2KPUclb6t2Phx7LMt9IlkZrjqnaOxrryVZujV7Z0uAfW0fr9H7Mhh6ZRatduGdrHd+/iiJipIs3Y4vj5aw7pnlyXMvSPfcdp2/TY7fd6437419ADd7LnyS3Hokx7uuStwbv2+qF4dzvKgbFJyDCJOKPU9JxZv291wKNXvnYiBqpeY82dWLP7enkUcBg9/Ka5XezJt9zb2e2rpsfp7Xp3h9slX6DJUubJTw1lW/esikvPjSa6ukEAVJ9/Eabqrjy6cld/+xBw59Oss98NEHHmxd+nvO2yrd9l8/L2HiVRezzHKP3PJNsmGiZ5JdhI89F1cfSbgGAXBmkk3rQdlbBfKmYuD+tl/hwX5vdWZLb9Y1sZRb9lZ3D+5NjVuVrxImr5XAMNcbJ33mpr5v9LN35aAqKQaz+v99ozmo1HeIE6Py7Gu+9tzrqvSSkHfqHd5pn2/y3PwzWnjDfyzyOn3T0Lu5cLwXf6HWTxJc9jP34SJ4cbP/aswplm5YmZOLiGJ4Kb5PT7v1hIDOOnH4lL32BY6SUCTsKpnoxbMvGu8L+Wfs89E5X5yuIY3THQc+etmLn9niLD9W+Q5KJx426tzixFasj2GCfeHFMUh4GAh4y4mfMuuaE4IQRqVtJKgVqkcCkdPXdWJ/H7pSPnb/yPleQssv01JtBsDZjxTVSh2b/rPXfO5ZqX4/lHru/qmNUtXPn+ux1JBSWPeMpzcYuVJp8MVzSdZxZosvs1QpDFjKVli6r14xSALdsrhu36T8s3/q1cvlukiQnVlf/cV3FIWHXvnRc+e98bhXs7eTOu3zIailQsp98IEpMpr1s3Rif+uy1Ptg48ake0vhNgk5qV8YHp67rUEiNeX1Kz2WMp9C/hmzPb6xf+oaTyrZF23zWAdL96w7J0no98MrWA4+9lzOzQ6XrPM+ffYhzIwFnlvy2y3exzdM9FiqUgpckPjG+9df4qvc+I8M1TFs5S9JVUrD2ApLZw9mNFcf+sVdsPYZCXdc2prlf3dmcRLwDc0KqnBmsQ7NG1ZK97AVli6jnMLqzP3+G/QzVfr9cLI/s/7W5UzNLJZqhQKb5Ruygyo0jHXIUlYKYSlbYen3il10G1+wVgLmvD8YXHQ4GCxVPQUOBt/QYFCFvmcdLN02A3pKOHSsk/8GV06qsFQdFFgp31ArVGGzrIOl808nFoUXIy8SOIbxR4oOnRCELr77DPQ+ENBrRwilL9yXcbu/CkQCiL8KoPQFqjsjMMSrMwsQEYsYVTsl4I3UP5e/6WF5rZo694I8rAFVseFf5j+o2bs4OfzHo1iYsvxgxqq89Lk9U1D2XqKaoX+iCbUeCVs2LfLiYYNO7KcX6vndwFZ+QoC6A6HeCYHIic2+ZoJ38d1Lku6k7su8vTcVJpdeuxHxvCfpctXY/peqWmFgl6cHf+Q8TOeiO7mei60Nm8pArJoTsLEP8p0ybU0gYJFNxolYvTMATvn35tMJ8YaJYL3KvqjuCQGmOvHch4+iuUC449L5Xnz6muv9J6wQpS9Qy4nNe2O2d3wzcoptDfpJGHh5Z4xz/ZT7YuoibpIpThQsZEwy+khwsTZ5oKmsr4Z6WV+1SLml9V/3fs85TwsGS5dLDgXujXpj7GufS2VmnS2qA3lGnMsjX42n2AxXdbXLrZeGhJY5MWcK1uKtR5Zz50+5f/3mWwlcqVtfeNBzP3srI8W0hS8vrrvac2dfc3YK3yTg57M5Cf0HrS+8EJFYO1Wv7w9JzMZqqXad70/SRfB7lsaKiQo3YZYuMw4Ki+tWwByamnSR+SSZN7A8On2Opwe0g4v7zDpnpNwpmF14I1U1/o53zheXpuJMPAY/ek0qPva101N9is9Lxbl3bj1SN+VWuHUI3jkd1jyC1btqyuWocrFBBd7paZ96rlf5Nrh/r7dhQl9E7G+ey9nA9kJH4PCB/YbBgp3+EwlV4z9w5fKQdzmpvTe10X7ANVYPuMHyDUQtk4NPMKU+gMFzVcoiCkw09Aa7QxWWqoMCV22+oVa407BZ1sHSuf9/Scy2QETgf4dvecuLo0xFQv2CJyU4kUDk8OzA+denuHLK5eBQ2P6XDzzt5EgvPaZN/IMWd2SVWtxZauWkwMWdb6BO7rT7kO++mGQdSIuyVCkS2SK2wlJzhwJShKSSxGPTn0j+c3k9pLsjstiROFycRGrguZVm1IF7liSZNbJ0B16eL+HPzYUcZ89lfsHVmxPjwQce8OKsPRK4YsSfHlEmASl5iffTC+9qBCkrajg5WD1LtUcBbyB8V3pxmsQ55T70ymPJyd2PKeGe8nqavwLQV+yWyiJ4kr5irMCsk3HgelF7HgLcf4DmkiNrp6Gsl3SvHLRXYzjxqoUqNYQofYFjGed8o+DSJApbjxxHtB2nNVVTrIAlMnH/AdOK8fsqppjaaOXwKo/W6NEiJ8Zc6P62b3pxDggF9HarBJZtBqzwH7AtvsFS+VMk8FV2cY5GQEsNBZYr1p/t6cGGCVd5XM44QV2GALMZHjJZqpZIoG48rM2Zg1Xs3m+Hce6kpEjhtmo/YlL9xnwxkXr3zS3YvRNYg/P98yB85cQ5jih9AWtvEgvwaqZLWVxvsTxnYXl9TVkFYk2ly8lPAU0gRDtl4UFhEoGcpH6Tu4YmmQ8kserxiJVkTOgBo4p7a4dLBng4kZ3mYTxhUT+VcdocCUyv1Qu87z323VKs8x96mgnMUlgBS+znXSRw4bgvo0yzSZGJaEakrPLi8FayfsEBz8UoJznH6OlO3yCBZaqIDmBa+k5zYkwEGDuaJBTOLpiLgfgsqTMyZwstqhpv6tvI3RenxEES2EEJLNmGHuDcmuw16go4Ccd2nmxZa9iMDstN+5+KdQCxelu1mTyPLdN2h31hOTMoXCYsdznWuNlbjtO9Shd7SpICzldMR5fjMHt/Fnag5RitIqSky+OcIBitZS6733zuG8s6fTOZC+ky7n/I9dcsQwYO169ZJjMpTOqepNeXKY3GvraMo4WWiuJM/ZkBxZH+XCZbqSyBdwYVbq3m4Z19/pOwPzq3PfbdmX6QUXgxtcCL75z8BSLn/qT2Kgm49kO0AVHAu/70RekLrEu3GBLOu+tDJDwdvPi7bwbZ4dMj2vjCn5vbnGgLpeOUwoL3rz8NB66/efGBlzsnBEAHG8YKvz0KQm//PNwXeCYHcbB61cxCyZ0jC4OPcyK2jyy1d3NhXd8mni64k6meSBBiW5EghKj3hbxTL/URGvQF3MJh30ATtxTe7VvIvqF04mEvnMudGBNTzCw/q4sEvOX7C6UvqE6+HAl82RkRc2INvjiS5PR10RkJGNgk/snCKf8c3qjoH9wfJfVg8KPf6RjEuYXLjblJ3jmxDpYufUWBa4Le4L0NVViqDgqsh2+oFUQnxv7bpayDJe44ndgTyetSnNe45rlOF0yHjrVO8cbCdZzMFOKvLbY76LtFcQo6rzKkIgE6RUjfb8A7vS+7euFB7oa8+DktOaZbDZzQTovWHfhmrcc3mAFJhas+6/g9p3JKlS06mEypnUggkil8l7ZJmcbyuMSS1eMG+7NztD7RdR7nPucTGuWO0kurCevHlVVSDfF4IcLVma9y9ZUuM1SWrMyJ0QQqYQ1smuKJgCWRHvC8se6Zun6CystH1kYVVi/zqcurzxhTT/rMxUz1cCW2jCkrVrEsl1GDiyitTUuvHezpwZhuH3l8g72WCq65UqxDjuFOScHFvFd+zeyNVxBaRThbWaoyChwIvoFBz5IK7MY6M0UGxVkrBac/wqHXqPew7V3I29cDErhE1+vbNMUzN+7KzsGJugHi45SUy5tIrI0plrwS1QOuL3yDk00qPIGwDuQhdNEBXDfdiPF5T2UcS9EJAVPIiWPj8ScTBRoQ58sU8B0dmxK3KnhorwQ/t57rISmpgpuOVbomky0UmO7wDZ4tpeJVPg05X7FKtRgJrIwtSnBGwxH4g988D3oUdEdI4eTbT0cwnsskswWun+vgrNAWV5FIdxpf0B7Cbo8lcwc9eOy7f2EnbYt76SZMMlvQ4CTrwK7jXzyitw5bQeF3G6UvsDknds+SdcgDv066zIkoDH60YorLPwMDucb5ikHuycg3GoEMwXg28i2gwB2ab2Dq+SoMKtbR5K7qKVVKP7AVlvJHJLC5OBEFHgX4kOuiKJMv5ETVcX0WCJwaQpGAvvidQekL7FW8/ddnwMJAiCqkoOmCJTKLQYmbyum4bylYhthN4kYsy+XNEtYc3TApwikEFzG49sAcoAonBevgLMFtY4Y/XbiHuU3PRQr6KK+IU3BPP05G+KufXxkFLqd8AwtallTwxjLWQYO0E1FwZsFMHhO5CrtMPSicfPul0tfq+5+WY+QGSZ6BcDROury4xLBnUQUTqyiO7APL0RnL45fNb5+UgBwbG4Tb1oU3k8gysng8ZY6IcG6mzxYsVT0FtsdUUAbATpkWmuh8C3vxED5e5LkMUwrIrpDWY0qtzszQjQxLHuz0gKM1bpWTwjacdJlPw5ilzAbhmv/ejUzYIANnsjJmXFGtwK5VmOQdMFdUJGhfYZOvqowSLemeXDkdM0oeU7BQr8RZZSCGeDK8O8jfe8vfsRCxel4b8yuKzKAAAx0txSh9gRuyFCMBxO8FSl+I1Ck4i+G6T56tpD/EKeA1BwvnR74mBa6zbr2+7/ADDbLnXJXc9STgkijJxJdLYvKUWf0REmcmz/liMTMz3mmiWizwvu8oLDq4zHMpIDXU2YRHIqXdTyQHIvUbohLr6VwJNICnBVmiSmgkBWcb1rL8M6pjTJd4SLAzdQrucEkTbFVjWUErLJN9cGHQKoWg9h/A7Rj1TJQdky7XCPTR4RELhYN8cb3vJgqawWyUAiK6ouY0/cZphjS4Saq0/pnYOJtgj0TyT4EfIf4+tD6Ol5/z88+ZIKtxq+mb6MT4LYdpvnJ9/oMz4o8S1j1z3ONVk9rhKs8SlvgNc8XnG2//XCMlFRxfkdh9oDLOWiOBT50Y92rMv6SumiiwRJ/9uyd+huAbLKUUCXwVibCv7DzLA2r1FD+xILm8A2ccCNxwYZhJZHEQkI/imKO+MOX1K2m8g48TZ/tCWA9vkdOwtfb3kPGv0VGcZwBPT0KkRL/53KSvilMEdtgfvTiPz4iZpNqRcOSW+zCZvvck4F1/4FD6gmxhCiCBh1bds6xYf7mGEhnsXb4wufvf/bbCRrkbRDXQMZHAk5Puj/UE69sJAWpMsINpSQFnNQ83pPf+v9jmLMYBG/5UIsIpIcGrfIm2dnyTvQYR/TN2g44YuxIPh+Y78GAOBvsOTABc9FDAaU1voGJPKsyrWQf2jCxVCmEpW2GpezUK+KpAH2bpHIrx4lEO02tt0uVHNBjv8SDGE4JWkUhAM/4qgtIXOBWFIoFXmfGZdcZKwMTMRzS/j+x4njfw8u008n3sJUc83pzzYzDyfUc9p5EsfafQSy5unCRwr8cen1SWgXnrPVLUOsXvlS4vLrn8sJRTKDDRHTK6Oeb9Gs/tNeocvLGNqaJKl96lwNsavdGs/6fKjVmqDgpMtPnGjudr8dtDTHawDpbOh1h72D360sVXMgk0CGUWTs/f6nyDhYuO8tdGjhRLtUDBH8vXsI9icKnCbza8w6KBLr9n0xtshSUO7R9K4CLDvQ3fyxJ0Clbhajg+bOYtUnBZyl3m1Ss2aDXY8fxmrkX+RzB+cmCJ5Ky3BF4KyQdI++QDlrKQAn3AEZUPeKTgusA6WKpSCmwFm4+/2zHdpx1Ybb04LRPB8R6T+DGmOVTJcpnIo8tL2TuWTgnmQoVbN2kDQNvfaUlAPoD7tMf4/TeOXPcG9Rvrt/8AHyUxjw56uh7afMMOqFTEReAqxjNDfwqTqSKMg0rY304CbGHot9VvDyBkQSULg5OMM4dhcOlmh3Zzj+P1hnvPkmux8xUnmfGyxFXPeAnI3xDm3yVR+0rmXVj+V6L/TekubuzXeUwTeNuA4ZqiznywcbBKfBducUJYsX6Chw++/SXgPPumBF7hIOfz8I1+CVw612PJywA9YETwKz5vU11OOGSyeON5lS5ueiQwh+IQIWFprQ8ILqc370cZBCxdLuEUmL/oJguuVRRyAnANwgXRUc1elk8k1/sPkMMhqdqPu90pHg6Y3yCaRmMA/UHUmYurC5c6pGyHJCB/889cuMLDan4OdwX/zAUnYPWviO8qLbQD6gHPXHxDZy6q8MzFOug3VcojFlthyau5EwImnoN8cy1noC/QgDhfpuB/LaejXc4oCvwhC52MPbYqcodVGJ2qvi0UeObiGzpzUYUnCdbBUi1GAivDfuVXz++U7DIDKU4BaeuyOGuMBBjoL6YofSFaTCNB6pw/kSC0YK1/OyZBiE1Q4HcHbD7J936Z5sX5L/4jYfCjD2pfQlYdCHzK9JIZjXrBO/lIgFEODjPPnxCEcKrJigS6NE71SCBi3/l1CHpMAoZ62M1NDoHZ1xalg93iTgTgQB2r/MswDLbfwae6vuLn21zI9OSDjVX8LvMYqFSZKRaqceAovy1nGlKId35u5VvDgMXK6bm8FeQk596GeZmllRsPinjby5LrigROGqxLbeOYCfzQndSkQUzwrKLSfaSohwQsP7wX8PD5bCSmxMUe8x/fHzjpKNaQfCzyPvxthBfnTYqehMY5e2Dp4EfP1ZAhtTmXS/oyJbmRgP44/DykrkkQwmKWFQlSx8xA37Be/avWrCQtR/6axDzy0NEj2F6643TjQnmeB59U9r3Z+sKaoTdrnjCCghPbsumfSF9ne+7VC59C47n6NRJ6p4+JSLUr4dtNyg/IBx/YdULQ6YNxIiGshcGw9cjdfmsU0BM/2lH6ghCjPRJUJ3e3SBDi3VkkCG094p0QhNgEBWcbXMyf2fEPmAB4gv8hQytDnwJBA3jjP4b5wveNLvAR7hV947iYcmz54TXJZZ0BwgjAQpiVZGSzZC/1gFsrUqykPoQyYtj0judHqkRlvi28q38VNX3h4ariAgksuejrAb9WUYmlrIwEvooN0ld2FqF/YWfVa1js+xOlLwjRn5EgX9GfkSBEf0aCEP0ZCUJsgoITQ0LtMwrYbJbFHynqdEJA606c1xMofSF6mQID4qcXXtI8wELzkv8aN1+UviDEqIsEVM6vkYdPCEK8rYoEIV56R4IQm6CgAwY3L+5XCPcHTwho198rUPoCFYQigad7NFzoC/PeqK5KoxmNBfgHjx9DcRt7kd8lvoPS4a2H2vJ7Xi/FlQVm1fMpm0PpC6qKZ8hI4A2StpdIeKprqYevbBmpSODRVygSoiYo8Lr/7IIaWpvVFwl0K5r1PY7SF9Quj5wSQi1nJhI1Xrth8y5y+Q2XAq80sAolXS7tSFiQ7BZjU2ns4TtemyTDliXXaz1AxqTsiUEkFf7MlXXwZ6+qlD/9ZCssXV7oUZjU/Szc5eGNZT0uxOHqLKQH2SlOJtyJdsDV33dI2DvAXLRCgb+f9d+AHVShYaxDlrJSms5WWPq9wjxgYvuHBOSyPJB4LtbxFJM6XDb5l5H4A44hQ5np+e1RwE283sCa7Kvw5pR14IItpUqxh6hXLLEszJfQ8eXRiCS8gX0HC2w6cvlOWKNRBxcOVspSrVBgs3xDdlAFTeNrwGh/h2OlNJ2tsNT9DtY0WIlBpIDh5cbr381KwELi8fetaLwrcoOpKnEwvEoCcvAkdzb+nCLJ+GXJzIvbHIYU1SLOYV8g/K3LH56+xPMXG8gtdJOqy8eHXpmpIwZLJNDbJNAAhIInS1QJjaSgMxa7z9HBITFfAkNa48ew4YBC2Y80rrsccpbyFgUGBRIxvIEogd6/j7SbXv5MJe46Y4q0D3+rqct/HFwa4eN9o9TZBa1S/MEy8oGzUvxxNI4ElXQbxOsSlhiLBRKeHjHGH0/+UI/jydWWDpNX2BRLWUiBP0rhGxpPqnA8WYfGk5ViaHBnukWlmqVAO7j5YmK2wpe8L3jGaoS7ng7JqxfWRGDjaM9DK10W+pCLDmafnIv1q40fAZrU+J8EHEf9VSMSQPz1CKUvSJ0oEvhFR+qRIMQmKCjFwZ7pr8YU4Cb+NLTZCQGVO8pJUPqCENNBCQj0LKmj0iQF/S4S6wgnqX9DyQMrVz2MG64HEJ/cr9k+FsaWMkQClWUEBSfG9HLI6J88/TiIaTBPGNrYmb+gbbi9E+IYSrw2jgT2Ms6UTkJYizMWMXtdlfp+cxTQHSfOoxRKX+DleByRi1sRCDzZSMAJ1O887ul8Z/PAyzwLh3XX43UAJneSxzyXt+jcgFjy7loPeHBj3sff7bpIPJK8+GCIvHoFzlv89k+jmKKwlHEU/J+PsBcuL8kpMBR5LHFxreXx1zEseb7TA0w45Db3+WkK5zVNGjJ6lkr1MBJYGZIpv3rn77wpv9+PHw6WBH70ZkbDa8CkhPAd5jQ5/ksL1i70BZ7O9FLPUb19IXyHV3bI4tUlpNuX+P7mtonSibNL/NGGBDzwP9eg9AVp0d8SJl51Kq5dHmQEnXqiQgqcQrwJPz3tVRmKrfgSX0BNflUofYHvC0UCnYSP+yerqqjOONdq3obC2ra+tfwhHkoHqYTflsO7bjqRF8k4rjfEZ9EC1NdcM9ZlVfAfrcGJtJ3/gHt4u84Nkam1T3LpU428JkHhW4jSFyIzKLjT1pwkw5hJ8eINp4nmaPgMRFdznYL1gOGOUcRgb9E3ohQP1qGJGmlklriVpsDbHsR1iQQcJg5KQFinmL9is66CvXI3du8qKf6uQQ/4D99g4iEV1sY6WKpWCk6MGwkeJ7FUnkKOj0inpHhNqwdI03XPxVJKkcBX40QUeFaOHEEaCXCR7yOUvkDEPCqU9VokaIGgGyIBer4iSl/gy4wkfrlkVhanAOhTlL7A13CKOkODIUGJH1uJBCV+vF2PBC6BQpEQNUGBp/xj05/QSHHOtY0EtUKfRoIQf9keCTCNH5qeOCEQOT1j/CFlM+RzeZ5+2MJP0gioMb6A6MBSBcQnSFWSekdCqOXoF4+UuMf4alxBJLBlIT6RGt+REGrJBKQqWXrKD8d8im3jlyxVwWxBAn9bI8QnepnvUHBiPOVSCn5ug5f4RGo8M0vgvVeA5vkv8x0Kzi6YwNnA2wosjAkkDItwfqvlf4XkZ1susCz596H0gPcHTLGZSunvXMGtDq/1UPiHTpS+gD2ew/kf/yIIz4eRwAZxl+Kb4MRw04psqCiJrfE7CegqFvYfk5ivMeSv1XHHGMOXJY8TOIa7uUG4nNyI/QJXKqwuEqiLaxe/Nsf5fwA=(/figma)--><span style=\"white-space:pre-wrap;\">Электронный билет – эквивалент бумажного билета. Это файл (в формате PDF), который может быть получен ВАМИ на электронную почту после оплаты заказа. Распечатать электронный билет можно на любом принтере, цветном или черно-белом. Так же электронный билет можно сохранить в смартфоне.\r\n<br><br>У каждого электронного билета есть свой уникальный штрих-код, который отличает его от всех остальных. Этот штрих-код является защитой от несанкционированного воспроизведения Вашего билета. Штрих-код Вашего электронного билета будет отсканирован при входе на мероприятие, после этого Вы получаете право на проход в зрительный зал.\r\n<br><br>Во избежание несанкционированного копирования билета КАТЕГОРИЧЕСКИ НЕ РЕКОМЕНДУЕТСЯ выкладывать в интернет его фото и изображения. \r\n<br><br>Сервис \"DNK-BILET\" не несет ответственности за копирование штрих-кодов билетов, выложенных в сеть Интернет.\r\n<br>На сайте «DNK-BILET» можно приобрести электронный билет на любое представленное мероприятие.\r\n<br>Входной электронный билет действует наравне с физическим билетом и не нуждается в обмене – его можно предъявлять при входе на мероприятие как обычный билет приобретенный в кассе.\r\n<br></span></p>', '2020-09-02 09:21:09', '2020-09-02 09:21:09', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `fee`
--

CREATE TABLE `fee` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `fee`
--

INSERT INTO `fee` (`id`, `type`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Выдача', 'extradition', '2020-10-14 12:12:18', '2020-10-14 12:12:19'),
(2, 'Ожидание', 'waiting', '2020-10-14 12:12:20', '2020-10-14 12:12:19'),
(3, 'Оплачен', 'paid', '2020-10-14 12:12:20', '2020-10-14 12:12:21'),
(4, 'Отменен', 'canceled', '2020-10-14 12:12:22', '2020-10-14 12:12:22'),
(5, 'Безнал', 'cash', '2020-10-14 12:12:23', '2020-10-14 12:12:23');

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE `groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Альянс-Шатро', '2020-10-02 22:24:50', '2020-10-02 22:24:51');

-- --------------------------------------------------------

--
-- Структура таблицы `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(95, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-26 15:32:45\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-26 15:32:45.962498\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601123565, 1601123505),
(96, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-26 15:43:22\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-26 15:43:22.378528\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601124202, 1601124142),
(97, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"8\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-26 20:08:23\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-26 20:08:23.543223\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601140103, 1601138303),
(98, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"8\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-27 00:58:41\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-27 00:58:41.714214\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601157521, 1601155721),
(99, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-27 01:14:53\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-27 01:14:53.845312\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601158493, 1601156693),
(100, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"8\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-27 01:15:01\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-27 01:15:01.350274\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601158501, 1601156701),
(101, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"15\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-27 01:15:05\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-27 01:15:05.752415\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601158505, 1601156705),
(102, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-27 01:15:22\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-27 01:15:22.150677\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601158522, 1601156722),
(103, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"11\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-27 01:15:37\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-27 01:15:37.254642\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601158537, 1601156737),
(104, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"12\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-27 01:16:31\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-27 01:16:31.282011\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601158591, 1601156791),
(105, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"11\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-27 01:16:52\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-27 01:16:52.268925\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601158612, 1601156812),
(106, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"12\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-27 01:17:25\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-27 01:17:25.584669\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601158645, 1601156845),
(107, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"8\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-27 01:17:49\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-27 01:17:49.396745\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601158669, 1601156869),
(108, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"11\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-27 01:18:11\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-27 01:18:11.622493\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601158691, 1601156891),
(109, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"11\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-27 01:18:32\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-27 01:18:32.623405\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601158712, 1601156912),
(110, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"15\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-27 01:30:08\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-27 01:30:08.096456\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601159408, 1601157608),
(111, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-27 02:08:30\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-27 02:08:30.287510\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601161710, 1601159910),
(112, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-27 02:10:23\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-27 02:10:23.449589\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601161823, 1601160023),
(113, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"15\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-28 18:31:33\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-28 18:31:33.644162\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601307093, 1601305293),
(114, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"8\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-28 19:04:41\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-28 19:04:41.180565\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601309081, 1601307281),
(115, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"11\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-28 19:39:06\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-28 19:39:06.342742\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601311146, 1601309346),
(116, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"15\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-28 19:40:15\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-28 19:40:15.436166\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601311215, 1601309415),
(117, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-28 20:17:23\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-28 20:17:23.379991\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601313443, 1601311643),
(118, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"8\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-28 20:48:50\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-28 20:48:50.926150\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601315330, 1601313530),
(119, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"8\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-28 21:26:31\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-28 21:26:31.969095\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601317591, 1601315791),
(120, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-28 21:40:38\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-28 21:40:38.334844\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601318438, 1601316638),
(121, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"15\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-29 01:04:29\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-29 01:04:29.542474\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601330669, 1601328869),
(122, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-29 00:37:46\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-29 00:37:46.346296\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601329066, 1601329006),
(123, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-29 00:38:02\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-29 00:38:02.853163\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601329082, 1601329022),
(124, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-29 00:41:09\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-29 00:41:09.060499\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601329269, 1601329209),
(125, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-29 00:42:35\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-29 00:42:35.989709\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601329355, 1601329296),
(126, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"15\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-09-29 01:25:37\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-29 01:25:37.126902\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601331937, 1601330137),
(127, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-02 18:06:19\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-02 18:06:19.805001\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601651179, 1601651119),
(128, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-02 18:22:33\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-02 18:22:33.306552\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601652153, 1601651253),
(129, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"8\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-02 18:49:03\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-02 18:49:03.966255\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601653743, 1601652843),
(130, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"8\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-02 18:53:46\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-02 18:53:46.566761\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601654026, 1601653126),
(131, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"15\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-02 19:36:18\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-02 19:36:18.268218\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601656578, 1601655678),
(132, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-02 19:51:54\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-02 19:51:54.459981\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601657514, 1601656614),
(133, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-03 01:19:47\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-03 01:19:47.235281\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601677187, 1601676287),
(134, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-03 01:54:32\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-03 01:54:32.975320\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601679272, 1601678372),
(135, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"15\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-03 02:34:32\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-03 02:34:32.107288\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601681672, 1601680772),
(136, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"12\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 16:19:33\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 16:19:33.662252\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601817573, 1601816673),
(137, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"11\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 17:11:43\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 17:11:43.826100\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601820703, 1601819803),
(138, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 17:26:36\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 17:26:36.672827\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601821596, 1601820696),
(139, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 17:27:23\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 17:27:23.220261\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601821643, 1601820743),
(140, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 17:28:25\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 17:28:25.940816\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601821705, 1601820805),
(141, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 17:29:00\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 17:29:00.542265\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601821740, 1601820840),
(142, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 17:32:12\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 17:32:12.506762\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601821932, 1601821032),
(143, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 17:33:18\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 17:33:18.860271\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601821998, 1601821098),
(144, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 17:34:37\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 17:34:37.672556\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601822077, 1601821177),
(145, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 17:37:21\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 17:37:21.267528\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601822241, 1601821341),
(146, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 17:38:06\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 17:38:06.365993\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601822286, 1601821386),
(147, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 17:57:43\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 17:57:43.983417\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601823463, 1601822564),
(148, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"15\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 17:58:29\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 17:58:29.143138\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601823509, 1601822609),
(149, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 18:09:13\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 18:09:13.676929\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601824153, 1601823253);
INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(150, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 18:10:19\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 18:10:19.705221\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601824219, 1601823319),
(151, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 18:10:42\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 18:10:42.671035\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601824242, 1601823342),
(152, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 18:11:51\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 18:11:51.369143\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601824311, 1601823411),
(153, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 18:12:24\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 18:12:24.816317\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601824344, 1601823444),
(154, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 18:27:39\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 18:27:39.892629\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601825259, 1601824359),
(155, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-04 18:51:00\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-04 18:51:00.566592\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601826660, 1601825760),
(156, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-05 15:19:16\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-05 15:19:16.868902\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601900356, 1601899457),
(157, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-05 15:31:06\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-05 15:31:06.408576\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601901066, 1601900166),
(158, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"15\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-05 15:36:19\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-05 15:36:19.795464\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601901379, 1601900479),
(159, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"8\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-05 15:36:45\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-05 15:36:45.839639\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601901405, 1601900505),
(160, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"12\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-05 16:05:19\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-05 16:05:19.599520\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601903119, 1601902219),
(161, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"12\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:6;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-05 16:57:25\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-05 16:57:25.213437\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1601906245, 1601905345),
(162, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"1\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-14 14:36:17\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-14 14:36:17.829509\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1602675377, 1602674478),
(163, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"1\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-14 14:42:29\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-14 14:42:29.142804\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1602675749, 1602674849),
(164, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"1\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-14 14:44:23\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-14 14:44:23.997919\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1602675863, 1602674964),
(165, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"1\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-14 15:41:49\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-14 15:41:49.640992\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1602679309, 1602678409),
(166, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"1\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-14 17:00:26\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-14 17:00:26.394987\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1602684026, 1602683126),
(167, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"1\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-14 17:46:32\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-14 17:46:32.877122\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1602686792, 1602685893),
(168, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"1\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-17 02:19:01\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-17 02:19:01.329862\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1602890341, 1602889441),
(169, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"1\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-17 02:19:40\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-17 02:19:40.163804\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1602890380, 1602889480),
(170, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"1\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-17 02:26:49\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-17 02:26:49.252344\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1602890809, 1602889909),
(171, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"1\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-17 02:29:50\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-17 02:29:50.339626\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1602890990, 1602890090),
(172, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"6\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-18 17:32:47\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-18 17:32:47.912430\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603031567, 1603030669),
(173, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"7\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-18 17:33:49\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-18 17:33:49.342722\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603031629, 1603030729),
(174, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"6\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-18 17:34:41\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-18 17:34:41.389745\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603031681, 1603030781),
(175, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"7\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-18 17:37:07\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-18 17:37:07.728447\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603031827, 1603030927),
(176, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"8\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-18 17:38:17\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-18 17:38:17.784902\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603031897, 1603030997),
(177, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"9\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-18 17:43:38\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-18 17:43:38.838945\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603032218, 1603031318),
(178, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"10\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-18 17:45:13\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-18 17:45:13.886393\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603032313, 1603031413),
(179, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"11\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-18 17:45:39\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-18 17:45:39.787319\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603032339, 1603031439),
(180, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"12\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-18 17:51:05\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-18 17:51:05.787418\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603032665, 1603031765),
(181, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"6\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-18 17:53:41\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-18 17:53:41.373149\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603032821, 1603031921),
(182, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"8\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-18 21:39:13\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-18 21:39:13.169032\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603046353, 1603045453),
(183, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"7\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-18 21:39:43\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-18 21:39:43.530951\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603046383, 1603045483),
(184, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:1:\\\"6\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-18 21:41:33\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-18 21:41:33.547109\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603046493, 1603045593),
(185, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"14\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-18 22:18:15\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-18 22:18:15.427643\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603048695, 1603047795),
(186, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"15\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-18 22:20:59\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-18 22:20:59.581187\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603048859, 1603047959),
(187, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"16\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-18 22:29:08\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-18 22:29:08.333925\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603049348, 1603048448),
(188, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"14\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 01:06:00\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 01:06:00.047040\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603058760, 1603057860),
(189, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"15\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 01:07:05\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 01:07:05.902577\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603058825, 1603057925),
(190, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"16\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 01:14:08\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 01:14:08.776902\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603059248, 1603058348),
(191, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"14\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 01:14:49\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 01:14:49.088402\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603059289, 1603058389),
(192, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"14\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 01:16:41\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 01:16:41.935517\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603059401, 1603058501),
(193, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"16\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 01:19:02\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 01:19:02.367717\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603059542, 1603058642),
(194, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"14\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 01:37:30\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 01:37:30.422155\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603060650, 1603059750),
(195, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"14\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 01:47:15\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 01:47:15.125487\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603061235, 1603060335),
(196, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"16\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 01:55:16\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 01:55:16.463297\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603061716, 1603060816),
(197, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"17\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 02:00:03\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 02:00:03.795084\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603062003, 1603061103),
(198, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"18\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 14:33:31\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 14:33:31.323418\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603107211, 1603106311),
(199, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"14\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 14:36:14\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 14:36:14.269743\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603107374, 1603106474),
(200, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"14\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 14:39:13\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 14:39:13.075990\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603107553, 1603106653),
(201, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"16\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 14:45:12\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 14:45:12.716606\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603107912, 1603107012),
(202, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"18\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 14:46:02\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 14:46:02.150214\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603107962, 1603107062),
(203, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"14\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 14:47:28\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 14:47:28.076381\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603108048, 1603107148),
(204, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"14\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 14:49:56\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 14:49:56.997076\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603108196, 1603107297);
INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(205, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"14\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 14:51:22\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 14:51:22.880457\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603108282, 1603107382),
(206, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"14\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 14:51:51\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 14:51:51.765469\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603108311, 1603107411),
(207, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"14\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 14:59:42\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 14:59:42.786462\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603108782, 1603107882),
(208, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"25\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 15:28:14\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 15:28:14.507816\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603110494, 1603109594),
(209, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"25\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 15:46:33\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 15:46:33.724847\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603111593, 1603110693),
(210, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"33\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:17;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 15:54:07\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 15:54:07.182621\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603112047, 1603111147),
(211, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"34\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:19;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 15:58:56\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 15:58:56.018655\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603112336, 1603111436),
(212, 'default', '{\"displayName\":\"App\\\\Jobs\\\\PlaceTimer\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\PlaceTimer\",\"command\":\"O:19:\\\"App\\\\Jobs\\\\PlaceTimer\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";s:2:\\\"35\\\";s:13:\\\"\\u0000*\\u0000concert_id\\\";i:19;s:9:\\\"\\u0000*\\u0000expire\\\";s:19:\\\"2020-10-19 16:02:03\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-10-19 16:02:03.146923\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:11:\\\"Europe\\/Kiev\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1603112523, 1603111623);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_01_01_000000_create_pages_table', 1),
(6, '2016_01_01_000000_create_posts_table', 1),
(7, '2016_02_15_204651_create_categories_table', 1),
(8, '2016_05_19_173453_create_menu_table', 1),
(9, '2016_10_21_190000_create_roles_table', 1),
(10, '2016_10_21_190000_create_settings_table', 1),
(11, '2016_11_30_135954_create_permission_table', 1),
(12, '2016_11_30_141208_create_permission_role_table', 1),
(13, '2016_12_26_201236_data_types__add__server_side', 1),
(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(15, '2017_01_14_005015_create_translations_table', 1),
(16, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(17, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(18, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
(19, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(20, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(21, '2017_08_05_000000_add_group_to_settings_table', 1),
(22, '2017_11_26_013050_add_user_role_relationship', 1),
(23, '2017_11_26_015000_create_user_roles_table', 1),
(24, '2018_03_11_000000_add_user_settings', 1),
(25, '2018_03_14_000000_add_details_to_data_types_table', 1),
(26, '2018_03_16_000000_make_settings_value_nullable', 1),
(27, '2019_08_19_000000_create_failed_jobs_table', 1),
(28, '2020_06_09_105707_create_concerts_table', 1),
(29, '2020_06_23_191057_add_fields_to_concerts_table', 2),
(30, '2020_06_24_140614_add_before_and_after_table', 3),
(33, '2020_06_24_160135_add_datetime_to_concerts_table', 4),
(37, '2020_06_30_213946_create_etickets_table', 5),
(38, '2020_07_06_130025_add_hours_and_minutes_concert_table', 6),
(46, '2020_07_06_173052_create_cities_table', 7),
(49, '2020_07_06_181503_create_addresses_table', 8),
(50, '2020_07_14_145139_create_cultures_table', 9),
(52, '2020_07_14_145515_create_colors_table', 10),
(56, '2020_07_15_120235_drop_place_column_table', 13),
(57, '2020_07_15_120458_add_dk_id_column_table', 14),
(58, '2020_07_15_125943_add_color_id_table', 15),
(59, '2020_07_16_103730_create_sections_table', 16),
(60, '2020_07_16_104110_create_concert_prices_table', 17),
(61, '2020_07_16_113503_create_places_table', 18),
(62, '2020_07_16_114049_add_price_id_to_concerts_table', 19),
(63, '2020_07_16_212005_add_concert_id_to_concerts_prices_table', 20),
(64, '2020_07_16_213054_add_price_id_places_table', 21),
(65, '2020_07_17_111039_add_default_price_id', 22),
(66, '2020_07_17_112819_drop_price_id_foreign', 23),
(67, '2020_07_17_113033_add_new_price_id_foreign', 24),
(68, '2020_07_17_114025_drop_concert_id_foreign', 25),
(69, '2020_07_17_114103_add_new_concert_id_foreign', 26),
(70, '2020_07_17_121131_drop_price_id_column', 27),
(71, '2020_07_17_121730_add_new2_concert_id_foreign', 28),
(72, '2020_07_17_122000_add_foreign_concert_id', 29),
(73, '2020_07_18_223707_drop_price_id_col_places', 30),
(75, '2020_07_18_223925_add_foreign_key_places', 31),
(76, '2020_07_18_231727_default_engaged', 32),
(78, '2020_07_19_130433_drop_timestamps_colors', 34),
(79, '2020_07_20_174434_drop_foreign_concert_prices_and_places', 35),
(80, '2020_07_20_174632_add_foreign_concert_places', 36),
(81, '2020_07_20_175951_drop_pirce_id_on_places', 37),
(82, '2020_07_20_180155_drop_concert_id_on_concert_prices', 38),
(83, '2020_07_20_180411_add_place_id_concert_prices', 39),
(84, '2020_07_21_195911_add_foreign_places_on_concert_prices', 40),
(85, '2020_08_02_213622_add_section_slug', 41),
(86, '2020_08_02_230609_drop_concert_prices', 42),
(87, '2020_08_02_230808_drop_colors', 42),
(89, '2020_08_02_233319_add_color_places', 43),
(90, '2020_08_04_162021_create_new_colors_table', 44),
(91, '2020_08_08_200907_foreign_on_places', 45),
(92, '2020_08_09_213356_add_slug_cultures', 46),
(93, '2020_08_14_142912_drop_color_on_places', 47),
(94, '2020_08_31_134920_add_lang_to_etickets_table', 48),
(95, '2020_08_31_172459_add_city_ua_city_enrename_city_cities_table', 49),
(96, '2020_08_31_173455_city_ua_city_en_after_city_ru_cities_table', 50),
(97, '2020_08_31_174219_city_ua_city_en_after_city_ru_2_cities_table', 51),
(98, '2020_08_31_174507_city_ua_city_en_after_city_ru_3_cities_table', 52),
(99, '2020_08_31_202950_rename_city_ua_city_uk_table', 53),
(100, '2020_09_01_192614_remove_address_add_address_ru_address_ua_address_en_addresses_table', 54),
(101, '2020_09_01_192830_remove_address_add_address_ru_address_ua_address_en_2_addresses_table', 55),
(102, '2020_09_01_193812_remove_address_add_address_ru_address_ua_address_en_3_addresses_table', 56),
(103, '2020_09_02_120042_rename_address_ua_to_address_uk_addresses_table', 57),
(104, '2020_09_07_211956_add_culture_id_to_sections_table', 58),
(105, '2020_09_10_231116_add_expires_places', 59),
(106, '2020_09_11_004232_create_jobs_table', 60),
(107, '2020_09_29_180423_create_roles_table', 61),
(108, '2020_09_29_182943_create_groups_table', 61),
(109, '2020_09_29_183151_add_group_id_users_table', 61),
(110, '2020_09_29_183437_add_group_id_users_table_2', 62),
(111, '2020_09_29_184337_create_organizators_table', 62),
(112, '2020_09_29_184415_add_organizator_id_users_table', 62),
(113, '2020_09_29_184501_add_organizator_id_users_table_2', 62),
(114, '2020_09_29_184633_add_phone_login_users_table', 62),
(115, '2020_10_02_193735_before_after_nullable_concerts_table', 63),
(116, '2020_10_05_162404_roles_add_slug', 64),
(117, '2020_10_06_160656_drop_city__organizer_columns', 65),
(118, '2020_10_06_160847_add_city__organizer_foreign', 66),
(119, '2020_10_08_170603_create_distributors_table', 67),
(120, '2020_10_14_143607_create_shipping_table', 67),
(121, '2020_10_14_150939_create_fee_table', 68),
(122, '2020_10_14_154615_create_status_table', 69),
(124, '2020_10_14_160241_create_payments_table', 70),
(147, '2020_10_14_180933_create_orders_table', 71),
(148, '2020_10_17_194815_create_tickets_table', 72),
(149, '2020_10_18_220020_add_column_price_places', 73),
(150, '2020_10_15_171349_drop_lang_e_tickets_table', 74),
(151, '2020_10_15_171514_rename_title_ru_text_ru_e_tickets_table', 74),
(152, '2020_10_15_172202_add_title_uk_title_en_text_uk_text_en_tickets_table', 74),
(154, '2020_10_19_152056_default_expires_places', 75),
(155, '2020_10_19_195857_foreign_concert_id_status_id_ship_id_order_id_to_payments_table', 76),
(156, '2020_10_20_220020_add_column_price_places', 1),
(157, '2020_10_20_220020_add_column_price_places', 1),
(158, '2020_10_19_201521_foreign_concert_id_status_id_ship_id_order_id_to_payments_2_table', 77);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `concert_id` bigint(20) UNSIGNED NOT NULL,
  `distributor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fee_id` bigint(20) UNSIGNED NOT NULL,
  `shipping_id` bigint(20) UNSIGNED NOT NULL,
  `comments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `concert_id`, `distributor`, `fee_id`, `shipping_id`, `comments`, `created_at`, `updated_at`) VALUES
(1000070, 19, NULL, 2, 3, NULL, '2020-10-19 12:47:03', '2020-10-19 12:47:03');

-- --------------------------------------------------------

--
-- Структура таблицы `organizators`
--

CREATE TABLE `organizators` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `organizators`
--

INSERT INTO `organizators` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Organizator', '2020-10-02 22:26:44', '2020-10-02 22:26:45');

-- --------------------------------------------------------

--
-- Структура таблицы `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `payment_date` date NOT NULL,
  `sum` int(11) NOT NULL,
  `incoming` int(11) NOT NULL,
  `bank` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `concert_id` bigint(20) UNSIGNED NOT NULL,
  `status_id` bigint(20) UNSIGNED NOT NULL,
  `ship_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `places`
--

CREATE TABLE `places` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `section_id` bigint(20) UNSIGNED NOT NULL,
  `row` int(11) NOT NULL,
  `num_place` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `color_id` bigint(20) UNSIGNED NOT NULL,
  `engaged` tinyint(1) NOT NULL DEFAULT '0',
  `concert_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `role`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Администратор', 'admin', '2020-10-02 22:25:47', '2020-10-02 22:25:47');

-- --------------------------------------------------------

--
-- Структура таблицы `sections`
--

CREATE TABLE `sections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `culture_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `sections`
--

INSERT INTO `sections` (`id`, `name`, `slug`, `culture_id`) VALUES
(1, 'Партер', 'parterre', 6),
(2, 'Балкон', 'balcony', 6),
(3, 'Южная ложа', 'south', 6),
(4, 'Северная ложа', 'north', 6);

-- --------------------------------------------------------

--
-- Структура таблицы `shipping`
--

CREATE TABLE `shipping` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shipping`
--

INSERT INTO `shipping` (`id`, `type`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Самовывоз', 'pickup', '2020-10-14 11:37:57', '2020-10-14 11:37:58'),
(2, 'Курьером', 'courier', '2020-10-14 11:37:57', '2020-10-14 11:37:58'),
(3, 'Интернет', 'internet', '2020-10-14 11:38:45', '2020-10-14 11:38:46');

-- --------------------------------------------------------

--
-- Структура таблицы `status`
--

CREATE TABLE `status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `status`
--

INSERT INTO `status` (`id`, `type`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Успешно', 'success', '2020-10-14 12:48:12', '2020-10-14 12:48:13'),
(2, 'Отменен', 'canceled', '2020-10-14 12:48:27', '2020-10-14 12:48:28'),
(3, 'Тестовый', 'test', '2020-10-14 12:48:39', '2020-10-14 12:48:40');

-- --------------------------------------------------------

--
-- Структура таблицы `tickets`
--

CREATE TABLE `tickets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `place_id` bigint(20) UNSIGNED NOT NULL,
  `print` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_id` bigint(20) UNSIGNED DEFAULT NULL,
  `organizator_id` bigint(20) UNSIGNED DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `is_admin`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `group_id`, `organizator_id`, `phone`, `login`) VALUES
(3, 1, 'Admin', 1, 'admin@admin.com', 'users/default.png', '2020-10-02 22:35:52', '$2y$10$7VzFqLijWyF4NsPIwCBOWeyBH4oZUI534Vr4enG3Bmf8oW77sHalS', NULL, NULL, '2020-10-02 22:35:40', '2020-10-02 22:35:40', 1, 1, '3456', '4356436'),
(4, 1, 'werter', NULL, '2wert3ert@eret', 'users/default.png', NULL, '$2y$10$QzD.gcjMRHYTVSY1OiK2.e9/4cmIZ/7XjeDeGKc8/Go2bJh1N.Ka2', NULL, NULL, '2020-10-06 18:51:37', '2020-10-06 18:51:37', 1, 1, '2323', '3465646'),
(6, 1, 'ewertre', NULL, 'wewter@ertyre', 'users/default.png', NULL, '$2y$10$leRquc3a7H8X5AsrE8VBUe9pkd5p1exta/pFIqJNxMmrbfy/xeDjO', NULL, NULL, '2020-10-06 18:52:18', '2020-10-06 18:52:18', 1, 1, '2323', '2323'),
(7, 1, 'wetrwet', NULL, 'rwetw@wetw', 'users/default.png', NULL, '$2y$10$2ZQ/JuuwY5tuXOOj5UUHauvpPs3KbAwsMb67KKPdq17Nm.ra9H3dy', NULL, NULL, '2020-10-06 18:53:32', '2020-10-06 18:53:32', 1, 1, '2345', '25345'),
(8, 1, 'dertre222', NULL, 'renterj@ijreigjr', 'users/default.png', NULL, '$2y$10$88yiN8byGz7ELj8wlofKauYDStm7ECJpkVucWJ0/siRIMTeYgIgE6', NULL, NULL, '2020-10-12 20:14:37', '2020-10-12 20:38:46', 1, 1, '34653', 'yrtytry');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `addresses_city_id_foreign` (`city_id`);

--
-- Индексы таблицы `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `colors_concert_id_foreign` (`concert_id`);

--
-- Индексы таблицы `concerts`
--
ALTER TABLE `concerts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `concerts_dk_id_foreign` (`dk_id`),
  ADD KEY `concerts_city_id_foreign` (`city_id`),
  ADD KEY `concerts_organizer_id_foreign` (`organizer_id`);

--
-- Индексы таблицы `cultures`
--
ALTER TABLE `cultures`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `distributors`
--
ALTER TABLE `distributors`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `e_tickets`
--
ALTER TABLE `e_tickets`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `fee`
--
ALTER TABLE `fee`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_concert_id_foreign` (`concert_id`),
  ADD KEY `orders_fee_id_foreign` (`fee_id`),
  ADD KEY `orders_shipping_id_foreign` (`shipping_id`);

--
-- Индексы таблицы `organizators`
--
ALTER TABLE `organizators`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_concert_id_foreign` (`concert_id`),
  ADD KEY `payments_status_id_foreign` (`status_id`),
  ADD KEY `payments_ship_id_foreign` (`ship_id`),
  ADD KEY `payments_order_id_foreign` (`order_id`);

--
-- Индексы таблицы `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`id`),
  ADD KEY `places_section_id_foreign` (`section_id`),
  ADD KEY `places_concert_id_foreign` (`concert_id`),
  ADD KEY `places_color_id_foreign` (`color_id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sections_culture_id_foreign` (`culture_id`);

--
-- Индексы таблицы `shipping`
--
ALTER TABLE `shipping`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tickets_order_id_foreign` (`order_id`),
  ADD KEY `tickets_place_id_foreign` (`place_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`),
  ADD KEY `users_group_id_foreign` (`group_id`),
  ADD KEY `users_organizator_id_foreign` (`organizator_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT для таблицы `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT для таблицы `colors`
--
ALTER TABLE `colors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `concerts`
--
ALTER TABLE `concerts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `cultures`
--
ALTER TABLE `cultures`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `distributors`
--
ALTER TABLE `distributors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `e_tickets`
--
ALTER TABLE `e_tickets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `fee`
--
ALTER TABLE `fee`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `groups`
--
ALTER TABLE `groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=213;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000076;

--
-- AUTO_INCREMENT для таблицы `organizators`
--
ALTER TABLE `organizators`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `places`
--
ALTER TABLE `places`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `sections`
--
ALTER TABLE `sections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `shipping`
--
ALTER TABLE `shipping`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `status`
--
ALTER TABLE `status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `colors`
--
ALTER TABLE `colors`
  ADD CONSTRAINT `colors_concert_id_foreign` FOREIGN KEY (`concert_id`) REFERENCES `concerts` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `concerts`
--
ALTER TABLE `concerts`
  ADD CONSTRAINT `concerts_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `concerts_dk_id_foreign` FOREIGN KEY (`dk_id`) REFERENCES `cultures` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `concerts_organizer_id_foreign` FOREIGN KEY (`organizer_id`) REFERENCES `organizators` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_concert_id_foreign` FOREIGN KEY (`concert_id`) REFERENCES `concerts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_fee_id_foreign` FOREIGN KEY (`fee_id`) REFERENCES `fee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_shipping_id_foreign` FOREIGN KEY (`shipping_id`) REFERENCES `shipping` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_concert_id_foreign` FOREIGN KEY (`concert_id`) REFERENCES `concerts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payments_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payments_ship_id_foreign` FOREIGN KEY (`ship_id`) REFERENCES `shipping` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payments_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `places`
--
ALTER TABLE `places`
  ADD CONSTRAINT `places_color_id_foreign` FOREIGN KEY (`color_id`) REFERENCES `colors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `places_concert_id_foreign` FOREIGN KEY (`concert_id`) REFERENCES `concerts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `places_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sections`
--
ALTER TABLE `sections`
  ADD CONSTRAINT `sections_culture_id_foreign` FOREIGN KEY (`culture_id`) REFERENCES `cultures` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `tickets_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tickets_place_id_foreign` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_organizator_id_foreign` FOREIGN KEY (`organizator_id`) REFERENCES `organizators` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
