$(function() {
    let payment = $('#payment');
    payment.addClass('disabled');
    payment.attr('disabled','true');

// $('.fw-input').focus(function () {
// $(this).parents('.fw-field').addClass('fw-focused');
// });
// $('.fw-input').focusout(function () {
//     $(this).parents('.fw-field').removeClass('fw-focused');
// });
//
// $('.fw-input').keypress(function () {
//     $(this).parents('.fw-field').addClass('fw-has-value');
//     if($(this).val() === ''){
//         $(this).parents('.fw-field').removeClass('fw-has-value');
//     }
// });
//
// $('.fw-input').keyup(function () {
//     if($(this).val() === ''){
//         $(this).parents('.fw-field').removeClass('fw-has-value');
//     }
// });

$('.fw-input').focusout(function () {
if($('#name').val() !== '' && $('#tel').val() !== '' && $('#email').val() !== ''){
    $('.profile_svg').hide();
    $('.profile1_svg').show();
    $('.fio').hide();
    $('.check-name').show();
    var name = $('#name').val();
    var tel = $('#tel').val();
    var email = $('#email').val();
    $('.check-name-text-name').html(name);
    $('.check-name-text-tel').html(tel);
    $('.check-name-text-email').html(email);
    // $('.select_place-2-form').show();
    // $('.checkbox-step-3-1').css('margin-left','-690px');
    $('.edit-step-3').show();
    $('.delivery_svg').show();
    $('.delivery1_svg').hide();
    $('.select_place-2-form').css('opacity','1');
    $('.change_cashier').show();
    $('.pay').css('margin-top','0px');
    $('.line-select_place-4').hide();
    if($('#online_ticket').prop('checked')){
        $('.change_cashier').hide();
        $('.delivery_svg').hide();
        $('.delivery1_svg').show();
    }
    if($('#buy_by_yourself').prop('checked')){
        $('.change_cashier').hide();
        $('.delivery_svg').hide();
        $('.delivery1_svg').show();
    }
}else{
    $('.profile1_svg').hide();
    $('.profile_svg').show();
}
});

$('.edit').click(function () {
    $('.fio').show();
    $('.check-name').hide();
    $('.profile_svg').show();
    $('.profile1_svg').hide();
});

$('#online_ticket').click(function () {
$('.delivery_svg').hide();
$('.delivery1_svg').show();
$('.select_place-2-form p').hide();
$('.select_place-2-form .change_cashier').hide();
$('.line-select_place-3').css('height','50px');
$('.checkbox-step-3 img').hide();
$('.select_place-2-form .checkbox-step-3-2').hide();
    $('#card').prop('checked', true);
    $('.checkbox-step-4-2').hide();
    $('.select_place-3-form p').hide();
    $('.select_place-3-form #comment').hide();
    $('.pay_svg').hide();
    $('.pay1_svg').show();
})

$('.edit-step-3').click(function () {
    $('.delivery_svg').show();
    $('.delivery1_svg').hide();
    $('.select_place-2-form p').show();
    $('.select_place-2-form .change_cashier').show();
    $('.line-select_place-3').css('height','200px');
    $('.checkbox-step-3 img').show();
    $('.select_place-2-form .checkbox-step-3-2').show();
    $('.select_place-2-form input[type=checkbox]').prop('checked', false);
    $('.checkbox-step-3-1').show();
    $('#buy_by_yourself').css('margin-left','0px');
    $('.checkbox-step-3').css('margin-top','20px');
    $('.checkbox-step-4-2').show();
    $('.select_place-3-form p').show();
    $('#comment').show();
    $('.pay_svg').show();
    $('.pay1_svg').hide();
    $('.select_place-3-form input[type=checkbox]').prop('checked', false);
    $('.checkbox-step-4-1').show();
    $('.checkbox-step-4-1').css('margin-top','10px');
    $('.checkbox-step-4-2').css('margin-left','-110px');
    $('.select_place-3-form').css('opacity','0.0001');
    // $('.link-dnk-order-copy').addClass('disabled');
    payment.addClass('disabled');
    payment.attr('disabled','true');
    $('.link-dnk-order-copy-mobile').addClass('disabled');
})

$('#buy_by_yourself').click(function () {
    $('.delivery_svg').hide();
    $('.delivery1_svg').show();
    $('.select_place-2-form p').hide();
    $('.select_place-2-form .change_cashier').hide();
    $('.line-select_place-3').css('height','50px');
    $('.checkbox-step-3 img').hide();
    $('.checkbox-step-3').css('margin-top','5px');
    $('.select_place-2-form .checkbox-step-3-1').hide();
    $('#buy_by_yourself').css('margin-left','100px');
    $('.pay_svg').hide();
    $('.pay1_svg').show();
    $('.checkbox-step-4-1').hide();
    $('#comment').hide();
    $('.select_place-3-form p').hide();
    $('.checkbox-step-4-2').css('margin-left','0px');
    $('.checkbox-step-4').css('margin-top','-10px');
    $('.select_place-3-form input[type=checkbox]').prop('checked', true);
})

$('#online_ticket').click(function () {
$('.select_place-3-form').css('opacity','1');
// $('.link-dnk-order-copy').removeClass('disabled');
    payment.removeClass('disabled');
    payment.attr('disabled',false);
$('.link-dnk-order-copy-mobile').removeClass('disabled');
})
$('#buy_by_yourself').click(function () {
    $('.select_place-3-form').css('opacity','1');
    // $('.link-dnk-order-copy').removeClass('disabled');
    payment.removeClass('disabled');
    payment.attr('disabled',false);
    $('.link-dnk-order-copy-mobile').removeClass('disabled');
})

$( ".link-dnk-order-copy" ).click(function() {
    $( ".data" ).submit();
});

    $('#online_ticket').click(function () {
        if($('#online_ticket').prop('checked',true)) {
            $('#online_ticket').removeAttr('checked');
        }
    });
$('#card').click(function () {
    if($('#card').prop('checked',true)) {
        $('#card').removeAttr('checked');
    }
});
$('#buy_by_yourself').click(function () {
    if($('#buy_by_yourself').prop('checked',true)) {
        $('#buy_by_yourself').removeAttr('checked');
    }
});
$('#cash').click(function () {
    if($('#cash').prop('checked',true)) {
        $('#cash').removeAttr('checked');
    }
});

$('#tel').on('input', function() {
    $(this).val($(this).val().replace (/\D/, ''));
});
});