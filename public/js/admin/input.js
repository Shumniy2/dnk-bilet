$(function () {
    $('.fw-input-admin').focus(function () {
        $(this).parents('.fw-field').addClass('fw-focused');
    });
    $('.fw-input-admin').focusout(function () {
        if ($(this).val() === '') {
            $(this).parents('.fw-field').removeClass('fw-focused');
        }
    });

    $('.fw-input-admin').keypress(function () {
        $(this).parents('.fw-field').addClass('fw-has-value');
        if ($(this).val() === '') {
            $(this).parents('.fw-field').removeClass('fw-has-value');
        }
    });

    $('.fw-input-admin').keyup(function () {
        if ($(this).val() === '') {
            $(this).parents('.fw-field').removeClass('fw-has-value');
        }
    });

    $('.fw-input').focus(function () {
        $(this).parents('.fw-field').addClass('fw-focused');
    });
    $('.fw-input').focusout(function () {
        if ($(this).val() === '') {
            $(this).parents('.fw-field').removeClass('fw-focused');
        }
    });

    $('.fw-input').keypress(function () {
        $(this).parents('.fw-field').addClass('fw-has-value');
        if ($(this).val() === '') {
            $(this).parents('.fw-field').removeClass('fw-has-value');
        }
    });

    $('.fw-input').keyup(function () {
        if ($(this).val() === '') {
            $(this).parents('.fw-field').removeClass('fw-has-value');
        }
    });
});