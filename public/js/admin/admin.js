var color_control = new (function () {
    var self = this;
    var BreakException = {};
    let origin = window.location.origin;
    let url = window.location.href;

    let $table = $("#table_tickets tr:first-child");
    let $modal = $("#modal");
    let $modal_place = $("#modal_place").hide();
    let $places = $(".dnk_circle").not('.dnk_circle_invisible, .dnk_white');
    let $mod_price = $modal_place.find('#apply_price');
    let $mod_count = $modal_place.find('#apply_count');
    let $msg = $(".modal-save-body").find('.msg');


    let current_color = null;
    let current_price = null;
    let count_places = null;

    let old_colors = [];
    let selected = [];
    let table = [];

    self.init = function () {

        self.setTotal();
        self.handlerHover();

        if ($("#table_tickets").find(".ticket_block").length > 0) {
            self.buttonsControls(true);
            self.getPlaces();
        } else {
            $("#table_tickets").on('click', 'tr.ticket_block', self.activate);
        }

        $("#add_color").on('click', function () {
            $("#table_tickets tr.ticket_block").removeClass('active');
            $modal.modal('show');
        });

        $("#remove_color").on('click', async function () {
            const response = await fetch(origin + '/removeColor', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                    'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
                },
                body: JSON.stringify({
                    'color_id': $("#table_tickets tr.ticket_block.active").data('id'),
                })
            });
            if (response.ok) {
                let answer = await response.json();
                if (answer.status === "ok") {
                }
            } else {
                alert("Error HTTP: " + response.status);
            }
        });


        $('.dnk-place').on('click', '.dnk_circle', function () {
            if ($(this).hasClass('price_selected') && !$(this).data('color')) {
               self.addColor($(this));
            } else if ($(this).hasClass('price_selected') && $(this).data('color')) {
              self.removeColor($(this));
            }
            self.updateApply();
        });

        self.addColor = function($target) {
            if (current_color) {
                $target.data('color', current_color);
                $target.css({'background-color': current_color});
                let data = self.getParams($target[0]);
                let index = selected.push(data) - 1;
                $target.attr('inx', index);
                count_places++;
            }
        };

        self.removeColor = function($target) {
            let color = $target.data('color');
            if(current_color === color) {
                let inx_remove = parseInt($target.attr('inx'));
                removeElement(selected, inx_remove);
                // delete selected[inx_remove];
                $target.removeAttr('inx');
                count_places--;

                $target.data('color', '');
                $target.css({'background-color': ""});

            } else {
                self.saveOldColors($target);
                self.addColor($target);
            }
        };


        $("#save_price").on('click', self.savePrice);
        $("#cancel_price").on('click', self.cancelPrice);

        $("#save_place").on('click', self.savePlaces);
        $("#cancel_place").on('click', self.cancelPlace);

        $("#update_colors").on('click', function () {
            $(".ticket_block").removeClass('active');
            $places.removeClass('price_selected');
        });

    };

    self.activate = function () {
        if(!self.isHasActive()) {
            $(this).addClass('active');
            $places.addClass('price_selected');

            current_color = $(this).find('.ticket_color').data('color');
            current_price = $(this).find('.ticket_price').data('price');
        }
    };

    self.saveOldColors = function($target) {
        let data = self.getParams($target[0]);
        old_colors.push(data) - 1;
    };

    self.preparedSave = function () {
        self.clearModal();
        self.updateRow();
    };

    self.clearModal = function () {
        $modal_place.hide();
        $mod_price.text(0);
        $mod_count.text(0);

        $mod_price.data('apply-price', 0);
        $mod_count.data('apply-count', 0);
    };

    self.isHasActive = function () {
        let status = false;
        $("#table_tickets tr.ticket_block").each(function () {
            if($(this).hasClass('active')) {
                status = true;
            }
        });
        return status;
    };

    self.savePlaces = async function () {
        self.preparedSave();
        const response = await fetch(origin + '/savePlaces', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            body: JSON.stringify({
                'places': selected,
                'colors': table
            })
        });
        if (response.ok) {
            let answer = await response.json();
            if (answer.status === "ok") {
                selected = [];
                table = [];
            }
        } else {
            alert("Error HTTP: " + response.status);
        }
    };

    self.updatePlaceStatus = function () {
        data.forEach(function (el) {
            let $place = $(".dnk_place[data-row='" + el.row + "'][data-place='" + el.num_place + "'][data-section='" + el.section_id + "']");
            $place.data('color', el.color);
            $place.css({'background-color': el.color});
        });
    };

    self.getPlaces = async function () {
        const response = await fetch(origin + '/getPlaces', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            body: JSON.stringify({
                'id': url.substring(url.lastIndexOf('/') + 1),
                'places': true
            })
        });
        if (response.ok) {
            let answer = await response.json();
            self.updateScene(answer);
        } else {
            alert("Error HTTP: " + response.status);
        }
    };

    self.updateScene = function (data) {
        data.forEach(function (el) {
            let $place = $(".dnk_place[data-row='" + el.row + "'][data-place='" + el.num_place + "'][data-section='" + el.section_id + "']");
            $place.data('color', el.color);
            $place.css({'background-color': el.color});
        });
        self.buttonsControls(false);
        $("#table_tickets").on('click', 'tr.ticket_block', self.activate);
    };

    self.buttonsControls = function(state) {
        $(".btn.btn-primary").prop('disabled', state);
    };

    self.setTotal = function () {
        let count = 0;
        let total = 0;
        $("#table_tickets tr.ticket_block").each(function () {
            let params = self.getTableParams($(this));
            // self.updateTotal(params);
            count += params['count'];
            total += params['total'];
            if ($(this).hasClass('active')) {
                table.push(params);
            }

        });
        $(".ticket_total").find('td[data-count]').text(count);
        $(".ticket_total").find('td[data-total]').text(total);
    };

    self.getTableParams = function (el) {
        let data = {};
        data['price'] = el.find('.ticket_price').data("price");
        data['count'] = el.find('.ticket_count').data("count");
        data['color'] = el.find('.ticket_color').data("color");
        data['total'] = el.find('.ticket_total').data("total");

        return data;
    };

    self.updateTotal = function (data) {
        let $total_count = $(".ticket_total").find('td[data-count]');
        let $total = $(".ticket_total").find('td[data-total]');

        let total_cnt = $total_count.data('count');
        let total_cnt_new = total_cnt + data['count'];

        $total_count.data('count', total_cnt_new);
        $total_count.text(total_cnt_new);

        let total = $total.data('total');
        let total_new = total + data['total'];
        $total.data('total', total_new);
        $total.text(total_new);
    };


    self.savePrice = function () {
        let $ipt = $('#ticket_price');
        let $val = parseInt($ipt.val());
        let msg = null;
        if(isFinite($val)) {
            let status = self.priceControl($val);
            if (status) {
                if ($val != null && $val > 0) {
                    let color = self.getColor();
                    $table.after("<tr class='ticket_block' tabindex='0'><td class='ticket_price' data-price='" + $val + "'>" + $val + "</td>"
                        + "<td><div class='ticket_color' data-color='" + color + "' style='background-color: " + color + "'></div>"
                        + "</td><td class='ticket_count' data-total=''>0</td><td class='ticket_total' data-total=''>0</td></td></tr>");
                }
                $modal.modal('hide');
                $ipt.val("");
            } else {
                $ipt.val("");
                msg = "Такая цена уже существует";
            }
        } else {
            msg = "Введите цену";
        }

        if(msg != null) {
            $msg.text(msg);
            $msg.show().delay(5000).queue(function (next) {
                $(this).hide();
                next();
            });
        }
    };

    self.cancelPlace = function () {
        self.clearModal();
        self.cancelSelected();
    };

    self.cancelPrice = function () {
        $("#ticket_price").val("");
        $("#modal").modal('hide');
    };

    self.cancelSelected = function () {
        let has_old = old_colors.length > 0;
        let data_selected = has_old ? old_colors : selected;
        data_selected.forEach(function (el) {
            let color = has_old ? el.color : '';
            let $place = $(".dnk_place[data-row='" + el.row + "'][data-place='" + el.place + "'][data-section='" + el.section + "']");
            $place.data('color', color);
            $place.css({'background-color': color});
        });
        old_colors = [];
        selected = [];
        count_places = 0;
    };

    self.priceControl = function (val) {
        let exists = true;
        try {
            $("#table_tickets").find('.ticket_block').each(function () {
                let price = $(this).find('.ticket_price').data('price');
                if (price == val) {
                    exists = false;
                    throw BreakException;
                }
            });
        } catch (e) {
            if (e !== BreakException) throw e;
        }

        return exists;
    };

    self.updateApply = function () {

        if (count_places > 0) {
            $modal_place.show();

            $mod_price.text(current_price);
            $mod_count.text(count_places);

            $mod_price.data('apply-price', current_price);
            $mod_count.data('apply-count', count_places);
        } else {
            $modal_place.hide();

            $mod_price.text(0);
            $mod_count.text(0);

            $mod_price.data('apply-price', 0);
            $mod_count.data('apply-count', 0);
        }
    };

    self.updateRow = function () {
        let $target_count = $('.ticket_block.active').find('.ticket_count');
        let $target_total = $('.ticket_block.active').find('.ticket_total');

        let count = $target_count.data('count');
        if (count > 0) {
            let actual = count + count_places;
            $target_count.data('count', actual);
            $target_count.text(actual);

            let total = count_places * current_price;
            let prev_total = $target_total.data('total');
            prev_total += total;
            $target_total.data('total', prev_total);
            $target_total.text(prev_total);
        } else {
            $target_count.data('count', count_places);
            $target_count.text(count_places);

            let total = count_places * current_price;

            $target_total.data('total', total);
            $target_total.text(total);
        }
        count_places = 0;
        self.setTotal();
    };

    self.handlerHover = function () {
        $(".dnk-place").on('mouseover', '.dnk_place', {show: true}, self.bubble);
        $(".dnk-place").on('mouseout', '.dnk_place', {show: false}, self.bubble);
    };

    self.bubble = function (event) {
        let row = $(this).data('row');
        let place = $(this).data('place');
        let section = $(this).data('section');
        if (isFinite(row) && isFinite(place)) {
            let attr = 'bubble_' + section + "_" + row + "_" + place;
            let $target = $(document).find(".tooltiptext-bubble[data-hover='" + attr + "']");
            if (event.data.show) {
                $target.css({
                    'visibility': 'visible',
                    'opacity': '1'
                });
            } else {
                $target.css({'visibility': 'hidden'});
            }
        }
    };

    self.getColor = function () {
        const letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    };


    self.rand = function () {
        return Math.floor(Math.random() * 256);
    };

    self.getParams = function (el) {
        let data = {};
        [].forEach.call(el.attributes, function (attr) {
            if (/^data-/.test(attr.name)) {
                let camelCaseName = attr.name.substr(5).replace(/-(.)/g, function ($0, $1) {
                    return $1.toUpperCase();
                });
                data[camelCaseName] = attr.value;
            }
        });
        if($(el).data('color')) {
            data['color'] = $(el).data('color');
        }
        return data;
    };

    function removeElement(array, index) {
        if (index > -1) {
            array.splice(index, 1);
        }
    }

});

$(function () {
    color_control.init();
});