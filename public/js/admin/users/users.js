import * as admin from '../admin_module.js';

$(function() {

    //     format:'Y-m-d H:i'
    // $('#date').datetimepicker({
    // });

    $('#user_create_form').on('submit', {
        'url': '/admin/users/store',
        'table': $("#users"),
        'modal': $("#userModal")
    }, admin.onCreate );

    $(".edit-user").on("click", {
        'table': $("#users")
    }, admin.editModal);


    $('#user_edit_form').on('submit', {
        'url': '/admin/users/update',
        'table': $("#users"),
        'modal': $("#userEditModal")
    }, admin.onUpdate);

});