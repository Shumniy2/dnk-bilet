import * as admin from '../admin_module.js';

$(function() {

    $('#city_create_form').on('submit', {
        'url': '/admin/cities/store',
        'table': $("#cities"),
        'modal': $("#concertModal")
    }, admin.onCreate );


    $(".edit-city").on("click", {
        'table': $("#cities")
    }, admin.editModal);


    $('#city_edit_form').on('submit', {
        'url': '/admin/cities/update',
        'table': $("#cities"),
        'modal': $("#cityEditModal")
    }, admin.onUpdate);
});