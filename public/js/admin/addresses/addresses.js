import * as admin from '../admin_module.js';

$(function() {

    $('#text_ru').summernote({
        toolbar: [
            ['style', ['bold', 'italic', 'underline']],
            ['fontsize', ['fontsize']],
            ['fontname', ['fontname']],
            ['height', ['height']],
        ]
    });
    $('#text_ua').summernote({
        toolbar: [
            ['style', ['bold', 'italic', 'underline']],
            ['fontsize', ['fontsize']],
            ['fontname', ['fontname']],
            ['height', ['height']],
        ]
    });
    $('#text_en').summernote({
        toolbar: [
            ['style', ['bold', 'italic', 'underline']],
            ['fontsize', ['fontsize']],
            ['fontname', ['fontname']],
            ['height', ['height']],
        ]
    });
    $('#text_ru_edit').summernote({
        toolbar: [
            ['style', ['bold', 'italic', 'underline']],
            ['fontsize', ['fontsize']],
            ['fontname', ['fontname']],
            ['height', ['height']],
        ]
    });
    $('#text_ua_edit').summernote({
        toolbar: [
            ['style', ['bold', 'italic', 'underline']],
            ['fontsize', ['fontsize']],
            ['fontname', ['fontname']],
            ['height', ['height']],
        ]
    });
    $('#text_en_edit').summernote({
        toolbar: [
            ['style', ['bold', 'italic', 'underline']],
            ['fontsize', ['fontsize']],
            ['fontname', ['fontname']],
            ['height', ['height']],
        ]
    });

    $('#address_create_form').on('submit', {
        'url': '/admin/addresses/store',
        'table': $("#addresses"),
        'modal': $("#addressModal")
    }, admin.onCreate );

    $(".edit-address").on("click", {
        'table': $("#addresses")
    }, admin.editModal);


    $('#address_edit_form').on('submit', {
        'url': '/admin/addresses/update',
        'table': $("#addresses"),
        'modal': $("#addressEditModal")
    }, admin.onUpdate);

});