import * as admin from '../admin_module.js';

$(function() {

    $('#description').summernote({
        toolbar: [
            ['style', ['bold', 'italic', 'underline']],
            ['fontsize', ['fontsize']],
            ['fontname', ['fontname']],
            ['height', ['height']],
        ]
    });

    $('#date-create').datetimepicker({
        format:'Y:m:d H:i',
        lang:'en'
    });
    $('#date').datetimepicker({
        format:'Y:m:d H:i',
        lang:'en'
    });

    $('#concert_create_form').on('submit', {
        'url': '/admin/concerts/store',
        'table': $("#concerts"),
        'modal': $("#concertModal")
    }, admin.onCreate );


    $(".edit-concert").on("click", {
        'table': $("#concerts")
    }, admin.editModal);


    $('#concert_edit_form').on('submit', {
        'url': '/admin/concerts/update',
        'table': $("#concerts"),
        'modal': $("#concertEditModal")
    }, admin.onUpdate);

    $('.parametrs_block input').on('change', function() {
        $('input').not(this).prop('checked', false);
    });

    $("#fl_inp").change(function(){
        var filename = $(this).val().replace(/.*\\/, "");
        $("#fl_nm").html(filename);
    });

});




