export function onUpdate(e) {
    e.preventDefault();

    e.data.target = this;
    let data = e.data;

    let fd = new FormData(data.target);

    $.ajax({
        url: location.origin + data.url,
        type: "POST",
        processData: false,
        contentType: false,
        headers: {
            'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
        },
        data: fd,
        success: function (answer) {
            let tr = $(data.table).find("tr[data-city=" + answer.id + "]")[0];
            $(tr).children().each(function () {
                let index = $(this).data("td");
                if (index) {
                    let val = answer[index];
                    $(this).text(val);
                    $(this).data('value', val);
                }
            });
            $(data.modal[0]).modal("hide");

        },
        error: function (err) {
            console.log(err);
        }
    }).done(function () {
        console.log("Done");
    });
}

export function editModal(e) {
    let data = e.data;
    const id = $(this).data('id');
    if (id) {
        $("#id").val(id);
        let tr = $(data.table).find("tr[data-city=" + id + "]")[0];
        $(tr).children().each(function () {
            let index = $(this).data("td");
            if (index) {
                $("#" + index).val($(this).data("value"));
            }
        });
    }
}


export function onCreate(e) {
    e.preventDefault();
    e.data.target = this;
    let data = e.data;
    let fd = new FormData(data.target);

    $.ajax({
        url: location.origin + data.url,
        type: 'POST',
        processData: false,
        contentType: false,
        headers: {
            'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
        },
        data: fd,
        success: function (answer) {
            $(data.table[0]).find("tr").last().after(answer);
            $(data.modal[0]).modal("hide");
            $(data.target)[0].reset()
        },
        error: function (err) {
            console.log(err);
        }
    });
}
