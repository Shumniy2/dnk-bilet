let start = new (function () {
    let self = this;

    let $concerts = $(".concerts-block");
    let $loader = $(".ajax-loader").hide();
    let url = window.location.href;

    self.init = function () {
        self.loading = false;
        self.isEmpty = false;
        self.count = 2;

        $(document).on("scroll", self.onScroll);
        $("#ajax_search").on("input", self.onSearch);
    };

    self.onScroll = function () {
        let $window_scroll = $(window).scrollTop();
        let $window_height = $concerts.height();
        let $footer = $(".dnk-footer").height();
        if (!self.isEmpty && ($window_scroll >= $window_height - $footer) && !self.loading) {
            let data = {};
            data['token'] = $('meta[name="csrf-token"]').attr('content')
            data['page'] = self.count;
            $.ajax({
                url: url,
                data: data,
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': data["token"]
                },
                beforeSend: function () {
                    self.loading = true;
                    $loader.show();
                },
                success: function (data) {
                    if (data != "") {
                        $concerts.append(data);
                    } else {
                        self.isEmpty = true;
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            }).done(function () {
                $loader.hide();
                self.loading = false;
                self.count++;
            });
        }
    };

    self.onSearch = function () {
        let isSearching = false;
        let query = this.value;
        if (!isSearching) {
            $.ajax({
                url: url + "search",
                data: {query: query},
                dataType: 'json',
                method: "GET",
                beforeSend: function () {
                    $(document).off("scroll");
                    isSearching = true;
                },
                success: function (data) {
                    $concerts.empty();
                    $concerts.html(data);
                },
                error: function (err) {
                    console.log(err);
                }
            }).done(function () {
                isSearching = false;
                if(query.length == 0) {
                    $(document).on("scroll", self.onScroll);
                }
            });
        }
    };
});
$(function () {
    start.init();
});

