import * as Timer from '../modules/Timer.js';

var checkout = new (function () {
    var self = this;

    self.init = function () {
        self.getStatus();
        $("#payment").on("click", self.processing);
    };

    self.processing = function () {
        let data = {
            "contact": $("#name").val(),
            "phone": $("#tel").val(),
            "email": $("#email").val(),
            "shipping": $(".checkbox-step-3 input[type=checkbox]:checked").attr("name"),
            "payment": $(".checkbox-step-4 input[type=checkbox]:checked").attr("name"),
        };
        $.ajax({
            url: origin + '/payment',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            data: {
                'data': JSON.stringify(data)
            },
            success: function(answer) {
                if(answer.status === 'success') {
                    $("#form_buy").submit();
                }
            }
        });
    };

    self.getStatus = function() {
        $.ajax({
            url: origin + '/processingStatus',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            success: function (status) {
                if(status != "continue") {
                    Timer.stopTimer();
                    self.clearCart();
                } else {
                    Timer.startTimer();
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    };
});

$(function () {
    checkout.init();
});