let loader = new (function () {
    const TOKEN = 'pk.eyJ1IjoiZG5rLWJpbGV0IiwiYSI6ImNrY2NpemJvMjA1NWYyenA5cXgwM3FjbngifQ.b7TLL3uQ_xo1Sm3wy_AENA';
    const SOURCE = window.location.origin + '/geojson/markers.geojson';
    var self = this;
    var map;

    self.init = function () {
        let center = [35.038077, 48.470270];
        let zoom = 15;
        $links = $('.btn-link-address');

        mapboxgl.accessToken = TOKEN;
        map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: center,
            zoom: zoom,
            minZoom: 10,
            attributionControl: false
        });

        map.addControl(new mapboxgl.NavigationControl(), 'bottom-right');


        $.getJSON(SOURCE, function (geojson) {
            let index = 0;
            geojson.features.forEach(function (marker) {
                var el = document.createElement('div');
                el.className = 'marker';
                let coordinates = marker.geometry.coordinates;

                new mapboxgl.Marker()
                    .setLngLat(coordinates)
                    .setPopup(new mapboxgl.Popup({offset: 25})
                        .setHTML('<span class="marker-title">' + marker.properties.title + '</span>' +
                        '<p>' + marker.properties.description + '</p>'))
                    .addTo(map);

                $links.eq(index).attr('data-target-marker', coordinates.join(';'));
                index++;
            });
        });

        self.activateLinks();
    };

    self.activateLinks = function () {
        $('#accordionExample').on('click', '.btn-link-address', function () {
            $links.removeClass('active');
            let coordinates = $(this).attr('data-target-marker');
            map.flyTo({
                center: coordinates.split(';'),
                essential: true
            });
            $(this).addClass('active');
        });
    };

});

$(function () {
    if (mapboxgl.supported()) {
        loader.init();
    } else {
        alert('Your browser does not support Mapbox GL');
    }
});