window.onload = function() {
    L.mapquest.key = 'nzQmkWoaw9njB58AuLIRzAFQ33VwcYTq';

    var map = L.mapquest.map('map', {
        center: [48.470270, 35.038077],
        layers: L.mapquest.tileLayer('satellite'),
        zoom: 16
    });

    L.marker([48.470270, 35.038077], {
        icon: L.mapquest.icons.marker(),
        draggable: false
    }).bindPopup('Dnepr, Test').addTo(map);


    L.marker([40.470270, 31.038077], {
        icon: L.mapquest.icons.marker(),
        draggable: false
    }).bindPopup('Test, Test').addTo(map);

    map.addControl(L.mapquest.control());
};