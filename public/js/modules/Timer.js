let $time = $("#time");
var refTime;

const DEFAULT_TIME_LABEL = "15:00";


export function startTimer() {
    let duration = self.getTimer();
    if(duration) {
        var timer = duration, minutes, seconds;

        refTime = setInterval(function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            let time = minutes + ":" + seconds;
            $time.text(time);

            if (timer <= 0) {
                self.clearCart();
            }
            timer--;
            self.updateTimer(timer);
        }, 1000);
    }
}


self.getTimer = function() {
    let name = 'timer';
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? parseInt(decodeURIComponent(matches[1])) : undefined;
};

self.updateTimer = function(value) {
    let expire = 86400;
    let expireToDate = new Date();
    expireToDate.setTime(new Date().getTime() + expire * 1000);
    let cookieStr = encodeURIComponent('timer') + "=" + encodeURIComponent(value) + "; path=/; Max-Age=" + expire + "; Expires=" + expireToDate.toUTCString();
    document.cookie = cookieStr;
};

export function stopTimer() {
    $time.text(DEFAULT_TIME_LABEL);
    clearInterval(refTime);
}