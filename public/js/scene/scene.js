import * as Timer from '../modules/Timer.js';

var scene = new (function () {
    var self = this;
    self.is_timing = false;

    const COLOR = '#112535';
    const DEFAULT_TIME_LABEL = "15:00";

    let origin = window.location.origin;
    let url = window.location.href;
    let $loader = $(".ajax-loader").hide();
    let $overlay = $(".scene-overlay").hide();
    let $time = $("#time");


    let $places = $(".admin-scene > .dnk_circle").not('.dnk_circle_invisible, .dnk_white');
    let old_colors = [];


    self.init = function () {
        self.loading = false;
        self.getStatus();
        self.getPlaces();
        self.handlerHover();
        $("#clearCart").on('click', self.clearCart);
        $('.dnk-place').on('click', '.dnk_circle', self.reserve);
        self.initCart();
    };


    self.reserve = async function() {
        let $el = $(this);
        if ($(this).data('color') && typeof $el.data('engaged') === 'undefined') {
            let $btn = $(".link-dnk-ticket-id");
            self.saveOldColors($(this));
            $(this).data('color', COLOR);
            $(this).css('background-color', COLOR);

            $.ajax({
                url: origin + '/reservePlace',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                },
                beforeSend: function () {
                    $('.dnk-place').off('click', '.dnk_circle', self.reserve);
                    $btn.addClass('disabled');
                    self.showOverlay();
                },
                success: function (answer) {
                    $el.data('engaged', 1);
                    self.updateCart(answer);

                },
                error: function (err) {
                    console.log(err);
                }
            }).done(function () {
                $('.dnk-place').on('click', '.dnk_circle', self.reserve);
                $btn.removeClass('disabled');
                self.hideOverlay();
            });
        }
    };

    self.clearCart = function () {
        let $btn = $(".link-dnk-ticket-id");
        $.ajax({
            url: origin + '/clearCart',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            data: {
                'id': $("#clearCart").data('id')
            },
            beforeSend: function () {
                $('.dnk-place').off('click', '.dnk_circle', self.reserve);
                $btn.addClass('disabled');
                self.showOverlay();
                // self.stopTimer();
                Timer.stopTimer();
            },
            success: function (data) {
                if(data) {
                    $(".dnk-block-cart").html(data.html);
                    self.updateScene(data.colors);
                }
            },
            error: function (err) {
                console.log(err);
            }
        }).done(function () {
            $('.dnk-place').on('click', '.dnk_circle', self.reserve);
            self.hideOverlay();
        });

    };

    self.updateCart = function(answer) {
        let $cart = $(".scroll-cart");
        // if(answer.new) {
        //     $(".dnk-block-cart").html(answer.cart);
        //     $("#clearCart").on('click', self.clearCart);
        //     self.timer();
        //     // Timer.start();
        // } else if($.trim($cart.html()).length) {
        //     $cart.find('.block_cart').last().after(answer.cart);
        // }

        $(".dnk-block-cart").html(answer.cart);
        $("#clearCart").on('click', self.clearCart);
        if(!self.is_timing) {
            self.timer();
        }


        self.initCart();

        //
        // $("#cart_qty").text(parseInt(answer.qty));
        // $("#cart_total").text(parseInt(answer.total));
    };

    self.initCart = function() {
        $(".dnk-block-cart .scroll-cart").find('.btn-cart').each(function () {
            $(this).on('click', self.removeTicket);
        });
    };

    self.removeTicket = function() {
        $.ajax({
            url: origin + '/removeTicket',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            data: {
                'concert': $("#clearCart").data('id'),
                'id': $(this).data('ticket')
            },
            beforeSend: function () {
                self.showOverlay();
            },
            success: function (data) {
                // let $cart = $(".dnk-block-cart");
                // if(data.update) {
                //     $cart.html(data.html);
                //     self.initCart();
                // } else {
                //     $cart.html(data.html);
                //     self.stopTimer();
                //     // Timer.stopTimer();
                // }
                self.updateCart(data);
                self.updateScene(data.colors);
                if(data.empty) {
                    Timer.stopTimer();
                }
            },
            error: function (err) {
                console.log(err);
            }
        }).done(function () {
            self.hideOverlay();
        });
    };

    self.getPlaces = function () {
        $.ajax({
            url: origin + '/getPlaces',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            data: {
                'id': url.substring(url.lastIndexOf('/') + 1),
                'places': true
            },
            beforeSend: function () {
                self.loading = true;
                self.showOverlay();
            },
            success: function (data) {
                self.updateScene(data);
            },
            error: function (err) {
                console.log(err);
            }
        }).done(function () {
            self.hideOverlay();
            self.loading = false;
        });
    };

    self.updateScene = function (data) {
        if(data) {
            data.forEach(function (el) {
                let $place = $(".dnk_place[data-row='" + el.row + "'][data-place='" + el.num_place + "'][data-section='" + el.section_id + "']");
                if (!el.engaged) {
                    $place.data('color', el.color);
                    $place.data('id', el.id);
                    $place.css({'background-color': el.color});
                }
                self.setTooltip($place, el.price, el.engaged);
            });
            $places.addClass('price_selected');
        }
    };

    self.setTooltip = function (el, price, engaged) {
        let $parent = el.parent();
        let $tooltip = $parent.find('.tooltiptext-bubble');
        $tooltip.find('.tooltiptext-price-block').show();
        $tooltip.find('.tooltiptext-price').text(price + " грн");
        $tooltip.find('.tooltiptext-status-info').text(engaged ? "Занято" : "Свободно");
    };

    self.handlerHover = function () {
        let $dnk_place = $(".dnk-place");
        let $dnk_filter = $(".dnk_filter_circle");
        $dnk_place.on('mouseover', '.dnk_place', {show: true}, self.bubble);
        $dnk_place.on('mouseout', '.dnk_place', {show: false}, self.bubble);

        $dnk_filter.on('mouseover', '.dnk_circle', {show: true}, self.bubbleFilter);
        $dnk_filter.on('mouseout', '.dnk_circle', {show: false}, self.bubbleFilter);
    };

    self.bubble = function (event) {
        let row = $(this).data('row');
        let place = $(this).data('place');
        let section = $(this).data('section');
        if (isFinite(row) && isFinite(place)) {
            let attr = 'bubble_' + section + "_" + row + "_" + place;
            let $target = $(document).find(".tooltiptext-bubble[data-hover='" + attr + "']");
            if (event.data.show) {
                $target.css({
                    'visibility': 'visible',
                    'opacity': '1'
                });
            } else {
                $target.css({'visibility': 'hidden'});
            }
        }
    };

    self.bubbleFilter = function (event) {
        let filter = $(this).data('filter');
        if (isFinite(filter)) {
            let attr = 'filter_bubble_' + filter;
            let $target = $(document).find(".tooltiptext-bubble.bubble-filter[data-hover='" + attr + "']");
            self.toggleVisible($target, event.data.show);
        }
    };

    self.toggleVisible = function ($target, show) {
        if (show) {
            $target.css({
                'visibility': 'visible',
                'opacity': '1'
            });
        } else {
            $target.css({'visibility': 'hidden'});
        }
    };

    self.saveOldColors = function($target) {
        let data = self.getParams($target[0]);
        old_colors.push(data) - 1;
    };

    self.getParams = function (el) {
        let data = {};
        [].forEach.call(el.attributes, function (attr) {
            if (/^data-/.test(attr.name)) {
                let camelCaseName = attr.name.substr(5).replace(/-(.)/g, function ($0, $1) {
                    return $1.toUpperCase();
                });
                data[camelCaseName] = attr.value;
            }
        });
        if($(el).data('color')) {
            data['color'] = $(el).data('color');
        }
        return data;
    };



    $(".custom-switch").click(function () {
        if($("#customSwitches").is(':checked')){
            $('.admin-scene').hide();
            $('.tablet-sector').show();
        }else{
            $('.admin-scene').show();
            $('.tablet-sector').hide();
        }
    });


    self.timer = function () {
        let duration = self.getTimer();
        if (duration) {
            var timer = duration, minutes, seconds;
            self.is_timing = true;


            var refTime = setInterval(function () {
                if (self.is_timing) {
                    minutes = parseInt(timer / 60, 10);
                    seconds = parseInt(timer % 60, 10);

                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;

                    let time = minutes + ":" + seconds;
                    $time.text(time);

                    if (timer <= 0) {
                        // self.stopTimer();
                        // self.getStatus();
                        self.clearCart()

                    }
                    timer--;
                    self.updateTimer(timer);
                } else {
                    clearInterval(refTime);
                }
            }, 1000);
        }
    };


    self.getTimer = function() {
        let name = 'timer';
        let matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? parseInt(decodeURIComponent(matches[1])) : undefined;
    };

    self.updateTimer = function(value) {
        let expire = 1;
        let expireToDate = new Date();
        expireToDate.setTime(new Date().getTime() + expire);
        let cookieStr = encodeURIComponent('timer') + "=" + encodeURIComponent(value) + "; path=/; Max-Age=" + expire + "; Expires=" + expireToDate.toUTCString();
        document.cookie = cookieStr;
    };

    // self.stopTimer = function() {
    //     $time.text(DEFAULT_TIME_LABEL);
    //     self.is_timing = false;
    // };


    self.getStatus = function() {
        $.ajax({
            url: origin + '/processingStatus',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            success: function (status) {
                if(status != "continue") {
                    // self.stopTimer();
                    Timer.stopTimer();
                    // Timer.stopTimer();
                    self.clearCart();
                } else {
                    // self.timer();
                    Timer.startTimer();
                    // Timer.start();
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    };

    self.showOverlay = function() {
        $loader.show();
        $overlay.show();
    };

    self.hideOverlay = function() {
        $loader.hide();
        $overlay.hide();
    };

    $('.order-ticket-footer img').click(function () {
        $('.order-ticket-footer img').hide();
        $('.dnk-block-cart').css('position','absolute');
        $('.dnk-block-cart').css('width','300px');
        $('.dnk-block-scene').css('background','rgba(17, 37, 53, 0.1)');
        $('.dnk-block-cart').css('display','block');
        $('.dnk-block-cart').css('background-color','white');
        $('.tablet-sector').css('background','rgba(17, 37, 53, 0.1)');
        $('.filter_colors_tablet').css('background','white');
        $('.dnk-block-scene').click(function () {
            $('.order-ticket-footer img').show();
            $('.dnk-block-cart').css('display','none');
            $('.dnk-block-scene').css('background','whitesmoke');
            $('.tablet-sector').css('background','whitesmoke');
            $('.filter_colors_tablet').css('background','whitesmoke');
        });
    });
});

$(function () {
    scene.init();
});